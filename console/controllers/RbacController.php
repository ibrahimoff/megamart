<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;
class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'dashboard';
        $auth->add($dashboard);
        $userPanel = $auth->createPermission('user_panel');
        $userPanel->description = 'user panel';
        $auth->add($userPanel);
        $rule = new UserRoleRule();
        $auth->add($rule);
        $user = $auth->createRole('user');
        $user->description = 'user';
        $user->ruleName = $rule->name;
        $auth->add($user);
        $moder = $auth->createRole('moder');
        $moder->description = 'moder';
        $moder->ruleName = $rule->name;
        $auth->add($moder);
        $auth->addChild($moder, $user);
        $auth->addChild($moder, $dashboard);
        $admin = $auth->createRole('admin');
        $admin->description = 'admin';
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $moder);
        $auth->addChild($admin, $userPanel);
    }
}