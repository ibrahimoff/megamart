<?php
namespace console\controllers;
use backend\models\Category;
use backend\models\CategoryLang;
use Elastica\Test\Exception\ElasticsearchExceptionTest;
use Elasticsearch\Client;
use frontend\models\Manufacturer;
use frontend\models\Product;
use frontend\models\ProductLang;
use Yii;
use yii\console\Controller;
use yii\helpers\VarDumper;
class SettingsController extends Controller
{

    /*переписывает ид категории в продуктах*/
    public function actionRepair()
    {
        $lastChildren = Category::find()->where('nleft+1=nright')->joinWith('categoryTranslate')->andFilterWhere(['not REGEXP','name','[А-Яа-я]'])->all();
        foreach($lastChildren as $lastChild){
             $products = Product::find()->where(['category_id'=>$lastChild->id])->all();
                foreach($products as $product){
                    $product->category_id = $lastChild->parent_id;
                    $product->save(false);
                    var_dump('saved');
                }
        }

    }
    /*удаление брендов и реинит дерева*/
    public function actionReinit()
    {
        $lastChildren = Category::find()->where('nleft+1=nright')->joinWith('categoryTranslate')->andFilterWhere(['not REGEXP','name','[А-Яа-я]'])->all();
        foreach($lastChildren as $lastChild){
            //if(CategoryLang::deleteAll(['category_id'=>$lastChild->id])){
                if($lastChild->deleteWithChildren()){
                    var_dump('deleted');
                }
           // }
        }

    }


    public function actionChangeCategory(){
        $lastChildren = Category::find()->where('nleft+1=nright')->all();
        foreach($lastChildren as $last){
            $parents = $last->parents()->all();
            foreach($parents as $p){
                var_dump($p->id);
            }
        }
    }

    /*удаляет переводы без родителя*/
    public function actionDeleteNull(){
        $nullCats = CategoryLang::find()->joinWith('category')->where('category.id is null')->all();
        foreach($nullCats as $nullCat){
            if($nullCat->delete()){
                var_dump('deleted');
            }
        }
    }
    public function actionRemove($id)
    {
        $category = Category::find()->where(['id'=>$id])->one();
        $category->deleteWithChildren();
        echo 'deleted';
    }
    /*вставляет имена производителей в поле manufacturer в табилце product*/

    public function actionInsertByCategory(){
        $products = Product::find()->where(['!=','manufacturer_id',0])->joinWith('manufacturer')->andFilterWhere(['is not','manufacturer.id',null])->all();
        $manufacturers = Manufacturer::find()->indexBy('id')->all();

        foreach($products as $product){
            $product->manufacturer = $manufacturers[$product->manufacturer_id]->name;
            $product->save(false);
            var_dump('saved');

        }
    }
    /*вставляет имена категорий в поле manufacturer в табилце product*/
    public function actionInsertByChild(){
        $products = Product::find()->where(['=','manufacturer_id',0])->with('category')->all();

        foreach($products as $product){
            $product->manufacturer = $product->category->categoryTranslate->name;
            $product->save(false);
            var_dump('saved');

        }
    }
    /*вставляет id в manufacturer_id по имени из поля manufacturer в табилце product*/

    public function actionInsertManufacturerId(){
        $products = Product::find()->where(['=','manufacturer_id',0])->all();
        $manufacturers = Manufacturer::find()->indexBy('name')->all();
        foreach($products as $product){
            if(!empty($manufacturers[$product->manufacturer])){
                $product->manufacturer_id = $manufacturers[$product->manufacturer]->id;
                $product->save(false);
                var_dump('saved');
            }

        }
    }

    /*вставляет новые бренды таблицу manufacturer из таблицы product поле manufacturer*/
    /*нужно повторить предыдущий шаг так как я взял всех произвдителей из поля manufacturer и
     создал новые бренды но теперь в продукте нужно вставить manufacturer_id нового бренда*/
    public function actionInsertNewBrands(){
        $products = Product::find()
            ->andWhere(['NOT REGEXP','manufacturer','[А-Яа-я]'])
            ->andWhere(['!=','category_id',1])
            ->groupBy('manufacturer')->all();
        $manufacturers = Manufacturer::find()->select('name')->asArray()->column();
        foreach($products as $product){
            if(!in_array($product->manufacturer,$manufacturers)) {
                $newBrand = new Manufacturer();
                $newBrand->name = $product->manufacturer;
                $newBrand->status = 1;
                $newBrand->save(false);
                var_dump('saved');
            }
        }
    }

    //SELECT DISTINCT name FROM `category_lang` WHERE name not REGEXP '[А-Яа-я]' and language='ru'

    /*вставляет новые бренды таблицу manufacturer из таблицы category_lang*/
    public function actionInsertNewBrandsCategory(){
        $categoryName = CategoryLang::find()
            ->where(['!=','language','ru'])
            ->andWhere(['not REGEXP','name','[А-Яа-я]'])
            ->joinWith('category')
            ->andWhere('nleft+1=nright')
            ->groupBy('name')
            ->all();
        $manufacturers = Manufacturer::find()->select('name')->asArray()->column();

        foreach($categoryName as $category){
            if(!in_array($category->name,$manufacturers)){
                $newBrand = new Manufacturer();
                $newBrand->name = $category->name;
                $newBrand->status = 1;
                $newBrand->save(false);
                var_dump('not in array');
            }else{
                var_dump('in array!!!');

            }
        }
    }
    public function actionInsertCategory(){
        $parent = Category::findOne(['id' => 245]);
        $child = new Category(['status' => '0','parent_id'=>25,'level'=>2]);
        if($child->insertAfter($parent)){
            var_dump($child->id);
        }
        var_dump('success');
    }


    public function actionPushElastic(){
        $products = ProductLang::find()->with(['productOnly','discount','productCoverImage','productAttr'])->indexBy('id')->asArray()->one();
        $client = new \Elastica\Client([
            'host'=>'localhost',
            'port'=>9200
        ]);
        $index = $client->getIndex('products');
        $index->create([], true);
        $type = $index->getType('product');
           foreach($products as $key=>$value){
                $type->addDocument($key, $value);
                $index->refresh();

           }

        var_dump('success',6,1);
    }
}