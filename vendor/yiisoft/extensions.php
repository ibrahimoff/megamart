<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  '2amigos/yii2-date-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-picker-widget',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@dosamigos/datepicker' => $vendorDir . '/2amigos/yii2-date-picker-widget/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '1.8.2.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.2.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'borales/yii2-elasticsearch-behavior' => 
  array (
    'name' => 'borales/yii2-elasticsearch-behavior',
    'version' => '0.0.1.0',
    'alias' => 
    array (
      '@borales/behaviors/elasticsearch' => $vendorDir . '/borales/yii2-elasticsearch-behavior/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput',
    ),
  ),
  '2amigos/yii2-tinymce-widget' => 
  array (
    'name' => '2amigos/yii2-tinymce-widget',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@dosamigos/tinymce' => $vendorDir . '/2amigos/yii2-tinymce-widget/src',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating',
    ),
  ),
  'creocoder/yii2-nested-sets' => 
  array (
    'name' => 'creocoder/yii2-nested-sets',
    'version' => '0.9.0.0',
    'alias' => 
    array (
      '@creocoder/nestedsets' => $vendorDir . '/creocoder/yii2-nested-sets/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-elasticsearch' => 
  array (
    'name' => 'yiisoft/yii2-elasticsearch',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/elasticsearch' => $vendorDir . '/yiisoft/yii2-elasticsearch',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.6.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '1.3.2.0',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
);
