<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace yii\helpers;

/**
 * Url provides a set of static methods for managing URLs.
 *
 * @author Alexander Makarov <sam@rmcreative.ru>
 * @since 2.0
 */
class Url extends BaseUrl
{
    public static function link($link,$routeA=null){
        $currentLanguage = \Yii::$app->language;
        if($routeA != null){
            $link = '/'.$currentLanguage.'/'.$routeA.'/'.$link;
        }else{
            $link = '/'.$currentLanguage.'/'.$link;

        }
        return $link;
    }
}
