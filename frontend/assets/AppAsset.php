<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/fonts.css',
        'lib/font-awesome/css/font-awesome.min.css',
        'lib/slick-carousel/slick/slick.css',
        'lib/slick-carousel/slick/slick-theme.css',
        'css/bootstrap_skin.css',
        'css/style.css',
        'css/media.css',
        'css/rating.css',
        "css/nouislider.css",
        "css/animate.css",

    ];
    public $js = [

        'lib/slick-carousel/slick/slick.min.js',
        'lib/imagesloaded/imagesloaded.pkgd.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js',
        "js/nouislider.js",
        'js/common.js',
        'js/pinbox.js',
        'js/jquery.elevatezoom.js',
        "js/react.js",
        "js/react-dom.js",
        "js/browser.min.js",
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
