
$(document).ready(function(){

    $(document).on('click','.product-actions a.pin-product', function(e){
        var _this = $(this);
        var pinCountSpan = _this.parent().find("span.count");
        var pinCount = parseInt(pinCountSpan.html());

        $.ajax({
            url: '/site/pin-unpin',
            dataType: 'json',
            type: 'get',
            data: {id:_this.data("id")},
            success: function (data) {
                if (data.checked == 1) {
                    _this.addClass("active");
                    pinCountSpan.html(pinCount+1);
                    $("#pinnedCounter").html(parseInt($("#pinnedCounter").html())+1);
                    //openModal('Password reset', data.message, 310);
                } else if (data.checked == 0) {
                    _this.removeClass("active");
                    pinCountSpan.html(pinCount-1);
                    $("#pinnedCounter").html(parseInt($("#pinnedCounter").html())-1);
                }

            },
            error: function (data) {
                //
            }
        });
        e.preventDefault();
    });


    $(document).on('click','#rate-order-form button', function(e){
        var _this = $(this);
        var token =_this.data("token");
        var form = $("#rate-order-form");
        $("#rating_alert").remove();
        $.ajax({
            url: '/rating/rate?token='+token,
            dataType: 'json',
            type: 'get',
            data: form.serialize(),
            success: function (data) {
                if (data.created == 1) {
                    $(".rate-order").prepend("<div id='rating_alert' class='alert-success alert fade in'>"+
                    "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                        data.message+"</div>");
                    //openModal('Password reset', data.message, 310);
                } else if (data.created == 0) {
                    $(".rate-order").prepend("<div id='rating_alert' class='alert-danger alert fade in'>"+
                        "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"+
                        data.message+"</div>");
                }

            },
            error: function (data) {
                //
            }
        });
        e.preventDefault();
    });


    $(document).on('click','.rating-item i.fa', function(e){
        var token = $(this).data("token");
        var commentDiv = $(this).parents(".rating-item").find(".review-comment");
        var text = commentDiv.html().trim();
        commentDiv.html("<textarea data-token='"+token+"' class='change-rating-comment' rows='4' name='Rating[comment]'>"+text+"</textarea>");
        $(".change-rating-comment").focus();
        $(this).hide();
    });
    $(document).on('keyup','.rating-item .change-rating-comment', function(e){
        var token = $(this).data("token");
        var _this = $(this);
        var item = _this.parents(".rating-item");
        var text = $(this).val().trim();
        $(".change-rating-comment").focus();
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $.ajax({
                url: '/rating/rate?token=' + token,
                dataType: 'json',
                type: 'get',
                data: {'comment':text},
                success: function (data) {
                    if (data.created == 1) {
                        $(".rating-list i.fa").show();
                        item.html("<div class='alert alert-info fade in'>"+data.approveText+"</div>");
                        setTimeout(function(){
                            item.fadeOut("slow");
                        }, 4000);
                    }
                },
                error: function (data) {
                    //
                }
            });
        }
    });

});
