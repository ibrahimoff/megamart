function range()
{
    var array = [];
    var start = (arguments[0] ? arguments[0] : 0);
    var end   = (arguments[1] ? arguments[1] : 9);
    if (start == end) { array.push(start); return array; }
    var inc   = (arguments[2] ? Math.abs(arguments[2]) : 1);
    inc *= (start > end ? -1 : 1);
    for (var i = start; (start < end ? i <= end : i >= end) ; i += inc)
        array.push(i);
    return array;
}
var locale = [{
    az:{
        buy:'Səbətə at',
        compare:'Müqayisə'
    },
    ru:{
        buy:'Купить',
        compare:'Сравнить'
    }
}];

var redBarCounter = [{
    compare:{
        count:0,
        items:[]
    },
    pinned:{
        count:0,
        items:[]
    },
    bucket:{
        count:0,
        items:[]
    }
}]

var BuyButton = React.createClass({

    buyItem:function(e){
        $('#elastic').removeClass('hidden')
        e.stopPropagation();
        var bucketCounterElement = $('#bucketCounter');
            e.preventDefault();
            var id = e.target.dataset.id;
            e.target.className += ' disabled';
            $.ajax({
                method:'post',
                url:'/card/save-to-bucket',
                data:{
                    id:id
                }
            }).success(function(response){
                var bucketCounter = parseInt(bucketCounterElement.text());
                var increasedBucketCounter = bucketCounter += 1;
                bucketCounterElement.text(increasedBucketCounter);
                console.log(response)
            }).error(function(err){
                console.error(err)
            })
    },
    render:function(){
        return (
                <button className="itemBlockBtn buyButton" type="button" id="buyButton" data-id={this.props.product} onClick={this.buyItem}>{locale[0]['ru']['buy']}</button>
        )
    }
})

var CompareButton = React.createClass({
    compareItem:function(e){
        $('#elastic').removeClass('hidden')
        e.stopPropagation();
        var compareCounterElement = $('#compareCounter');
        e.preventDefault();
        var id = e.target.dataset.id;
        e.target.className += ' disabled';
        $.ajax({
            method:'post',
            url:'/compare/save-to-compare',
            data:{
                id:id
            }
        }).success(function(response){
            var compareCounter = parseInt(compareCounterElement.text());
            var increasedCompareCounter = compareCounter += 1;
            compareCounterElement.text(increasedCompareCounter);
            console.log(response)
        }).error(function(err){
            console.error(err)
        })
    },
    render: function () {
        return(
            <button className="itemBlockBtn" type="button" data-id={this.props.product} id="compareButton" onClick={this.compareItem}>{locale[0]['ru']['compare']}</button>

        )
    }
})

var ElasticSearch = React.createClass({
    getInitialState(){
        return ({
            items:undefined,
            itemsLength:undefined,
            position:undefined
        })
    },
    componentDidMount:function(){
        this._sendAjax();
        this._hideOneDocumentClick();
        this.setState({lang:$('#elastic').attr('language')});
    },
    _sendAjax(){
        var self = this;
        var holded = false;
        var inputElement = $('#elasticSearch');
        inputElement.keydown(function(){
            holded = true;
        }).keyup(function(e){
                var charKeys = range(48, 90);
                charKeys.push(189)
                var value = $(this).val();
                if(value.trim().indexOf('-') != -1)
                {
                    value = value.trim().replace("-", " ");
                }
                var language = e.target.dataset.lang;
                if(value.trim() != ''){
                    if(value.trim().length > 3){
                        //if($.inArray(e.keyCode,charKeys) != -1 || e.keyCode === 8){
                            $.ajax({
                                method:'post',
                                url:'/site/search',
                                type:'json',
                                data:{
                                    value:value.trim(),
                                    lang:language
                                }
                            }).success(function(response){
                                if(response != ''){
                                    self.setState({items:response,itemsLength:response.length});
                                    //console.log(response)
                                }
                                if(response.length == 0){
                                    self.setState({items:undefined,itemsLength:response.length});
                                }
                            }).error(function(err){
                                console.error(err)
                            })
                      //  }
                    }
                }else{
                    if(e.keyCode == 8){
                        self.setState({items:undefined});
                        self.setState({position:undefined})
                    }
                }
            holded = false;
        });
        $(window).keydown(function(e){
            self._selectResult(e)
        });
        inputElement.on('keyup keypress', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                if($(this).val().trim().length < 3){
                    e.preventDefault();
                    return false;
                }
            }
        });

    },
    _selectResult:function(e){
        var self = this;
        var keyCode = e.keyCode || e.which;
        var itemElement = $('.itemBlock');
        var inputElement = $('#elasticSearch');
        if(keyCode === 40){
            if(this.state.position == undefined){
                //если позиция не задана то при нажатии на кнопку вниз позиция становится 1
                this.setState({position:1})
            }else{
                //если уже есть то просто прибавляет единицу к текущей позиции
                this.setState({position:this.state.position+1})
            }
            if(this.state.position > this.state.itemsLength){
                //если позиция равна состоянию компонента то обнуляет состояние
                this.setState({position:1})
            }
            inputElement.blur();
            e.preventDefault();
        }
        if(keyCode === 38){
            if(this.state.position == undefined){
                //если позиция не задана то при нажатии на кнопку вниз позиция
                // становится самой максимальной, тоесть выбран последний элемент
                this.setState({position:this.state.itemsLength})
            }else{
                //если уже есть то просто отнимает единицу от текущей позиции
                this.setState({position:this.state.position-1})
            }
            if(this.state.position < 1){
                //если позиция равна состоянию компонента то обнуляет состояние
                if(this.state.itemsLength > 1){
                    this.setState({position:this.state.itemsLength})
                }else{
                    this.setState({position:undefined})
                }
            }
            inputElement.blur();
            e.preventDefault();
        }
        if(self.state.items != undefined){
            if(e.keyCode == 13){
                window.location = $('.selected').children()[0].href
            }
        }
        itemElement.removeClass('selected');
        itemElement.find('button').css('display','none')
        itemElement.each(function(){
            if($(this).data('position') == self.state.position){
                $(this).find('button').css('display','block')
                $(this).addClass('selected');
            }
        });

    },
    _hideOneDocumentClick:function(){
        var inputElement = $('#elasticSearch');
        var listElement = $('#elastic');
        $(document).on('click','body',function(){
            $('#elastic').addClass('hidden');
        });
        inputElement.click(function(e){
            e.stopPropagation();
            if(listElement.hasClass('hidden')){
                listElement.removeClass('hidden')
            }
        });
    },
    _selectItem: function (e) {
        $('.itemBlock').removeClass('selected');
        this.setState({position:undefined});
        if(e.target.parentNode.className == "itemBlock"){
            e.target.parentNode.className += " selected";
        }
        $('.selected').find('button').css('display','inline-block')
    },
    _deselectItem: function (e) {
        $('.selected').find('button').css('display','none')
        if(e.target.parentNode.className == "itemBlock selected"){
            e.target.parentNode.className = "itemBlock";
        }
    },
    render:function(){
        var self = this;
        if(this.state.items != undefined) {
            var itemsResult = this.state.items.map(function (item,key ) {
                var imageName = item['_source']['_product']['productCoverImage']['product_id']+'-'+item['_source']['_product']['productCoverImage']['id']+'-small.jpg'
                var imageUrl = '/uploads/products/'+imageName;

                return (
                    <div key={key} className="itemBlock" data-position={key+1} onMouseLeave={self._deselectItem} onMouseEnter={self._selectItem}>
                        <a href={"/"+self.state.lang+"/_p/"+item['_source']['_product']['slug']}>
                                <img src={imageUrl} alt=""/>
                                {item['_source']['_product']['name']}

                        </a>
                        <div className="clearfix"></div>
                    </div>
                );
            })
        }
        return (
            <div>
                {itemsResult}
            </div>
        )
    }
});
ReactDOM.render(<ElasticSearch/>,document.getElementById('elastic'));
