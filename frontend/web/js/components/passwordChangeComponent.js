/**
 * Created by fuad on 12/24/15.
 */
var formGroup = "form-group";
var formControl = "form-control";
var successClass = "success";
var errorClass = "text-danger";
var errorColor = '#a94442';
var successColor = '#3c763d';
var defaultColor = "#cccccc";
var btnClass = "btn btn-primary";
var PasswordChangeComponent = React.createClass({
    getInitialState:function(){
        return ({
            activeLanguage:null,
            oldPasswordFieldError:false,
            newPasswordFieldError:false,
            confirmPasswordFieldError:false,
            formValidated:false,
            formMessage:undefined
        })
    },
    render:function(){
        var self = this;
        var formMessageClass = self.state.formValidated == false ? "text-danger" : "text-success";
        return (<form action="site/changepassword" id="changePasswordForm">
            <div>
                <div className={formMessageClass}>{self.state.formMessage}</div>
                <div className={formGroup}>
                    <label htmlFor="changepassform-oldpassword">Old password</label>
                    <input type="password" name="ChangePassForm[oldPassword]" className={formControl} ref="oldPassword" onBlur={self._checkOldPassword}/>
                    <div className={errorClass}>{self.state.oldPasswordFieldError}</div>
                </div>
                <div className={formGroup}>
                    <label htmlFor="changepassform-newpassword">New Password</label>
                    <input type="password" name="ChangePassForm[newPassword]" className={formControl} ref="newPassword"  onBlur={self._comparePasswords}/>
                </div>
                <div className={formGroup}>
                    <label htmlFor="changepassform-confirmpassword">Confirm password</label>
                    <input type="password" name="ChangePassForm[confirmPassword]" className={formControl} ref="confirmPassword" onBlur={self._comparePasswords}/>
                    <div ref="newPasswordConfirmError" className={errorClass}></div>
                </div>
            </div>
            <button type="button" onClick={self._validateForm} className={btnClass}>Save</button>
        </form>)
    },
    _checkOldPassword:function(event){
        var newPasswordField = event.target;
        var self = this;
        if(event.target.value.trim() != ''){
            $.ajax({
                method: "POST",
                url: "/site/check-password",
                data: {
                    oldPasswordValue: newPasswordField.value.trim()
                }
            }).done(function(result) {
                if(result == true){
                    newPasswordField.style.borderColor = successColor;
                    self.setState({
                        oldPasswordFieldError:null
                    })
                    self.setState({oldPasswordFieldError:false})
                }else{
                    newPasswordField.style.borderColor = errorColor;
                    self.setState({oldPasswordFieldError:true})
                    self.setState({
                        oldPasswordFieldError:'Incorrect password'
                    })
                }
            });
        }
    },
    _comparePasswords:function(){
        var self = this;
        var newPasswordInput = this.refs.newPassword;
        var comparePasswordInput = this.refs.confirmPassword;
        if(newPasswordInput.value.trim() != '' && comparePasswordInput.value.trim() != ''){
            if(newPasswordInput.value.trim() != comparePasswordInput.value.trim()){
                newPasswordInput.style.borderColor = errorColor;
                comparePasswordInput.style.borderColor = errorColor;
                self.setState({confirmPasswordFieldError:true});
                self.setState({newPasswordFieldError:true});
                self.refs.newPasswordConfirmError.innerHTML = 'Passwords don\'t match';
            }else{
                newPasswordInput.style.borderColor = successColor;
                comparePasswordInput.style.borderColor = successColor;
                self.setState({confirmPasswordFieldError:false});
                self.setState({newPasswordFieldError:false});
                self.refs.newPasswordConfirmError.innerHTML = '';
            }
        }else{
            newPasswordInput.style.borderColor = defaultColor;
            comparePasswordInput.style.borderColor = defaultColor;
        }
    },
    _setTimeOut:function(){
        var self = this;
        setTimeout(function(){
            self.setState({formMessage:undefined})

        },4500)
    },
    _validateForm:function(){
        var self = this;
        var fieldsArray = [
            this.refs.oldPassword,
            this.refs.newPassword,
            this.refs.confirmPassword
        ];
        $.each(fieldsArray,function(key,input){
            if(self.state.confirmPasswordFieldError == false &&
               self.state.newPasswordFieldError == false &&
               self.state.oldPasswordFieldError == false){
                if(input.value.trim() != ''){
                    self.setState({formValidated:true});
                }else{
                    input.style.borderColor = errorColor;
                    self.setState({formValidated:false});
                }
            }else{
                self.setState({formValidated:false});
            }
        });
        self.forceUpdate(function(){
            if(this.state.formValidated == true){
                $.ajax({
                    method: "POST",
                    url: "/site/change-password",
                    data: {
                        newPassword: self.refs.newPassword.value.trim()
                    }
                }).done(function(result) {
                    if(result == true){
                        self.setState({formMessage:'Successfully saved!'});
                        self._setTimeOut();
                        self.refs.oldPassword.value = '';
                        self.refs.newPassword.value = '';
                        self.refs.confirmPassword.value = '';
                    }else{
                        self.setState({formMessage:'Entered data is not correct'});
                        self._setTimeOut();
                    }
                });
            }else{
                self.setState({formMessage:'Entered data is not correct'});
                self._setTimeOut();
            }
        });

    },
});
ReactDOM.render(<PasswordChangeComponent />,document.getElementById('changePassComponent'))