var placeSearch, autocomplete;
var componentForm = {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

function initAutocomplete() {
    if(document.getElementById('user-address') != null)
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('user-address')),{types: ['geocode']});
}
function checkoutAutocomplete() {
    if(document.getElementById('addressform-address') != null)
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('addressform-address')),{types: ['geocode']});
}
function addressAutocomplete() {
    if($('.user-address').length > 0){
     //   console.log(increment);
        $('.user-address').each(function(key,item){
            autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementsByClassName('user-address')[key]),{types: ['geocode']});
        });

    }
}