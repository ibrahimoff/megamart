var summaryCardPrice = $('#sum_price');
var itemCounter = $('#item_counter');

var productArray = [];
function ucfirst( str ) {
	var f = str.charAt(0).toUpperCase();
	return f + str.substr(1, str.length-1);
}
function subStrByChar(str){
	var string;
	string = str.substr(str.indexOf('-')+1);
	return string;
}
function showBucket(){
	$('#bucket_backdrop').show();
	TweenMax.to($('#bucket_backdrop'), 0.2, {opacity:.75,ease:Power4.easeNone});
	TweenMax.to($('#bucket_window'), 0.2, {x:-600,ease:Power4.easeNone});
}
function slider_init(){
	$('#hot-deals-slider').slick({
	  dots: true,
	  infinite: true,
	  speed: 500,
	  fade: true,
	  cssEase: 'ease',
	  autoplay: true,
	  autoplaySpeed: 3000,
	});
}

function item_slider_init(){
	$('.items-slider').slick({
		speed: 200,
		dots: false,
		infinite: true,
		accessibility:true,
		arrows: true,
		cssEase: 'ease',
		slidesToShow: 5,
		slidesToScroll: 5,
		prevArrow: '<span class="glyphicon glyphicon-menu-left"></span>',
		nextArrow: '<span class="glyphicon glyphicon-menu-right"></span>',
		responsive: [
			{
			  breakpoint: 1300,
			  settings: {
			    slidesToShow: 4,
			    slidesToScroll: 4,
			  }
			},
			{
			  breakpoint: 992,
			  settings: {
			    slidesToShow: 3,
			    slidesToScroll: 3
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
			    slidesToShow:1,
			    slidesToScroll: 1
			  }
			}
		]
	});
}
function accessory_slider_init(){
	$('.accessory-slider').slick({
		speed: 200,
		dots: false,
		infinite: true,
		accessibility:true,
		arrows: true,
		cssEase: 'ease',
		slidesToShow: 7,
		slidesToScroll: 7,
		prevArrow: '<span class="glyphicon glyphicon-menu-left"></span>',
		nextArrow: '<span class="glyphicon glyphicon-menu-right"></span>',
		responsive: [
			{
				breakpoint: 1300,
				settings: {
					slidesToShow: 7,
					slidesToScroll: 7,
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 5
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow:1,
					slidesToScroll: 1
				}
			}
		]
	});
}

function sidebar_open(){
	$('#sidebar-wrapper-open').on('click',function(){
		$('#wrapper').toggleClass('sidebar-wrapper-opened');
		return false;
	});
	$('#sidebar-wrapper').on('mouseleave',function(){
		$('#wrapper').removeClass('sidebar-wrapper-opened');
	});
}

function search_button(){
	$(document).on('click','#search-button',function(){
		$('.search').addClass('opened');
		$('#header-main .row > div > a').hide();

		$('html').click(function(event) {
	        //check up the tree of the click target to check whether user has clicked outside of menu
	        if ($(event.target).parents('.search').length == 0) {
	           	$('.search').removeClass('opened');
				$('#header-main .row > div > a').show();
	            //this event listener has done its job so we can unbind it.
	            $(this).unbind(event);
	        }

	    });
	});
}

function sidebar_button(){
	$(document).on('click','#open-mini-sidebar',function(){
		$('#sidebar-wrapper').addClass('opened');

		$('html').click(function(event) {
	        //check up the tree of the click target to check whether user has clicked outside of menu
	        if ($(event.target).parents('#sidebar-wrapper').length == 0) {
	           	$('#sidebar-wrapper').removeClass('opened');
	            //this event listener has done its job so we can unbind it.
	            $(this).unbind(event);
	        }

	    });
	});
}
function product_small_img(){
	$('.product_small_img').on('click', function(){
		$('.product_small_img').removeClass('active');
		$(this).addClass('active');
		$('#product_img').attr('src',$(this).attr('href')).attr('data-zoom-image',$(this).attr('href'));
		$('.zoomWindowContainer > div').css('background-image','url("'+$(this).attr('href')+'")')
		return false;
	});
}


function product_img_zoom(){
	$('#product_img').elevateZoom({
		zoomType: "inner",
		cursor: "crosshair",
		zoomWindowFadeIn: 500,
		zoomWindowFadeOut: 750
	});
}



$(window).resize(function(){
	categoryScript();
});
$(document).ready(function(){
	$('#tawkchat-iframe-container').css('z-index','10');
	product_small_img();
	slider_init();
	item_slider_init();
	accessory_slider_init();
	sidebar_open();
	search_button();
	sidebar_button();
	product_img_zoom();
	categoryScript();
	/*buy product button*/
	var bucketCounterElement = $('#bucketCounter');
	var pinnedCounterElement = $('#pinnedCounter');

	$(document).on('click','.buyButton',function(e){
		e.preventDefault();
		var _this = $(this);
		var id = _this.data('id');
		var productList = $('#products_list');
		var counter = $('#item_counter');
		var sumPrice = $('#sum_price');
		var bucketCount = $('.bucket_count_span');

		$.ajax({
			method:'post',
			url:'/card/save-to-bucket',
			data:{
				id:id
			}
		}).success(function(response){
			showBucket();
			var html ='';
			_this.addClass('disabled');
			if(response != false){
				if(productArray[id] == undefined){
					productArray[id] = [];
				}
				$.each(response,function(key,item){

					productArray[id]['price'] = parseFloat(item.price)
					productArray[id]['quantity'] = parseInt(item.quantity)
						html = '<li>' +
						'<a class="bucket-item-img" href="'+item.slug+'"><img src="'+item.image+'" alt=""></a> ' +
						'<div class="bucket-item-name">' +
						' <a href="'+item.slug+'">'+item.name+'</a> ' +
						'</div> ' +
						'<div class="bucket-item-count" data-item-id="'+key+'"> ' +
						'<i class="fa fa-angle-up fa-2"></i> ' +
						'<span class="quantity">'+item.quantity+'</span>'+
						'<i class="fa fa-angle-down fa-2"></i> ' +
						'</div> <div class="bucket-item-price">'+item.price+'</div> ' +
						'<i class="bucket-item-close fa fa-times fa-2" data-item-id="'+key+'"></i> ' +
						'</li>';

					//summary = sumPrice.text() == ''? parseFloat(item.price):parseFloat(summary)+parseFloat(item.price)
				});
				bucketCount.text(parseInt(counter.text())+productArray[id]['quantity']);
				productList.append(html)
				counter.text(parseInt(counter.text())+productArray[id]['quantity']);
				sumPrice.text(parseFloat(sumPrice.text())+(parseFloat(productArray[id]['price'])*parseInt(productArray[id]['quantity'])))
				if($('#products_list').children().length >= 1){
					$('.make_order').removeClass('hidden');
				}

			}

		}).error(function(err){
			console.error(err)
		})
	});





	$(document).on('click','.bucket-item-remove',function(e){
		e.preventDefault();
		var id = $(this).data('item-id');
		var newPrice = parseFloat($('#new'+id).text());
		var totalPrice =  $('.total-price');
		$.ajax({
			url:'/card/remove-item',
			method:'post',
			data:{
				id:id
			}
		}).success(function(result){
			$('#item'+id).remove();
			totalPrice.text(parseFloat(totalPrice.text()) - parseFloat(newPrice));
			if($('.basket-item').length == 0){
				//window.location.replace('/')
			}
		}).error(function(err){
			console.error(err)
		})
	});




	$(document).on('click','.bucket-item-close',function(){
			var id = $(this).data('item-id');
			var _this = $(this)
			var counter = $('#item_counter');
			var price = $('#sum_price');
		var bucketCount = $('.bucket_count_span');

		counter.text(parseInt(counter.text()) - parseInt(productArray[id]['quantity']));
			price.text(parseInt(price.text()) - parseInt(productArray[id]['price']));
		if(id != undefined){
			delete productArray[id];
		}

		$.ajax({
				url:'/card/remove-item',
				method:'post',
				data:{
					id:id
				}
			}).success(function(){
			bucketCount.text(counter.text());
				$('#buy'+id).removeClass('disabled');
				var tl = new TimelineMax();
				tl.to(_this.parent('li'), 0.2, {x:600,ease:Power4.easeNone});
				tl.to(_this.parent('li'), 0.2, {height:0,ease:Power4.easeNone});
				if(parseInt(counter.text()) == 0){
					$('.make_order').addClass('hidden');
				}
			}).error(function(error){
				console.error(error)
			});

	})
});

$(document).on('click', '#bucket_button', function(){
	showBucket();
});



$(document).on('click', '.bucket_close', function(e){
	e.preventDefault();
	TweenMax.to($('#bucket_backdrop'), 0.2, {opacity:0,ease:Power4.easeNone, onComplete:function(){
		$('#bucket_backdrop').hide();
	}});
	TweenMax.to($('#bucket_window'), 0.2, {x:0,ease:Power4.easeNone});
});


function sendQuantityAjax(type,id,span){
	if(type != undefined){
		$.ajax({
			url:'/card/change-quantity',
			method:'post',
			data:{
				type:type,
				id:id,
			}

		}).success(function(){
			if($('.present-price').length != 0){
				var presentPrice = parseFloat($('#present'+id).text());
				var endPrice = presentPrice*parseInt(span.text());
				$('#price'+id).text(endPrice)
			}
		}).error(function(err){
			console.log(err)
		})
	}
}

$('#bucket_window').ready(function(){
	var parentElement = $('#products_list');
	if(parentElement.children().length > 0){
		var id;
		var quantity;
		var price;

		parentElement.children().each(function(){
			id = $(this).find('.bucket-item-count').data('item-id');
			price  = parseFloat($(this).find('.bucket-item-price').text());
			quantity = parseInt($(this).find('.quantity').text())
			if(productArray[id] == undefined){
				productArray[id] = [];
				productArray[id]['price'] = [];
				productArray[id]['quantity'] = [];
			}
			productArray[id]['price'] = price*quantity;
			productArray[id]['quantity'] = quantity;
		})
	}
})

$(document).on('click', '.bucket-item-count .fa', function(){
	var type;
	if($(this).hasClass('fa-angle-up')){
		type = 'increase';
		$(this).parents('.bucket-item-count').find('.quantity').text(parseInt($(this).parents('.bucket-item-count').find('.quantity').text())+1)
	}else if(parseInt($(this).parents('.bucket-item-count').find('.quantity').text())!=1){
		type = 'decrease';

		$(this).parents('.bucket-item-count').find('.quantity').text(parseInt($(this).parents('.bucket-item-count').find('.quantity').text())-1);
	}
	var id = $(this).parent('.bucket-item-count').data('item-id');
	var span = $(this).parents('.bucket-item-count').find('.quantity');
	var counter = $('#item_counter');
	var price = $('#sum_price');
	var summary = 0;
	var sumPrice = 0;
	var newPrice = $('#new'+id);
	var totalNewPrice = 0;
	var baseBonus = $('#item'+id+' .product-bonus a');
	var totalBonus = $('#item'+id+' .increase a');
	var bucketCount = $('.bucket_count_span');

	var presentPrice = parseFloat($(this).parent().next().text())*parseInt($(this).parents('.bucket-item-count').find('.quantity').text());
	if(productArray[id] == undefined){
		productArray[id] = [];
		productArray[id]['price'] = [];
		productArray[id]['quantity'] = [];
	}
	productArray[id]['price'] = presentPrice;
	productArray[id]['quantity'] = parseInt(span.text());

	var k;
	var test;
	for (k in productArray){
		sumPrice += parseFloat(productArray[k]['price']);
		summary +=parseInt(productArray[k]['quantity']);
	}
	counter.text(summary);
	price.text(sumPrice);
	bucketCount.text(summary);

	if(newPrice.length>0){
		newPrice.text(parseFloat($('#present'+id).text())*parseInt(span.text()));
		$('.new').each(function(){
			totalNewPrice += parseFloat($(this).text());
		})
		totalBonus.text(parseInt(baseBonus.text())*parseInt($(this).parents('.bucket-item-count').find('.quantity').text()))
		$('.total-price').text(totalNewPrice)
	}
	sendQuantityAjax(type,id,span)

});
function categoryScript(){
	if($(window).width() >=768){
		$(document).on('mouseover','li.parent-node',function(){
			$('#'+$(this).data('parent-id')).removeClass('hidden');
		})
		$(document).on('mouseleave','li.parent-node',function(){
			$('#'+$(this).data('parent-id')).addClass('hidden');
		})
	}else{
		$('.category_drop').attr('data-toggle','dropdown');
		// $(document).on('click','.category_list',function(e){
		// 	e.stopPropagation();
		// 	$(this).toggleClass('open');
		// });
		$(document).on('click','.parent-node',function(e){
			///alert(123123)
			e.stopPropagation();
			$('.category_list').addClass('open');
			$(this).find('ul').toggleClass('hidden')
				//.toggleClass('hidden');
		})
	}


}
// if($(window).width() <= 768){

// }
// $(document).on('click','.category_drop',function(e){
// 	e.preventDefault();
// });
//
// $(window).resize(function(){
// 	if($(window).width() <= 768){
// 		$(document).on('click','.mobile-navbar .category_list',function(){
// 			$(this).toggleClass('open');
// 		});
// 		$(document).on('click','.parent-node',function(){
// 			$(this).find('ul').toggleClass('hidden')
// 		})
// 	}
// })

$(document).on('click','#submit-filters',function(){
	var categoryId = $('.filters_list').attr('id');
	var checkboxes = $('.filters_list input[type="checkbox"]');
	var priceFields = $('.filters_list input[type="text"]');
	var redirectUrl = '?';
	//var redirectUrl = window.location.search;
	var checkboxParams = '?';
	var params = {};
	var key;
			checkboxes.each(function(){
				if(this.checked == true){

					if(params[$(this).data('type')] == undefined){
						params[$(this).data('type')] = [];
					}
					params[$(this).data('type')].push($(this).val());

				}
			});
				priceFields.each(function(){
				if($(this).val().trim() != ''){
					if(params[$(this).data('type')] == undefined){
						params[$(this).data('type')] = {};
					}
					params[$(this).data('type')] = parseFloat($(this).val());
				}
			});
	$.each(params,function(key,value){
		if(redirectUrl.length > 0){
			redirectUrl += '&'
		}
		redirectUrl += 'Filter['+key+']='+value.toString()
	});
	console.log(redirectUrl);
	if(redirectUrl.length > 0){
		window.location = redirectUrl;
	}

});


$(document).on('click','.compareButton',function(){
	var id = $(this).attr('id');
	var url;
	if($(this).hasClass('remove')){
		$(this).removeClass('remove');
		$(this).text('Сравнить')
		url = '/compare/remove'
	}else{
		$(this).addClass('remove');
		$(this).text('Удалить')
		url = '/compare/add'
	}
	$.ajax({
		url:url,
		method:'post',
		data:{
			id:id
		}
	}).success(function(success){

		console.log(success)
	}).error(function(err){
		console.log(err)
	})
});

$(document).on('click','.search form>button',function(e){
	if($('#elasticSearch').val().trim() == ''){
		e.preventDefault();
	}
});
$(document).on('click','#editable',function(){
	var container = $(this);
	var price = $(this).find('.present-price').text();

	var input = '<input type="number" class="form-control" value="'+Number(price).toFixed(2)+'" id="price_value" step="0.10">';
	if($(this).find('.present-price').length > 0){
		container.html(input);
	}
	var id = $(this).data('id');

	$('html').click(function(event) {
			var inputElem = $('#price_value');
			var inputVal = inputElem.val().trim();
			var value = Number(inputVal).toFixed(2);

			$.ajax({
				url:'/admin/product/change-price?id='+id+'&price='+value,
				method:'get'
			}).success(function(result){

			}).error(function(err){
				console.error(err)
			});
			var parent = inputElem.parent();
			var element = '<span class="present-price"  style="overflow:hidden;">'+inputVal+'<i class="mm-icon-azn" ></i></span>';
			if(!$(event.target).is('#price_value')){
				parent.html(element);
			}
			$(this).unbind(event);
	});

});
$(document).on('click','.checkout_btn',function(){

	$('.step_form').addClass('hidden');
	var id = $(this).data('step-id');
	var title = $(this).text();
	$('#'+id).toggleClass('hidden');
	$('#change_title').text(title);
});
$(document).on('click','.switcher',function(){
	var list = $(this).parent();
	var _this = $(this);
	var id = $(this).data('switch');
	var icon = $(this).find('.fa');
	_this.next().slideToggle();
		if(icon.hasClass('fa-caret-down')){
			icon.addClass('fa-caret-left');
			icon.removeClass('fa-caret-down')
		}else{
			icon.removeClass('fa-caret-left');
			icon.addClass('fa-caret-down')
		}
	$.ajax({
		url:'/product/switcher',
		method:'post',
		data:{
			id:id
		}
	}).success(function(response){

	}).error(function(err){
		console.error(err)
	});
});
$(document).on('click','.checkbox_attr',function(){
	var icon = $(this).find('.fa');
	if(icon.hasClass('fa-square-o')){
		icon.addClass('fa-check-square');
		icon.removeClass('fa-square-o')
	}else{
		icon.removeClass('fa-check-square');
		icon.addClass('fa-square-o')
	}
});
$(document).ready(function(){
	var snapSlider = document.getElementById('slider-handles');
	var snapValues = [
		document.getElementById('slider-snap-value-lower'),
		document.getElementById('slider-snap-value-upper')
	];
	if(document.getElementById('slider-snap-value-upper') != null || document.getElementById('slider-snap-value-lover')  != null){
		noUiSlider.create(snapSlider, {
			start: [ document.getElementById('slider-snap-value-lower').value, document.getElementById('slider-snap-value-upper').value],
			snap: false,
			connect: true,
			range: {
				'min': parseInt(document.getElementById('slider-snap-value-lower').dataset.min),
				'max': parseInt(document.getElementById('slider-snap-value-upper').dataset.max)
			}
		});

		snapSlider.noUiSlider.on('update', function( values, handle ) {
			snapValues[handle].value = values[handle];
		});
	}

});

$(document).on('click','.checkout_btn',function(){
	$('.checkout_btn').removeClass('active_btn');
	$(this).addClass('active_btn');
});
$(document).on('click','#read_more',function(){
	if($(this).hasClass('glyphicon-menu-down')){
		$(this).removeClass('glyphicon-menu-down');
		$(this).addClass('glyphicon-menu-up');
	}else{
		$(this).removeClass('glyphicon-menu-up');
		$(this).addClass('glyphicon-menu-down');
	}
	$('.text-block').toggleClass('increased_height');
})
$(document).on('click','.product-actions li',function(e){
	var list = $(this).find('.share_methods');
	list.addClass('show');
	list.removeClass('hidden');
	$('html').click(function(event) {
		//check up the tree of the click target to check whether user has clicked outside of menu
		if (list.hasClass('show')) {
			// your code to hide menu
			list.addClass('hidden')
			list.removeClass('show')
			//this event listener has done its job so we can unbind it.
			$(this).unbind(event);
		}

	});

})
var timeout;
$(document).on('mouseenter','.category_list',function(){
	if($(window).width() >= 768){
		clearTimeout(timeout);
		$(this).addClass('open');
		$('#w0').css('z-index','9999')
		$('.shadow_backdrop').fadeIn();
	}
});
$(document).on('mouseleave','.category_list',function(){
	if($(window).width() >= 768){
		var _this = $(this);
		$('#w0').css('z-index','1')
		$('.shadow_backdrop').fadeOut();
		timeout = setTimeout(function(){
			_this.removeClass('open');
		},1000)
	}
});


$(document).on('click','#send_call',function(){
	var text = $('#text_success').text();
	$.post('/site/make-call',$('#call_form').serialize()).success(function(result){
		if(result == ''){
			$('.make_call_modal .modal-content').html(text);
			setTimeout(function(){
				$('.make_call_modal').modal("hide");
			},1500)

		}
	})
});

var inc = -1;
$(document).on('click','#new_addr',function(){

	++inc;
	var container = $('.other_address_fields');
	var input = '<div id="'+inc+'" class="field_block">' +
		'<div>' +
		'<input class="form-control user-address" onfocus="addressAutocomplete()" name="User[address][]" />' +
		'</div>' +
		'<button class="remove-field" data-id="'+inc+'">remove</button>' +
		'</div>';

	container.append(input);
	addressAutocomplete()
});
$(document).on('click','#new_number',function(){

	++inc;
	var container = $('.other_number_fields');
	var input = '<div id="'+inc+'" class="field_block">' +
		'<div>' +
		'<input class="form-control"  name="User[number][]" />' +
		'</div>' +
		'<button class="remove-field" data-id="'+inc+'">remove</button>' +
		'</div>';

	container.append(input);
});
$(document).on('click','.remove-field',function(){
	$('#'+$(this).data('id')).remove();
	addressAutocomplete()
})
$(document).on('click','.remove-field',function(){
	var id = $(this).data('remove-id');
	var url = $(this).data('type') == 'number'? '/site/remove-number':'/site/remove-address';
	var type = $(this).data('type');
	$.ajax({
		url:url,
		method:'post',
		data:{
			id:id
		}
	}).success(function(success){
		$('#'+type+'_'+id).remove();
	}).error(function(err){
		console.error(err)
	})
})