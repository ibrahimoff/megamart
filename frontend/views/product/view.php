<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/28/16
 * Time: 11:24 AM
 */
use yii\helpers\Url;
use frontend\models\Product;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => $model->product->category->categoryTranslate->name];;
//$this->params['breadcrumbs'][] = ['label' => $model->product->manufacturer->name];
$this->params['breadcrumbs'][] = ['label' => $model->name];

Yii::$app->view->registerMetaTag(['name' => 'description','content' => $model->name.' в интернет-магазине Megamart.az. Тел: 012 404-80-99 или 055 944-47-07. '.$model->name.' - лучший сервис, быстрая доставка']);
Yii::$app->view->registerMetaTag(['name' => 'keywords','content' => str_replace(' ',',',$model->name).',купить,онлайн,интернет-магазин,интернет,магазин,доставка,гарантия,megamart,кредит']);

Yii::$app->view->registerMetaTag(['property' => 'og:type','content' => 'article']);
Yii::$app->view->registerMetaTag(['property' => 'og:title','content' => $model->name]);
Yii::$app->view->registerMetaTag(['property' => 'og:description','content' => strip_tags($model->short_description)]);
Yii::$app->view->registerMetaTag(['property' => 'og:url','content' => Url::link($model->slug,'/_p/')]);
Yii::$app->view->registerMetaTag(['property' => 'og:image','content' => Url::link(Yii::getAlias('@web/uploads/products/'.$model->productCoverImage->product_id.'-'.$model->productCoverImage->id.'-home.jpg'))]);
Yii::$app->view->registerMetaTag(['property' => 'og:image:width','content' => '200']);
Yii::$app->view->registerMetaTag(['property' => 'og:image:height','content' => '200']);
Yii::$app->view->registerMetaTag(['name' => 'twitter:description','content' => strip_tags($model->short_description)]);
Yii::$app->view->registerMetaTag(['name' => 'twitter:image','content' => Url::link(Yii::getAlias('@web/uploads/products/'.$model->productCoverImage->product_id.'-'.$model->productCoverImage->id.'-home.jpg'))]);
Yii::$app->view->registerMetaTag(['name' => 'twitter:card','content' => 'summary_large_image']);
Yii::$app->view->registerMetaTag(['name' => 'twitter:title','content' => $model->name]);


?>

<div class="row">
    <div class="col-lg-60">
        <div class="page-header text-red">
            <h1><?=$model->name?></h1>
        </div>
    </div>
</div>
<section id="product">


    <div class="row">
        <div class="col-lg-36 col-md-35  col-sm-50 col-xs-60 text-center">
            <img id="product_img" class="img-responsive" style="max-width:450px;" src="<?=Yii::getAlias('@web/uploads/products/'.$model->productCoverImage->product_id.'-'.$model->productCoverImage->id.'-thickbox.jpg')?>" alt=""/>
        </div>
        <div class="col-lg-6 col-md-7 col-sm-9 col-xs-60">
            <div class="row">
                <?php foreach($model->product->productImages as $productImage):?>
                    <div class="col-lg-60 col-md-60 col-sm-60 col-xs-20 text-center">
                        <a class="product_small_img <?=$productImage->cover == 1?'active':''?>" href="<?=Yii::getAlias('@web/uploads/products/').$productImage->product_id.'-'.$productImage->id.'-thickbox.jpg'?>">
                            <img src="<?=Yii::getAlias('@web/uploads/products/').$productImage->product_id.'-'.$productImage->id.'-medium.jpg'?>" alt=""/>
                        </a>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
        <div class="col-lg-18 col-md-18  col-sm-60 col-xs-60 pull-right">
            <div class="card-product-desc">
                    <div class="articul"><?=sprintf(Yii::t('frontend','articul'),'IPHA-047')?></div>
                    <?php if(!empty($model->discount)):?>
                        <div class="product-price">
                        <div>
                            <span class="past-price"><?=$model->product->price?><i class="mm-icon-azn"></i></span>
                        </div>
                       <div>
                           <span class="present-price"><?=$model->discount->price?><i class="mm-icon-azn"></i></span>
                       </div>
                       </div>
                    <?php else:?>
                    <div class="product-price vertical_aligned">
                        <span class="present-price "><?=$model->product->price?><i class="mm-icon-azn"></i></span>
                    </div>
                    <?php endif?>

                <div class="bonus"><?=sprintf(Yii::t('frontend','get_bonus'),$model->product->bonus)?></div>
                <div class="bonus"><?=Yii::t('frontend','delivery')?> <span style="color: #000;position: relative"><span class="present-price">5<i class="mm-icon-azn"  style="top: 1px!important;"></i></span></span></div>
                <a href="#" class="btn btn-primary buyButton <?=in_array($model->product_id,\frontend\models\Cart::checkCard())? 'disabled' :''?>" id="buy<?=$model->product_id?>" data-id="<?=$model->product_id?>"  tabindex="0"><?=Yii::t('frontend','buy_button')?></a>
                <div class="availability">
                    <?php if($model->product->availability == '1'):?>
                        <?=Yii::t('frontend','available')?>
                    <?php else:?>
                        <?=Yii::t('frontend','not_available')?>
                    <?php endif?>
                </div>

                <ul class="product-actions">
                    <li><a href="#" tabindex="0"><i class="mm-icon-like"></i></a> 451</li>
                    <li><a href="#" tabindex="0"><i class="mm-icon-share"></i></a> 97</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<div class="hidden">
    <!--                            <i class="mm-icon-pin"></i></a> <span class="count">--><?//=Product::getPinCount($model->product_id)?><!--</span></li>-->
    <!--<li><a --><?php //if(!yii::$app->user->isGuest):?><!-- class="pin-product --><?//=$model->pins ? 'active':''?><!--" data-id="--><?//=$model->product_id?><!--" href="#"--><?php //endif;?><!-- tabindex="0">-->

</div>

<section id="about_product">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#overview" data-toggle="tab" aria-expanded="true"><?=Yii::t('frontend','specifications')?></a></li>
        <li class=""><a href="#requirements" data-toggle="tab" aria-expanded="false"><?=Yii::t('frontend','warranty')?></a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="overview">
            <div class="row">
                <div class="col-lg-36">
                    <div style="margin-top: 30px">
                        <?=$model->description?>

                    </div>
                </div>
                <div class="col-lg-24">
                    <div style="height: 500px;overflow-y: auto;overflow-x: hidden">
                        <div class="fb-comments" data-href="<?=Yii::$app->params['url'].Yii::$app->language.Yii::$app->request->url?>" data-numposts="5"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="requirements">
            <div class="row">
                <div class="col-lg-36">
                    <table class="table table-bordered" style="margin-top: 30px">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>
</section>
<?php if(!empty($accessoryWidget)){?>

    <section id="buyed-with-this">

        <div class="row">
            <div class="page-header black-header">
                <div class="line-bg">
                    <h5 id="buttons"><?=Yii::t('frontend','accessory')?></h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-60">

                <div class="accessory-slider ">
                    <?=$accessoryWidget?>
                </div>

            </div>
        </div>

    </section>
<?php }?>
<?php if(!empty($randomItemsWidget)){?>
<section id="more-like-this">
    <div class="row">
        <div class="page-header black-header">
            <div class="line-bg">
                <h5 id="buttons"><?=Yii::t('frontend','same_products')?></h5>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-60">

            <div class="items-slider">
                <?=$randomItemsWidget?>
            </div>
        </div>

    </div>
</section>
<?php }?>

