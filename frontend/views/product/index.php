<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/25/16
 * Time: 1:57 PM
 */
$this->title = $category->name;
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header-block text-red" >
                <h1 class="checkout_headers heading"><?=$this->title?></h1>
            </div>
        </div>
        <div class="col-lg-13 col-md-16 col-sm-20 col-xs-60">
            <?=$filterWidget?>
        </div>



        <div class="col-lg-47 col-md-44 col-sm-40 col-xs-60">
            <?php if(!empty(Yii::$app->request->get('Filter'))):?>
                <?php if(!empty($products)):?>
                        <div class="row">
                            <?=$this->render('../site/_productItem',['products'=>$products])?>
                        </div>
                <?php endif?>

            <?php else:?>
                    <div class="row">
                        <?=$productWidget?>
                    </div>

                <center class="row">
                    <div>
                        <?php

                        echo \yii\widgets\LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>

                    </div>
                </center>

            <?php endif?>
            <div>
                <?=$description?>
            </div>  
        </div>

    </div>
