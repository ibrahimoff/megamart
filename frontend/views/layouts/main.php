<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php
        Yii::$app->view->registerMetaTag(['name' => 'description','content' => 'Megamart.az - Интернет-магазин бытовой техники и электроники']);
        Yii::$app->view->registerMetaTag(['name' => 'keywords','content' => str_replace(' ',',','Megamart.az Интернет-магазин бытовой техники и электроники').',купить,онлайн,интернет-магазин,интернет,магазин,доставка,гарантия,megamart,кредит']);

        Yii::$app->view->registerMetaTag(['property' => 'og:type','content' => 'article']);
        Yii::$app->view->registerMetaTag(['property' => 'og:title','Megamart.az']);
        Yii::$app->view->registerMetaTag(['property' => 'og:description','Интернет-магазин бытовой техники и электроники']);
        Yii::$app->view->registerMetaTag(['property' => 'og:url','content' => 'http://'.$_SERVER['HTTP_HOST'].'/']);
        Yii::$app->view->registerMetaTag(['property' => 'og:image','content' => '']);
        Yii::$app->view->registerMetaTag(['name' => 'twitter:description','content' => 'Интернет-магазин бытовой техники и электроники']);
        Yii::$app->view->registerMetaTag(['name' => 'twitter:image','content' => '']);
        Yii::$app->view->registerMetaTag(['name' => 'twitter:card','content' => 'summary_large_image']);
        Yii::$app->view->registerMetaTag(['name' => 'twitter:title','Megamart.az']);
    ?>
    <?php $this->head() ?>

</head>

<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5&appId=422815147908429";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php $this->beginBody() ?>
<?php

?>
<div id="wrapper">

    <div id="page-content-wrapper">
        <header>
            <?=\frontend\widgets\Header\Header::widget()?>
            <?=\frontend\widgets\Search\Search::widget()?>

            <?php NavBar::begin([

                'options' => [
                    'class' => 'navbar navbar-default hidden-xs',

                ],

            ]);

            $menuItems[] = ['label' => 'Сравнения', 'url' => \yii\helpers\Url::link('compare/index')];

            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Signup', 'url' => \yii\helpers\Url::link('signup')];
                $menuItems[] = ['label' => 'Login', 'url' =>\yii\helpers\Url::link('login')];
            } else {
                $menuItems[] = ['label' => 'Edit', 'url' => \yii\helpers\Url::link('edit-profile')];
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->firstname .' '.Yii::$app->user->identity->lastname .')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            $menuItemsRight = [];
            $menuItemsRight[] = ['label' => 'Скидки', 'url' => \yii\helpers\Url::link('discounts'),'linkOptions' => ['class'=>'text-red']];

            echo \frontend\widgets\Categories\Categories::widget();
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-left top_categories','id'=>'#menu'],
                'items' => \frontend\models\Category::getTopCategories(),
                'dropDownCaret'=>'<span style="font-size: 11px;" class="glyphicon glyphicon-align-justify"></span>',
            ]);
            NavBar::end();
            ?>
            <div class="visible-xs mobile-navbar">
                <?=\frontend\widgets\Categories\Categories::widget();?>
            </div>

        </header>
        <div class="container-fluid">

            <?= Alert::widget() ?>
             <div class="content_wrapper">
                 <div class="hidden-xs">
                     <?= Breadcrumbs::widget([
                         'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                     ]) ?>
                 </div>

                 <?= $content ?>

             </div>
        </div>

        <footer>
            <div class="container-fluid">
            <div class="content_wrapper">
                    <div class="row">
                        <div class="col-lg-42 col-md-30 col-sm-30 col-xs-60 pull-right">
                            <div class="row">
                                <div class="col-lg-60">
                                    <div class="inline_menu">
                                        <?=\frontend\widgets\StaticPages\StaticPages::widget()?>
                                        <ul class="social_block_icons">
                                            <li><a href=""><i class="fa fa-facebook-official"></i></a></li>
                                            <li><a href=""><i class="fa fa-google-plus-square"></i></a></li>
                                            <li><a href=""><i class="fa fa-twitter-square"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-18 col-md-30 col-sm-30 col-xs-60 pull-right">
                            <div class="row">
                                <div class="col-lg-60">
                                    <p class="copyright">© 2010-<?=date('Y')?>. <?=Yii::t('frontend','megamart_info')?></p>
                                    <p><?=Yii::t('static','full_number')?></p>
                                    <p><?=Yii::t('static','phone')?></p>
                                    <div>
                                        <img src="<?=Yii::getAlias('@web/img/idealand.png')?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </footer>
        <div class="modal fade make_call_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content" style="padding: 20px">
                    <h3><?=Yii::t('frontend','call_me')?></h3>
                    <?php $model = new \frontend\models\Calls();$form = \yii\widgets\ActiveForm::begin(['id' => 'call_form']); ?>

                    <?= $form->field($model, 'name')->textInput(['autocomplete'=>'off']) ?>
                    <?= $form->field($model, 'number')->widget(\yii\widgets\MaskedInput::className(), [
                        'mask' => '999-999-9999',
                    ]) ?>
                    <div class="form-group">
                        <?= Html::button('Send', ['class' => 'btn btn-primary', 'id'=>"send_call"]) ?>
                    </div>

                    <?php \yii\widgets\ActiveForm::end(); ?>

                </div>
                <div id="text_success"><?=Yii::t('frontend','will_call_to_you')?></div>
            </div>
        </div>
        <div class="shadow_backdrop"></div>
        <?=\frontend\widgets\Bucket\Bucket::widget()?>
    </div>
</div>
<?php $this->endBody() ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/570f34396691493d4aeeeaad/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
<script src="https://apis.google.com/js/platform.js" async defer></script>

</body>
</html>
<?php $this->endPage() ?>
