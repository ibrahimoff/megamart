<?php
use yii\helpers\Html;
$level = 0;
$id = '';
$class = '';
echo Html::beginTag('ul',['class'=>'nav navbar-nav']);
foreach ($categories as $n => $category) {

    if ($category->level == $level){
        echo Html::endTag('li') . "\n";
    }else if ($category->level > $level){

        echo Html::beginTag('ul',['id'=>$id,'class'=>$class]) . "\n";
        $class='hidden';


    }   else {
        echo Html::endTag('li') . "\n";
        for ($i = $level - $category->level; $i; $i--) {
            echo Html::endTag('ul') . "\n";
            echo Html::endTag('li') . "\n";
        }
    }
    if($category->parent_id == 0){
        $id=$category->id;
        echo Html::beginTag('li',['data-parent-id'=>$category->id,'class'=>'parent-node']);
    }else{
        echo Html::beginTag('li');
    }

    echo Html::beginTag('a',['href'=>\yii\helpers\Url::link($category->categoryTranslate->slug,'by-category')]).Html::encode($category->categoryTranslate->name).Html::endTag('a');

    $level = $category->level;
}

for ($i = $level; $i; $i--) {
    echo Html::endTag('li') . "\n";
    echo Html::endTag('ul') . "\n";
}
echo Html::endTag('ul') . "\n";
