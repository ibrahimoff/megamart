<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 3/16/16
 * Time: 11:03 AM
 */
$totalPrice = 0;
$this->title = Yii::t('frontend','shopping_history');
?>
<div class="row">
    <div class="col-lg-60">
        <div class="page-header-block text-red">
            <h1 class="checkout_headers"><?= $this->title ?></h1>
        </div>
    </div>
</div>
<?php foreach($orders as $order):?>

<div class="col-lg-60">
    <div class="white_block">
        <table class="table table_headers table-bordered">
            <thead>
            <tr>
                <th><?=Yii::t('frontend','date')?></th>
                <th><?=Yii::t('frontend','order_number')?></th>
                <th><?=Yii::t('frontend','item')?></th>
                <th><?=Yii::t('frontend','price')?></th>
                <th><?=Yii::t('frontend','quantity')?></th>
                <th><?=Yii::t('frontend','bonus')?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($order->orderDetails as $detail):?>
                <?php $totalPrice+=$detail->price*$detail->quantity?>
                <tr>
                    <td><?=$order->created_at?></td>
                    <td><?=$detail->order_id?></td>
                    <td><a href="<?=\yii\helpers\Url::link($detail->productName->slug,'_p')?>">
                            <?=$detail->productName->name?>
                        </a></td>
                    <td class="text-center price_tag"><?=$detail->price?><i class="mm-icon-azn"></i></td>
                    <td class="text-center"> <?=$detail->quantity?></td>
                    <td class="text-center"><?=$detail->bonus?></td>

                </tr>
            <?php endforeach;?>


            </tbody>
        </table>
    </div>
</div>
<?php endforeach?>
