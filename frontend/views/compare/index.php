<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 3/17/16
 * Time: 12:37 PM
 */
?>
<?php if(!empty($products)):?>
<?php foreach($products as $product):?>
    <div class="col-lg-30">
        <div class="text-center">
            <div>
                <?=$product->name?>
            </div>
            <div>
                <img src="<?=Yii::getAlias('@web/uploads/products/'.$product->productCoverImage->product_id.'-'.$product->productCoverImage->id.'-home.jpg')?>" alt="">
            </div>
            <div>
                Brand : <?=$product->product->manufacturerName->name?>
            </div>
            <div>
                Price : <?=$product->product->price?>
            </div>
            <?php if(!empty($product->discount->price)):?>
            <div>
                Discount price : <?=$product->discount->price?>
            </div>
            <?php endif?>
            <div>
                Bonus : <?=$product->product->bonus?>
            </div>
        </div>
    </div>
<?php endforeach?>
<?php else:?>
    <h1>nothing to compare</h1>
<?php endif?>
