<?php
/* @var $this yii\web\View */
$this->title = 'Корзина';
?>
<section id="basket">
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header text-red">
                <h1><?=$this->title?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-60">
			<div class="row">
				<div class="page-header green-header">
					<div class="col-lg-20">
					    <h5>ТОВАР</h5>
					</div>
					<div class="col-lg-12">
					    <h5>ЦЕНА</h5>
					</div>
					<div class="col-lg-12">
					    <h5>КОЛИЧЕСТВО</h5>
					</div>
					<div class="col-lg-12">
					    <h5>СУММА</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="basket-item">
					<div class="col-lg-20">
					    <div class="row">
					    	<div class="col-lg-30">
					    		<a href="#"><img class="img-responsive" src="http://52.73.2.239/uploads/products/22730-41941-home.jpg" alt=""></a>
					    	</div>
					    	<div class="col-lg-30">
					            <div class="product-title">
					              <a href="#"><h5>iPhone 6S 64 GB</h5></a>
					            </div>
								<div class="push20"></div>
					            <div class="product-bonus">
					            	<h5>104 бонуса</h5>
					            </div>
								<div class="push20"></div>
					            <div class="articul">
					            	Артикул: IPHA-047
					            </div>
					    	</div>
					    </div>
					</div>
					<div class="col-lg-12">
			            <div class="product-price">
				            <span class="past-price">539<i class="mm-icon-azn"></i></span>
				            <span class="present-price">449<i class="mm-icon-azn"></i></span>
				          </div>
					</div>
					<div class="col-lg-12">
            <div class="valign-wrapper basket-height">
					    <div class="bucket-item-count" data-item-id="22730">
				            <i class="fa fa-angle-up fa-2"></i>
				            <input min="1" type="number" value="1">
				            <i class="fa fa-angle-down fa-2"></i>
				        </div>
              </div>
					</div>
					<div class="col-lg-12">
			            <div class="product-price-sum">
			              <span class="present-price">449<i class="mm-icon-azn"></i></span>
			            </div>
						      <div class="push20"></div>
			            <div class="product-bonus">
			            	<h5>100094 бонуса</h5>
			            </div>
					</div>
          <div class="col-lg-4">
            <div class="valign-wrapper basket-height">
              <a href="#" class="bucket-item-remove"><i class="fa fa-times fa-2" data-item-id="22730"></i></a>
            </div>
          </div>
				</div>
				<div class="basket-item">
					<div class="col-lg-20">
					    <div class="row">
					    	<div class="col-lg-30">
					    		<a href="#"><img class="img-responsive" src="http://52.73.2.239/uploads/products/22730-41941-home.jpg" alt=""></a>
					    	</div>
					    	<div class="col-lg-30">
					            <div class="product-title">
					              <a href="#"><h5>iPhone 6S 64 GB</h5></a>
					            </div>
								<div class="push20"></div>
					            <div class="articul">
					            	Артикул: IPHA-047
					            </div>
					    	</div>
					    </div>
					</div>
					<div class="col-lg-12">
			            <div class="product-price">
				            <span class="past-price">539<i class="mm-icon-azn"></i></span>
				            <span class="present-price">449<i class="mm-icon-azn"></i></span>
				        </div>
					</div>
					<div class="col-lg-12">
            <div class="valign-wrapper basket-height">
              <div class="bucket-item-count" data-item-id="22730">
                  <i class="fa fa-angle-up fa-2"></i>
                  <input min="1" type="number" value="1">
                  <i class="fa fa-angle-down fa-2"></i>
              </div>
            </div>
					</div>
					<div class="col-lg-12">
			            <div class="product-price-sum">
			              <span class="present-price">449<i class="mm-icon-azn"></i></span>
			            </div>
						      <div class="push20"></div>
			            <div class="product-bonus">
			            	<h5>194 бонуса</h5>
			            </div>
					</div>
          <div class="col-lg-4">
            <div class="valign-wrapper basket-height">
              <a href="#" class="bucket-item-remove"><i class="fa fa-times fa-2" data-item-id="22730"></i></a>
            </div>
          </div>
				</div>
			</div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-60">
			<div class="row">
				<div class="page-header black-header">
					<div class="col-lg-44">
					    <h5>АДРЕС ДОСТАВКИ</h5>
					</div>
					<div class="col-lg-16">
					    <h5>ИТОГО</h5>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="basket-sum">
					<div class="col-lg-44">
					    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti tempora, saepe incidunt officia natus reprehenderit voluptatem explicabo qui voluptatum cupiditate, ad iste vel iusto at voluptas temporibus optio dignissimos perspiciatis!</p>
					</div>
					<div class="col-lg-16">
			            <div class="product-price-sum">
			              <span class="present-price">1000<i class="mm-icon-azn"></i></span>
			            </div>
						      <div class="push20"></div>
			            <div class="product-bonus text-red">
			            	<h5>100094 бонуса</h5>
			            </div>
      						<div class="push20"></div>
      						<a href="#" class="btn btn-primary" tabindex="0">ОФОРМИТЬ</a>
					</div>
				</div>
			</div>
        </div>
    </div>
</section>

<section id="buyed-with-this">
  <div class="row">
    <div class="col-lg-60">
      <div class="page-header">
        <h5>С ЭТИМ ТОВАРОМ ЧАСТО ПОКУПАЮТ</h5>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-60">

      <div class="items-slider">

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/26980-68562-large/-robello-jng24.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/36617-68488-large/-dahua-ca-dw181e.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/26980-68562-large/-robello-jng24.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/36617-68488-large/-dahua-ca-dw181e.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/26980-68562-large/-robello-jng24.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product ">
          <div class="product-img">
            <img src="https://megamart.az/36617-68488-large/-dahua-ca-dw181e.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

        <div class="card-product">
          <div class="product-img">
            <img src="https://megamart.az/36617-68488-large/-dahua-ca-dw181e.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-title">
              <h5>iPhone 6S 64 GB</h5>
            </div>
            <div class="product-price">
              <span class="present-price">449<i class="mm-icon-azn"></i></span>
            </div>
            <ul class="product-actions">
              <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
              <li><a href="#"><i class="mm-icon-pin"></i></a> 13</li>
              <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
            </ul>
            <a href="#" class="btn btn-primary">Купить</a>
          </div>
        </div>

      </div>
    </div>

  </div>
</section>