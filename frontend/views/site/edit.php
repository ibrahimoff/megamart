<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 12/23/15
 * Time: 10:12 AM
 */
$this->title = Yii::t('frontend','edit_profile');
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\datetime\DateTimePicker;
?>

<script src="/js/autocomplete.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=Yii::$app->params['GOOGLE_API_KEY']?>&signed_in=true&libraries=places&callback=initAutocomplete"
        async defer></script>
<div class="row">
    <div class="col-lg-60">
        <div class="page-header-block text-red">
            <h1 class="checkout_headers"><?= $this->title ?></h1>
        </div>
    </div>
</div>
<?php if(Yii::$app->session->hasFlash('success')){
    echo Yii::$app->session->getFlash('success');
}?>
<div>
    <div class="white_block">
        <div class="padding20">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class' => 'edit-profile']]); ?>
            <div class="client_code"><?=Yii::t('frontend','client_code')?>: <?=Yii::$app->user->id?></div>

            <div class="col-lg-29 col-md-29 text-center">
                <div>
                    <img src="<?=Yii::getAlias('@web/uploads/users/').$user->image?>" alt="" style="border-radius: 100%;"/>
                </div>
                <?= $form->field($user, 'file')->widget(\kartik\file\FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions'=>[
                        'showUpload'=>false
                    ]
                ]);?>
                <?= $form->field($user, 'date_birtday')->widget(
                    \dosamigos\datepicker\DatePicker::className(), [
                    'inline' => true,
                    'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);?>
                <?=$form->field($user,'newsletter')->checkbox()?>
            </div>
            <div class="col-lg-30 col-md-30">
                <?= $form->field($user, 'firstname') ?>
                <?= $form->field($user, 'lastname')?>
                <?php if(!empty($user->numbers)):?>
                    <?php foreach($user->numbers as $number):?>
                        <div id="number_<?=$number->id?>">
                            <?= $form->field($user, 'number['.$number->id.']['.$number->number.']')->textInput(['value'=>!empty($number->number) ? $number->number : '','class'=>'form-control'])?>
                            <button type="button" class="remove-field" data-type="number" data-remove-id="<?=$number->id?>">remove</button>

                        </div>

                    <?php endforeach?>
                <?php else:?>
                    <?= $form->field($user, 'number[]')->textInput(['value'=>!empty($number->number) ? $number->number : '','class'=>'form-control'])?>
                <?php endif?>
                <div class="other_number_fields">
                </div>
                <button id="new_number" type="button">add number</button>


                <?php if(!empty($user->addr)):?>
                    <?php foreach($user->addr as $address):?>
                        <div id="address_<?=$address->id?>">
                            <?= $form->field($user, 'address['.$address->id.']['.$address->address.']')->textInput(['onfocus'=>"addressAutocomplete()",'value'=>!empty($address->address) ? $address->address : '','class'=>'user-address form-control'])?>
                            <button type="button" class="remove-field" data-type="address" data-remove-id="<?=$address->id?>">remove</button>
                        </div>

                    <?php endforeach?>
                <?php else:?>
                    <?= $form->field($user, 'address[]')->textInput(['onfocus'=>"addressAutocomplete()",'value'=>!empty($address->address) ? $address->address : '','class'=>'user-address form-control'])?>
                <?php endif?>

                <div class="other_address_fields"></div>
                <button id="new_addr" type="button">add address</button>


                <?= $form->field($user, 'gender')->radioList(['1'=>Yii::t('frontend','male'),'2'=>Yii::t('frontend','female')])?>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-lg-30 col-lg-offset-29 col-md-30 col-md-offset-29">
                <?php $model = new \frontend\models\ChangePassForm();
                $form = ActiveForm::begin(['id' => 'changePassForm','action'=>'/site/changepassword','options'=>['class'=>'edit-profile']]); ?>
                <?= $form->field($model, 'oldPassword')->passwordInput() ?>
                <?= $form->field($model, 'newPassword')->passwordInput() ?>
                <?= $form->field($model, 'confirmPassword')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('frontend','save'), ['class' => 'btn btn-primary', 'name' => 'changePassBtn']) ?>
                </div>
                <?php ActiveForm::end(); ?>
                <div class="clearfix"></div>

            </div>
        </div>





        <div class="clearfix"></div>

    </div>








</div>