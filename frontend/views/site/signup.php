<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('frontend','Signup');
?>
<div class="row">
    <div class="col-lg-60">
        <div class="page-header-block text-red">
            <h1 class="checkout_headers"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
</div>
<div class="site-signup">
    <div class="button_checkout" style="">
        <div class="btn-block-tabs">
            <button class="checkout_btn active_btn"
                    data-step-id="signup"><?= Yii::t('frontend', 'signup_as_user') ?>
            </button>
            <button class="checkout_btn" data-step-id="without_signup"
                    style="margin-left: -5px;"><?= Yii::t('frontend', 'signup_as_company') ?>
            </button>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-26 col-lg-offset-2">
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                <?= $form->field($model, 'firstname') ?>

                <?= $form->field($model, 'lastname') ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
