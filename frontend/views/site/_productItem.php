<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:32 PM
 */
$compareArray = is_array(Yii::$app->session['compare'])?Yii::$app->session['compare']:[];
?>


<?php if(!empty($products)):?>

    <?php foreach($products as $product):?>
        <div class="col-lg-11 col-md-18 col-sm-23 col-xs-60">
            <div class="card-product white">
                <div class="product-img">
                    <a href="<?=\yii\helpers\Url::link($product['_source']['_product']['slug'],'_p')?>">
                        <?php if($product['_source']['_product']['productOnly']['on_sale'] == 1):?>
                            <span class="discount"><?=\frontend\models\Product::getDiscountPercent($product['_source']['_product']['discount']['old_price'],$product['_source']['_product']['discount']['price'])?></span>
                        <?php endif?>
                        <img src="<?=Yii::getAlias('@web/uploads/products/').$product['_source']['_product']['productCoverImage']['product_id'].'-'.$product['_source']['_product']['productCoverImage']['id'].'-large.jpg'?>" alt="">
                    </a>
                </div>
                <div class="product-description">
                    <div class="product-title">
                        <h5><a href="<?=\yii\helpers\Url::link($product['_source']['_product']['slug'],'_p')?>"><?=$product['_source']['_product']['name']?></a></h5>
                    </div>
                    <div class="product-price">
                        <?php if($product['_source']['_product']['productOnly']['on_sale'] == 1):?>
                            <span class="past-price"><?=$product['_source']['_product']['discount']['old_price']?><i class="mm-icon-azn"></i></span>
                            <span class="present-price"><?=$product['_source']['_product']['discount']['old_price']?><i class="mm-icon-azn"></i></span>
                        <?php else:?>
                            <span class="present-price"><?=$product['_source']['_product']['productOnly']['price']?><i class="mm-icon-azn"></i></span>
                        <?php endif?>
                    </div>
                    <div class="btn-group-custom">
                        <a href="#" class="btn btn-primary buyButton" data-id="<?=$product['_source']['_product']['product_id']?>"><?=Yii::t('frontend','Купить')?></a>
                        <span class="btn btn-info <?=in_array($product['_source']['_product']['product_id'],$compareArray)?'remove':''?> compareButton" id="<?=$product['_source']['_product']['product_id']?>"><?=in_array($product['_source']['_product']['product_id'],$compareArray)?Yii::t('frontend','remove_button'):Yii::t('frontend','compare_button')?></span>

                    </div>

                    <ul class="product-actions">
                        <li><a href="#"><i class="mm-icon-like"></i></a> 451</li>
                        <li><a href="#"><i class="mm-icon-share"></i></a> 97</li>
                    </ul>
                </div>
            </div>
        </div>

    <?php endforeach;?>
<?php endif?>

