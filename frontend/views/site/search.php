<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/30/16
 * Time: 3:39 PM
 */

?>
<div class="row">
    <div style="margin-top: 30px;">
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header-block text-red">
                <h1 class="checkout_headers"><?=sprintf('Найдено %s товаров по запросу %s',count($result),Yii::$app->request->get('name'))?></h1>
            </div>
        </div>
    </div>
    <div style="margin-bottom: 50px;">
        <div class="dropdown">
            <button class="btn btn-info dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                Dropdown
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="&sort=_product.productOnly.price:asc">Price lower first</a></li>
                <li><a href="&sort=_product.productOnly.price:desc">Price higher first</a></li>
            </ul>
        </div>
    </div>


            <?=$this->render('_productItem',['products'=>$result])?>
        </div>
</div>

