<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error white_block padding20">

        <h1><?= Html::encode($this->title) ?></h1>

        <h3 class="">
            <?= nl2br(Html::encode($message)) ?>
        </h3>

        <p>
            The above error occurred while the Web server was processing your request.
        </p>
        <p>
            Please contact us if you think this is a server error. Thank you.
        </p>
</div>
