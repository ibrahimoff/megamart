<?php

/* @var $this yii\web\View */


$this->title = 'Megamart.az';
?>

<?=$hotDealsWidget?>
<?=$saleOutWidget?>
<?=$bannersWidget?>
<?=$latestProductsWidget?>
<?=$bestsellersWidget?>

<div class="row text-center">
    <div class="col-lg-60">
        <div class="page-header black-header">
            <div class="line-bg">
                <h5 id="buttons"><?=Yii::t('frontend','test')?></h5>
            </div>
        </div>
    </div>
    <div style="margin-top: 25px;display: inline-block;">
        <?php if(!empty($footerBanner)):?>
            <?php foreach($footerBanner as $banner):?>
                <div class="col-lg-15">
                    <a href="//<?=$banner->link?>" class="inner_page"><img src="<?=Yii::getAlias('@web/uploads/banners/'.$banner->banner->image)?>" class="img-responsive" alt="">
                        <h3><?=$banner->text?></h3>

                    </a>
                </div>
            <?php endforeach?>
        <?php endif?>
    </div>

    <div class="clearfix"></div>
</div>

<div class="text-wrapper">
    <div class="col-lg-60">
        <div class="page-header black-header">
            <div class="line-bg">
                <h5 id="buttons"><?=Yii::t('frontend','test')?></h5>
            </div>
        </div>
    </div>
    <div class="col-lg-60">
        <div class="text-block">
            <?=Yii::t('static','main_text')?>
        </div>
        <div class="background-block">
            <button id="read_more" class="glyphicon glyphicon-menu-down"></button>
        </div>
    </div>

</div>

