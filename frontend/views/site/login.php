<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="row">
    <div class="col-lg-60">
        <div class="page-header-block text-red">
            <h1 class="checkout_headers"><?= Html::encode($this->title) ?></h1>
        </div>
    </div>
</div>
<div class="site-login">
    <div class="vertical_line hidden-xs">

    </div>
    <div class="white_block">

            <span class="hidden-xs">Login</span>
            <span class="hidden-xs" style="border-right: 0;margin-left: -5px;"><?= Yii::t('frontend', 'social_login') ?></span>


        <div class="row">
            <div class="col-lg-26 col-lg-offset-2 col-md-26 col-md-offset-2 col-sm-26 col-sm-offset-2">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                <?= $form->field($model, 'username') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'rememberMe')->checkbox() ?>

                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-lg-10 col-lg-offset-11 col-md-15 col-md-offset-10 col-sm-15 col-sm-offset-10 col-xs-30 col-xs-offset-15">
                <?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>
            </div>
        </div>
    </div>



</div>
