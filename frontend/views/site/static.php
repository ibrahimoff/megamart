<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/18/16
 * Time: 4:58 PM
 */
$this->title = $pageContent['name'];
$this->params['breadcrumbs'][] = ['label' => $pageContent['name']];
?>
<section id="static">
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header text-red">
                <h1><?=$pageContent['name']?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-60">
        	<?=$pageContent['description']?>
        </div>
    </div>
</section>