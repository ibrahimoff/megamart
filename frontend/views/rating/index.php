<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Ratings';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="" style="text-align: center;">
    <div class="rating-list">
        <?php if($ratings):?>
        <?php foreach($ratings as $rating):?>
            <div class="rating-item">
                <?php
                echo StarRating::widget([
                    'name' => 'Rating[rating]',
                    'value'=>$rating->rating,
                    'disabled'=>!$rating->mine,
                    'pluginOptions' => [
                        'min' => 0,
                        'max' => 5,
                        'step' => 1,
                        'size' => 'md',
                    ],
                    'pluginEvents'=>[
                        'rating.change'=>"function(){
                            var _this = $(this);
                            var item = _this.parents('.rating-item');
                            var token = $('.rating-item > i.fa').data('token');
                            var rating = $(this).val();

                            $.ajax({
                                url: '/rating/rate?token=' + token,
                                dataType: 'json',
                                type: 'get',
                                data: {'rating':rating},
                                success: function (data) {
                                    if (data.created == 1) {

                                    }
                                },
                                error: function (data) {
                                    //
                                }
                            });

                        }"
                    ]
                ]);?>
                <?php if($rating->mine):?>
                    <i data-token="<?=$rating->token?>" class="fa fa-2x fa-pencil-square-o"></i>
                <?php endif;?>
                <div class="user"><?=$rating->user->fullname?></div>
                <span class="review-comment">
                    <?=$rating->comment?>
                </span>
            </div>
        <?php endforeach;?>
        <?php else:?>
            <div class="alert alert-info">There is no any rating yet.
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>
        <?php endif;?>

    </div>
    </div>

<div id="ratingEditModal" class='modal fade' role='dialog'> <div class='modal-dialog'></div>
