<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use kartik\rating\StarRating;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Rate';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="col-lg-60">
    <div class="rate-order">
        <?php $form = ActiveForm::begin(['id' => 'rate-order-form']); ?>

        <?php // Control the number of stars, each caption value, and styles for display
        echo StarRating::widget([
            'name' => 'Rating[rating]',
            'pluginOptions' => [
                'min' => 0,
                'max' => 5,
                'step' => 1,
                'size' => 'sm',
                'required'=>true,
                'starCaptions' => [
                    1 => 'Very Poor',
                    2 => 'Poor',
                    3 => 'Ok',
                    4 => 'Good',
                    5 => 'Very Good',
                ],
                'starCaptionClasses' => [
                    1 => 'text-danger',
                    2 => 'text-warning',
                    3 => 'text-info',
                    4 => 'text-primary',
                    5 => 'text-success',
                ],
            ],
        ]);?>

        <textarea class="rating-comment" rows="4" required="required" name="Rating[comment]"></textarea>

        <div class="form-group">
            <button data-token="<?=$model->token?>" type="button" class="btn btn-primary">Rate</button>
        </div>


        <?php ActiveForm::end(); ?>

    </div>
    </div>

