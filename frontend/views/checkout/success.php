<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 4/1/16
 * Time: 12:24 PM
 */
?>
<div class="white_block centered success_block">
    <h2 class="text-center black-header"><?=$message?></h2>
    <div class="text-center">
        <span class="tick_img"><img src="<?=Yii::getAlias('@web/img/tick.png')?>" alt=""/></span>
    </div>
    <div class="side_pic left_pic">
        <img src="<?=Yii::getAlias('@web/img/group-3@3x.png')?>" alt=""/>
    </div>
    <div class="black_text">
        <div class="text-center"><?=sprintf($content,$orderId)?></div>
    </div>
    <div class="footer_text_success">
        <div class="text-center"><?=$text?></div>
    </div>
    <div class="side_pic right_pic">
        <img src="<?=Yii::getAlias('@web/img/group-2@3x.png')?>" alt=""/>
    </div>
    <div class="text-center">
        <a href="/<?=Yii::$app->language?>/" class="btn btn-primary" style="width: 100%;max-width: 280px;margin: 0 auto;">Continue shopping</a>
    </div>

</div>
