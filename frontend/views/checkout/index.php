<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/13/16
 * Time: 6:21 PM
 */
?>


<div class="container">
    <?php foreach($orders as $order):?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <span>No<?=$order->id?></span>
                <span>Price <?=$order->total?></span>
                <span>Points <?=$order->total_points?></span>
                <?php if($order->status == '0'){
                    echo  '<span>pending</span>';
                }elseif($order->status == '1'){
                    echo '<span >seen</span>';
                }elseif($order->status == '2'){
                    echo '<span >confirmed</span>';
                }elseif($order->status == '3'){
                    echo '<span >declined</span>';
                }?>
            </div>
            <div class="panel-body">
                <?php foreach($order->orderDetails as $item):?>
                <div>
                    <span><?=$item->orderItemLang->name?></span>
                    <div>
                        <span>QUANTITY x <?=$item->quantity?> = <?=$item->quantity*$item->price?></span>
                    </div>

                </div>
                <?php endforeach?>
            </div>
        </div>

    <?php endforeach?>
</div>

