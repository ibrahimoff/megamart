<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/10/16
 * Time: 1:18 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php if(Yii::$app->user->isGuest):?>
    <div class="panel panel-default">
        <div class="panel-heading">step 1</div>
        <div class="panel-body">
            <div>
                <button class="site_enter_button" data-type="signup">signup</button>
            </div>
            <div>
                <button class="site_enter_button" data-type="without_signup">without signup</button>
            </div>
            <div>
                <button class="site_enter_button" data-type="login">login</button>
            </div>
        </div>
    </div>



<?php elseif(Yii::$app->user->id != null || Yii::$app->session['guest_id'] != null):?>
    <div>
        <div class="address-form">

            <div class="row">
                <div class="col-lg-25">
                    <?php $form = ActiveForm::begin(['id' => 'address-form']); ?>

                    <?= $form->field($model, 'address')->dropDownList(['0'=>'aaaaaaa','1'=>'aassssssssss']) ?>
                    <?= $form->field($model, 'number')->dropDownList(['0'=>'aaaaaaa','1'=>'aassssssssss']) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>

    </div>
<?php endif;?>