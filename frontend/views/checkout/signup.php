<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/10/16
 * Time: 1:33 PM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$model = new \frontend\models\SignupForm();
?>
<div class="row">
    <div class="col-lg-30">
        <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>
        <div id="inputs">
            <?= $form->field($model, 'firstname') ?>

            <?= $form->field($model, 'lastname') ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>
        </div>


        <div class="form-group">
            <?= Html::button('sign up', ['class' => 'btn btn-primary submitForm','type'=>'button','data-type'=>'submit','data-element'=>'input','data-name'=>'signup','data-prev'=>"step_1" ]) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
