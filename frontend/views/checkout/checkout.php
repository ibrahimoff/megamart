<?php
/*
    @TODO проверить регистрацию.
    @TODO сделать оформление без регистрации.
    @TODO при регистрации и логине номера и адреса сохраняются в базу.
    @TODO можно добавлять адреса и номер.
    @TODO при выборе оформления без регистрации данные сохраняются в сессию.ну или можно попробовать в базу сохранить.
    @TODO сделать выбор оплаты.

 */
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('frontend', 'checkout');
$totalPrice = 0;
?>
<script src="/js/autocomplete.js" type="text/javascript"></script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=<?=Yii::$app->params['GOOGLE_API_KEY']?>&signed_in=true&libraries=places&callback=initAutocomplete"
    async defer></script>

<?php if(empty(Yii::$app->session['user']['address'])):?>
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header-block text-red">
                <h1 class="checkout_headers"><?= $this->title ?></h1>
            </div>
        </div>
    </div>

    <section id="basket">

        <div class="row">
            <div class="col-lg-60">
                <div class="row">
                    <div class="page-header green-header">
                        <div class="col-lg-20">
                            <h5><?= Yii::t('frontend', 'items') ?></h5>
                        </div>
                        <div class="col-lg-12">
                            <h5><?= Yii::t('frontend', 'price') ?></h5>
                        </div>
                        <div class="col-lg-12">
                            <h5><?= Yii::t('frontend', 'quantity') ?></h5>
                        </div>
                        <div class="col-lg-12">
                            <h5><?= Yii::t('frontend', 'total_price') ?></h5>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($products as $key => $product): ?>
                        <?php if ($product['new_price'] != '') {
                            $totalPrice += $product['new_price'] * $product['quantity'];
                        } else {
                            $totalPrice += $product['price'] * $product['quantity'];
                        }
                        ?>
                        <div class="basket-item" id="item<?= $key ?>">
                            <div class="col-lg-20">
                                <div class="row">
                                    <div class="col-lg-30">
                                        <a href="<?= $product['slug'] ?>" class="">
                                            <img class="img-responsive img_size"
                                                 src="<?= $product['image'] . '-home.jpg' ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="col-lg-30">
                                        <div class="product-title">
                                            <a href="<?= $product['slug'] ?>"><h5><?= $product['name'] ?></h5></a>
                                        </div>
                                        <div class="push20"></div>
                                        <!--                                <div class="articul">-->
                                        <!--                                    Артикул: IPHA-047-->
                                        <!--                                </div>-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="product-price">
                                    <?php if ($product['new_price'] != ''): ?>
                                        <span class="present-price"
                                              id="present<?= $key ?>"><?= $product['new_price'] ?></span>
                                        <i class="mm-icon-azn"></i>
                                    <?php else: ?>
                                        <span class="present-price" id="present<?= $key ?>"><?= $product['price'] ?></span>
                                        <i class="mm-icon-azn"></i>
                                    <?php endif ?>
                                    <div class="push20"></div>
                                    <div class="product-bonus">
                                        <h5>
                                            <?php if (!Yii::$app->user->isGuest): ?>
                                                <?= Yii::t('frontend', 'bonus') ?>:<a
                                                    href="<?= \yii\helpers\Url::link('order/my-orders') ?>"
                                                    style="color: #337ab7;"> <?= $product['bonus'] ?></a>
                                            <?php else: ?>
                                                <?= Yii::t('frontend', 'bonus') ?>: <?= $product['bonus'] ?>
                                            <?php endif ?>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">

                                <div class="valign-wrapper basket-height">
                                    <div class="bucket-item-count" data-item-id="<?= $key ?>">
                                        <i class="fa fa-angle-up fa-2"></i>
                                        <span class="quantity"><?= $product['quantity'] ?></span>
                                        <i class="fa fa-angle-down fa-2"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="product-price-sum">
                                    <?php if ($product['new_price'] != ''): ?>
                                        <span class="present-price new"
                                              id="new<?= $key ?>"><?= $product['new_price'] * $product['quantity'] ?></span>
                                        <i class="mm-icon-azn"></i>
                                    <?php else: ?>
                                        <span class="present-price new"
                                              id="new<?= $key ?>"><?= $product['price'] * $product['quantity'] ?></span>
                                        <i class="mm-icon-azn"></i>
                                    <?php endif ?>
                                </div>
                                <div class="push20"></div>
                                <div class="increase">
                                    <h5>
                                        <?php if (!Yii::$app->user->isGuest): ?>
                                            <?= Yii::t('frontend', 'bonus') ?>:<a
                                                href="<?= \yii\helpers\Url::link('order/my-orders') ?>"
                                                style="color: #337ab7;"> <?= $product['bonus'] * $product['quantity'] ?></a>
                                        <?php else: ?>
                                            <?= Yii::t('frontend', 'bonus') ?>: <?= $product['bonus'] * $product['quantity'] ?>
                                        <?php endif ?>
                                    </h5>

                                </div>

                            </div>
                            <div class="col-lg-4">
                                <div class="valign-wrapper basket-height">
                                <span class="bucket-item-remove" data-item-id="<?= $key ?>"><i
                                        class="fa fa-times fa-2"></i></span>
                                </div>
                            </div>

                        </div>
                    <?php endforeach; ?>


                </div>
            </div>
        </div>

    </section>
    <div class="row" style="    margin-bottom: 30px;border: 1px solid #eee;">
        <div class="col-lg-24 col-lg-offset-32">
            <div class="row">
                <div class="col-lg-30">
                    <div class="left_badge text-right">
                        <?= Yii::t('frontend', 'total_checkout') ?>
                    </div>
                </div>
                <div class="col-lg-30" style="background: #fff;">
                    <div class="right_badge text-center">
                        <span><span class="total-price"><?= $totalPrice ?></span><i class="mm-icon-azn"></i></span>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

<?php endif?>

<?php if (Yii::$app->user->isGuest): ?>

<?php elseif (empty(Yii::$app->session['user']['address'])): ?>
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header-block text-red">
                <h1 class="checkout_headers"><?= Yii::t('frontend', 'delivery_address') ?></h1>
            </div>
        </div>
    </div>
<?php endif ?>

<div class="" style="margin-bottom: 30px;">
    <div class="">
        <?php if (Yii::$app->user->isGuest && empty(Yii::$app->session['user']['guest_id'])): ?>
            <div class="clearfix"></div>
            <div class="">
                <div class="col-lg-30">
                    <div class="white_block">

                        <span>Login</span>
                        <span style="border-right: 0;margin-left: -5px;"><?= Yii::t('frontend', 'social_login') ?></span>

                        <div class="padding20">
                            <div class="col-lg-25 col-lg-offset-2">


                                <?php $login = new \common\models\LoginForm();
                                $form = ActiveForm::begin(['id' => 'login-form', 'action' => 'login', 'method' => 'post']); ?>

                                <?= $form->field($login, 'username') ?>

                                <?= $form->field($login, 'password')->passwordInput() ?>

                                <?= $form->field($login, 'rememberMe')->checkbox() ?>

                                <div class="form-group">
                                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary']) ?>
                                </div>
                                <div style="color:#999;margin:1em 0">
                                    <?= Yii::t('frontend', 'forgot_password') ?>
                                    ? <?= Html::a(Yii::t('frontend', 'reset_it'), ['site/request-password-reset']) ?>.
                                </div>
                                <?php ActiveForm::end(); ?>
                            </div>
                            <div class="col-lg-26 col-lg-offset-3">

                                <div style="margin-top: 24px;padding-left: 13%;">
                                    <?= yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div>

                </div>
                <div class="col-lg-30">
                    <div class="white_block">
                        <div class="button_checkout" style="">
                            <div class="btn-block-tabs">
                                <button class="checkout_btn active_btn"
                                        data-step-id="signup"><?= Yii::t('frontend', 'sign_up') ?>
                                </button>
                                <button class="checkout_btn" data-step-id="without_signup"
                                        style="margin-left: -5px;"><?= Yii::t('frontend', 'without_sign_up') ?>
                                </button>
                            </div>
                        </div>
                        <div class="steps padding20">
                            <div id="signup" class="step_form">
                                <div class="col-lg-60">
                                    <?php
                                    $signup = new \frontend\models\SignupForm();
                                    $form = ActiveForm::begin(['id' => 'form-signup', 'action' => 'signup', 'method' => 'post']); ?>

                                    <?= $form->field($signup, 'firstname') ?>

                                    <?= $form->field($signup, 'lastname') ?>

                                    <?= $form->field($signup, 'email') ?>

                                    <?= $form->field($signup, 'password')->passwordInput() ?>

                                    <div class="form-group">
                                        <?= Html::submitButton('Signup', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                            <div id="without_signup" class="hidden step_form">
                                <div class="col-lg-60">
                                    <?php
                                    $model = new \frontend\models\WithoutSignup();
                                    $form = ActiveForm::begin(['id' => 'withoutsignup-form', 'action' => 'without-signup', 'method' => 'post']); ?>

                                    <?= $form->field($model, 'firstname') ?>

                                    <?= $form->field($model, 'lastname') ?>

                                    <?= $form->field($model, 'email') ?>

                                    <div class="form-group">
                                        <?= Html::submitButton('Next', ['class' => 'btn btn-primary']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        <?php else: ?>
            <?php if (empty(Yii::$app->session['user']['address'])): ?>
                <div class="white_block">
                    <?php
                    $login = new \frontend\models\AddressForm();
                    $form = ActiveForm::begin(['id' => 'login-form', 'action' => 'address', 'method' => 'post']); ?><?php if (!empty($addrArray)):?>
                        <div class="col-lg-30"><?= $form->field($login, 'address')->dropDownList($addrArray) ?></div>
                    <?php else: ?>
                        <div class="col-lg-30">
                            <?= $form->field($login, 'address')->textInput(['onfocus' => 'checkoutAutocomplete()']) ?>
                        </div>
                    <?php endif ?>
                    <div class="col-lg-30">
                        <?= $form->field($login, 'number')->widget(\yii\widgets\MaskedInput::className(), [
                            'mask' => '999-999-9999',
                        ]) ?>
                    </div>
                    <div class="col-lg-60">
                        <div class="form-group text-center">
                            <?= Html::submitButton('Next', ['class' => 'btn btn-primary', 'style' => 'margin-top: 22px;height:35px;width: 300px;']) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                    <div class="clearfix"></div>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-lg-60">
                        <div class="page-header-block text-red" style="margin-top: 0">
                            <h1 class="checkout_headers"  style="margin-top: 30px;"><?= Yii::t('frontend', 'select_payment') ?></h1>

                        </div>
                    </div>
                </div>
                <div class="row">

                    <?php foreach (\frontend\models\Checkout::getPayments() as $payment): ?>
                        <?php if($payment['payment_id']== 8):?>
                            <div class="col-lg-20">
                                <div class="white_block">
                                    <div class="media">
                                        <div class="overlay"></div>
                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object"
                                                     src="<?= Yii::getAlias('@web/uploads/payment/' . $payment['payment']['logo']) ?>"/>
                                            </a>
                                        </div>
                                        <div class="media-body" style="vertical-align: middle;">
                                            <h3 class="media-heading" style="margin-bottom: 15px;"><?= $payment['name'] ?></h3>

                                            <a href="#" class="btn btn-primary"><?= Yii::t('frontend', 'select_button')?></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php else:?>
                            <div class="col-lg-20">
                                <div class="white_block">
                                    <div class="media">

                                        <div class="media-left">
                                            <a href="#">
                                                <img class="media-object"
                                                     src="<?= Yii::getAlias('@web/uploads/payment/' . $payment['payment']['logo']) ?>"/>
                                            </a>
                                        </div>
                                        <div class="media-body" style="vertical-align: middle;">
                                            <h3 class="media-heading"
                                                style="margin-bottom: 15px;"><?= $payment['name'] ?></h3>

                                            <a href="<?= \yii\helpers\Url::link('checkout/payment?id='.$payment['payment_id']) ?>"
                                               class="btn btn-primary"><?= Yii::t('frontend', 'select_button') ?></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endif;?>

                    <?php endforeach ?>
                </div>
            <?php endif ?>
        <?php endif ?>
        <div class="clearfix"></div>
    </div>
</div>

