<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$model = new \common\models\LoginForm();
?>
<div class="site-login">
    <div class="row">
        <div class="col-lg-30">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <div id="inputs">
                    <?= $form->field($model, 'username') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div>

                <div class="form-group">
                    <?= Html::button('Login', ['class' => 'btn btn-primary submitForm','type'=>'button','data-element'=>'input','data-type'=>'submit','data-name'=>'login','data-prev'=>"step_1" ]) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
