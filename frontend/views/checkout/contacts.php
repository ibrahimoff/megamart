<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/26/16
 * Time: 6:43 PM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$model = new \frontend\models\AddressForm();
?>
<?=Yii::$app->user->id?>
<script src="<?=Yii::getAlias('@web/js/autocomplete.js')?>"></script>
<div class="address-form">
    <div class="row">
        <div class="col-lg-30">
            <?php $form = ActiveForm::begin(['id' => 'address-form']); ?>

            <?php if(!empty(\frontend\models\Checkout::getAddresses()) && !empty(\frontend\models\Checkout::getNumbers())):?>
            <div id="inputs">
                <?= $form->field($model, 'address')->dropDownList(\frontend\models\Checkout::getAddresses(),['prompt'=>'Select address']) ?>

                <?= $form->field($model, 'number')->dropDownList(\frontend\models\Checkout::getNumbers(),['prompt'=>'Select number']) ?>
            </div>
            <div class="form-group">
                <?= Html::button('Save', ['class' => 'btn btn-primary submitForm','type'=>'button','data-type'=>'address','data-name'=>'address','data-element'=>'select','data-prev'=>"step_1" ]) ?>
            </div>
            <?php else:?>
                <div id="inputs">
                    <?= $form->field($model, 'address')->textInput(['onfocus'=>'checkoutAutocomplete()']) ?>

                    <?= $form->field($model, 'number')->textInput() ?>
                </div>
                <div class="form-group">
                    <?= Html::button('Save', ['class' => 'btn btn-primary submitForm','type'=>'button','data-type'=>'address','data-name'=>'address','data-element'=>'input','data-prev'=>"step_1" ]) ?>
                </div>
            <?php endif?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

