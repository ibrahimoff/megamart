<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/27/16
 * Time: 2:10 PM
 */
$payments = \frontend\models\PaymentLang::find()->joinWith('payment')->where(['payment.status'=>'1','language'=>Yii::$app->language])->all();
?>

<?php foreach($payments as $payment):?>
    <div><button class="select-payment" id="<?=$payment->payment_id?>"><?=$payment->name?></button></div>
<?php endforeach?>
