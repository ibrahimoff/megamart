<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/10/16
 * Time: 1:18 PM
 */
?>
<div class="panel panel-default">
    <div class="panel-heading">step 1</div>
    <div class="panel-body hidden steps" id="step_1">
        <div>
            <button class="site_enter_button" data-type="signup">signup</button>
        </div>
        <div>
            <button class="site_enter_button" data-type="without-signup">without signup</button>
        </div>
        <div>
            <button class="site_enter_button" data-type="login">login</button>
        </div>
    </div>
</div>
