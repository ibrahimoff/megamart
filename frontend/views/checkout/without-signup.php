<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/10/16
 * Time: 1:34 PM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$model = new \frontend\models\WithoutSignup();
?>
<div class="without-login">
    <div class="row">
        <div class="col-lg-30">
            <?php $form = ActiveForm::begin(['id' => 'without-signup-form']); ?>
            <div id="inputs">
                <?= $form->field($model, 'email') ?>

                <?//= $form->field($model, 'number') ?>
            </div>
            <div class="form-group">
                <?= Html::button('next', ['class' => 'btn btn-primary submitForm','type'=>'button','data-element'=>'input','data-type'=>'submit','data-name'=>'without-signup','data-prev'=>"step_1" ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
