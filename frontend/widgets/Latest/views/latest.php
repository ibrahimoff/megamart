<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:10 PM
 */
use frontend\widgets\Product\Product;
?>

<section id="new-products">
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header">
                <h5 id="buttons"><?=Yii::t('frontend','latest')?></h5>
            </div>
        </div>
        <div class="col-lg-60">
            <div class="items-slider">
                <?=\frontend\widgets\Product\Product::widget(['products'=>$products,'type'=>Product::GRAY_PRODUCT,'widgetId'=>$widgetId])?>
            </div>
        </div>
    </div>
</section>
