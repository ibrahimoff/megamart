<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Latest;

use frontend\models\Product;
use frontend\models\ProductLang;
use yii\base\Widget;
use Yii;
use yii\helpers\VarDumper;

class Latest extends Widget{

    public $_widgetId = 'latest';
    public $daysCount = 10;
    public $today;
    public $tenDays;
    public $products;
    public function init(){
        $this->tenDays = date('Y-m-d H:i:s', strtotime("-10 days"));
        $this->today = date('Y-m-d H:i:s');
        $this->products = ProductLang::find()
            ->where(['language'=>Yii::$app->language])
            ->joinWith(['productOnly','discount','productCoverImage'])
            ->andFilterWhere(['between','product.created_at',$this->tenDays,$this->today])
            ->asArray()
            ->limit(20)
            ->all();
    }
    public function run(){
        return $this->render('latest',['products'=>$this->products,'widgetId'=>$this->_widgetId]);
    }
}