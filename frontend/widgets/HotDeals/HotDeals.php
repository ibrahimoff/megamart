<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\HotDeals;

use frontend\models\Sliders;
use frontend\widgets\Discounts\Discounts;
use frontend\widgets\Slider\Slider;
use yii\base\Widget;
use Yii;
class HotDeals extends Widget{
    private $byRand;
    private $slider;
    public function init(){
        $this->byRand = Discounts::widget();
        $this->slider = Slider::widget(['position'=>1]);
    }
    public function run(){
        return $this->render('hotDeals',['byRand'=>$this->byRand,'slider'=>$this->slider]);
    }
}