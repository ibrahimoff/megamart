<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Header;

use frontend\widgets\LangSwitcher\LangSwitcher;
use frontend\widgets\StaticPages\StaticPages;
use yii\base\Widget;

class Header extends Widget{
    public $widget;
    public $widgetLanguageSwitcher;
    public function init(){
        $this->widget = StaticPages::widget();
        $this->widgetLanguageSwitcher = LangSwitcher::widget();
    }
    public function run(){
        return $this->render('header',['widget'=>$this->widget,'widgetLanguageSwitcher'=>$this->widgetLanguageSwitcher]);
    }
}