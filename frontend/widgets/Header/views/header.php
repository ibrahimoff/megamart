<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:01 PM
 */
?>

<div id="header-top">
    <div class="container-fluid">
      <div class="row">
                  <div class="col-lg-36 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm">
                    <a href="#" id="open-mini-sidebar" class="side-button"><span class="glyphicon glyphicon-menu-hamburger"></span></a>
                  </div>
        <div class="col-lg-40 col-md-37 hidden-sm hidden-xs">

            <?=$widget?>
        </div>
        <div class="col-lg-20 col-md-23 col-sm-30 hidden-xs">
                <?=$widgetLanguageSwitcher?>
          <div class="pull-right">
              <?php if(Yii::$app->user->isGuest):?>
                  <div class="btns-header">
                      <a href="<?=\yii\helpers\Url::link('signup')?>" class="header_links"><?=Yii::t('frontend','sign_up')?></a>
                      <a href="<?=\yii\helpers\Url::link('login')?>" class="header_links"><?=Yii::t('frontend','log_in')?></a>
                  </div>
              <?php else:?>
                  <div class="profile_menu">
                      <div class="dropdown" style="    margin-top: 7px;">
                          <a href="#" class="dropdown-toggle profile_drp" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                              <?php if(!empty(Yii::$app->user->identity->image) && file_exists(Yii::getAlias('@frontend/web/uploads/users/'.Yii::$app->user->identity->image))):?>
                                  <img src="<?=Yii::getAlias('@web/uploads/users/'.Yii::$app->user->identity->image)?>" alt="">
                              <?php else:?>
                                  <img src="https://placeholdit.imgix.net/~text?txtsize=5&txt=34%C3%9734&w=34&h=34" alt="">
                              <?php endif?>
                              <span class="name_span"><?=Yii::$app->user->identity->firstname?></span>
                              <span class="glyphicon glyphicon-triangle-bottom" style="color: #fff;    font-size: 11px;"></span>
                          </a>
                          <ul class="dropdown-menu dropdown-menu-left">
                              <li><a href="#" class=""><?=Yii::t('frontend','client_code')?>: <?=Yii::$app->user->id?></a></li>
                              <li><a href="<?=\yii\helpers\Url::link('edit-profile')?>"><?=Yii::t('frontend','edit_profile')?></a></li>
                              <li><a href="<?=\yii\helpers\Url::link('order/my-orders')?>"><?=Yii::t('frontend','shopping_history')?></a></li>
                              <li><a href="<?=\yii\helpers\Url::link('logout')?>" data-method="post" class=""><?=Yii::t('frontend','log_out')?></a></li>
                              <li><a href="#"><?=Yii::t('frontend','bonus')?> <?=Yii::$app->user->identity->points?></a></li>

                          </ul>
                      </div>
                  </div>
                  <div>
                  </div>
              <?php endif?>
          </div>
        </div>
        <div class="col-lg-60 text-center">
        </div>
      </div>
    </div>
</div>