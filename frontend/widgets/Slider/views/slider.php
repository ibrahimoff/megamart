<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/25/16
 * Time: 10:52 AM
 */
?>
<?php //if(!empty($slider)):?>

    <div id="hot-deals-slider">
        <?php foreach($slider->sliderImages as $data):?>
        <a href="<?=$data->sliderImageTranslate->link?>">
            <div class="slider-item">

                    <div class="img" style="background-image: url(<?=Yii::getAlias('@web/uploads/slider/'.$data->slider_id.'/').$data->image?>)"></div>

                <div class="slider-desc" style="color:#fff;">
                    <p class="lead"><?=$data->sliderImageTranslate->value?></p>
                </div>

            </div>
        </a>
        <?php endforeach;?>
    </div>
<?php //endif?>

