<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Slider;

use frontend\models\Sliders;
use yii\base\Widget;
use Yii;
class Slider extends Widget{
    private $slider;
    public $position;
    public function init(){
        $this->slider = Sliders::find()->where(['position'=>$this->position])->with('sliderImages')->one();
    }
    public function run(){
        return $this->render('slider',['slider'=>$this->slider]);
    }
}