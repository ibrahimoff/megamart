<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Banners;

use yii\base\Widget;
use Yii;
use frontend\models\Banner;
use yii\db\Expression;

class Banners extends Widget{

    public $position;
    private $banner;
    public function init(){
        $this->banner = Banner::find()->where(['position'=>$this->position])->with('bannerTranslate')->orderBy(new Expression('rand()'))->one();
    }
    public function run(){
        return $this->render('banners',['banner'=>$this->banner]);
    }
}