<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Product;

use frontend\models\Cart;
use frontend\models\CartProduct;
use yii\base\Widget;
use yii\helpers\VarDumper;
use Yii;
class Product extends Widget{
    const WHITE_PRODUCT = 1;
    const GRAY_PRODUCT = 2;
    const FLIPP_PRODUCT = 3;
    public $type;
    public $grid;
    public $_view;
    public $class;
    public $products;
    public $widgetId;
    public $inCart;
    public $compareArray;

    public function init(){

        $this->inCart = \frontend\models\Cart::checkCard();
        $this->compareArray = is_array(Yii::$app->session['compare'])?Yii::$app->session['compare']:[];

        switch($this->type){
            case 1:
                $this->class = 'white';
                $this->_view = $this->render('product',['class'=>$this->class,'products'=>$this->products,'widgetId'=>$this->widgetId,'grid'=>$this->grid,'inCart'=>$this->inCart,'compareArray'=>$this->compareArray]);
                break;
            case 2:
                $this->class = 'gray';
                $this->_view = $this->render('product',['class'=>$this->class,'products'=>$this->products,'widgetId'=>$this->widgetId,'grid'=>$this->grid,'inCart'=>$this->inCart,'compareArray'=>$this->compareArray]);
                break;
            case 3:
                $this->class = 'flip';
                $this->_view = $this->render('product',['class'=>$this->class,'products'=>$this->products,'widgetId'=>$this->widgetId,'grid'=>$this->grid,'inCart'=>$this->inCart,'compareArray'=>$this->compareArray]);
                break;
        }
    }
    public function run(){
        return $this->_view;
    }
}