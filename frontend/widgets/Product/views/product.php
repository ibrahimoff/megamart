<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:32 PM
 */
?>
<?php if(!empty($products)):?>
    <?php foreach($products as $product):?>

        <?php if(!empty($grid)):?>
            <div class="<?=$grid?>">

        <?php endif?>
            <div class="card-product white <?=$widgetId == 'accessory'? 'accessory':''?>">
                <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->role == Yii::$app->params['role']):?>
                            <div class="text-left">
                                <a href="<?=\yii\helpers\Url::link('admin/product/update?id='.$product['product_id'])?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </div>
                    <?php endif;?>

                <div class="product-img">
                    <a href="<?=\yii\helpers\Url::link($product['slug'],'_p')?>">
                        <?php if($product['productOnly']['on_sale'] == 1):?>
                            <span class="discount"><?=\frontend\models\Product::getDiscountPercent($product['discount']['old_price'],$product['discount']['price'])?></span>
                        <?php endif?>
                        <img src="<?=Yii::getAlias('@web/uploads/products/').$product['productCoverImage']['product_id'].'-'.$product['productCoverImage']['id'].'-large.jpg'?>" alt="">
                    </a>
                </div>
                <div class="product-description">
                    <div class="product-title">
                        <h5><a href="<?=\yii\helpers\Url::link($product['slug'],'_p')?>"><?=$product['name']?></a></h5>
                    </div>
                    <div style="<?=$widgetId == 'accessory'? 'padding-top: 20px;':''?>" class="product-price <?=$product['productOnly']['on_sale'] != 1 ? 'margin_class':''?>" <?=!Yii::$app->user->isGuest && Yii::$app->user->identity->role == Yii::$app->params['role'] && $product['productOnly']['on_sale'] != 1 ? 'id="editable" data-product="'.$product['product_id'].'"':''?>>

                        <?php if($product['productOnly']['on_sale'] == 1):?>
                            <span class="past-price"><?=$product['discount']['old_price']?><i class="mm-icon-azn"></i></span>
                            <div>
                                <span class="present-price"><?=$product['discount']['price']?><i class="mm-icon-azn"></i></span>
                            </div>
                        <?php else:?>
                            <span class="present-price"><?=$product['productOnly']['price']?><i class="mm-icon-azn"></i></span>
                        <?php endif?>
                    </div>

                    <div class="btn-group-custom">
                        <a href="#" class="btn btn-primary buyButton <?=in_array($product['product_id'],$inCart) ? 'disabled': ''?>" id="buy<?=$product['product_id']?>" data-id="<?=$product['product_id']?>"><?=$widgetId != 'accessory'?Yii::t('frontend','buy_button'):Yii::t('frontend','add_button')?></a>
                        <?php if($widgetId != 'accessory'):?>
                        <span class="btn btn-info <?=in_array($product['product_id'],$compareArray)?'remove':''?> compareButton" id="<?=$product['product_id']?>"><?=in_array($product['product_id'],$compareArray)?Yii::t('frontend','remove_button'):Yii::t('frontend','compare_button')?></span>
                        <?php endif?>

                    </div>
                    <?php if($widgetId != 'accessory'):?>
                        <ul class="product-actions">
<!--                            <li>-->
<!--                                <span><i class="mm-icon-share"></i></span>-->
<!--                                <div class="share_methods">-->
<!--                                    <div class="g-plus" data-action="share" data-annotation="none"></div>-->
<!--                                    <div class="fb-share-button" data-href="--><?//=\yii\helpers\Url::link($product['slug'],'_p')?><!--" data-layout="icon"></div>-->
<!--                                </div>-->
<!--                            </li>-->
                        </ul>
                    <?php endif?>

                </div>
            </div>
    <?php if(!empty($grid)):?>
    </div>
    <?php endif?>
    <?php endforeach;?>
<?php endif?>
