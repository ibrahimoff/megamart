<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:04 PM
 */
?>

<script src="/js/components/elasticSearch.js" type="text/babel"></script>
<div id="header-main">
    <div class="container-fluid">
        <div class="content_wrapper">
            <div class="row">

                <!--        <div class="col-lg-36 col-sm-12 col-xs-12 hidden-lg hidden-md hidden-sm">-->
                <!--          <a href="#" id="open-mini-sidebar" class="side-button"><i class="mm-icon-menu"></i></a>-->
                <!--        </div>-->
                <div class="col-lg-20 col-md-20 col-sm-20 col-xs-60">
                    <a href="/<?=Yii::$app->language.'/'?>" class="logo"><img class="img-responsive" src="<?=Yii::getAlias('/img/logo.png')?>" alt=""></a>
                </div>
                <div class="col-lg-20 col-md-20 col-sm-20 hidden-xs">
                        <div class="text-right work_time">
                            <div>Время работы</div>
                            <div>Пн-Пт: c 9:30 до 18:30</div>
                            <div>Сб: c 9:30 до 16:00</div>
                            </div>
                </div>
                <div class="col-lg-20 col-md-20 col-sm-20 contact hidden-xs">
                    <h4 class="text-right"><div><sup>+994 12</sup></div><strong style="font-size: 25px;line-height: 20px;    display: block;"><?=Yii::t('static','number')?></strong><div data-toggle="modal" style="display: inline-block;" data-target=".make_call_modal">
                            <?=Yii::t('frontend','ask_call')?>
                        </div></h4>
                </div>

                <div class="col-lg-60 col-md-60 col-sm-60 col-xs-60 search-container">
                    <a href="#" id="search-button" class="side-button text-right search-button hidden-lg hidden-md hidden-sm"><i class="mm-icon-search"></i></a>
                    <div class="search">
                        <form action="/site/find" method="get" class="search-form">
                            <div id="elastic" language="<?=Yii::$app->language?>"></div>
                            <button type="submit"><i class="mm-icon-search"></i></button>
                            <input type="text" name="_product_name" placeholder="<?=Yii::t('frontend','search_placeholder')?>" value="<?=Yii::$app->request->get('name')?>" class="form-control" id="elasticSearch" data-lang="<?=Yii::$app->language?>" autocomplete="off">
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>