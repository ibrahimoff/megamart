<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/18/16
 * Time: 4:17 PM
 */
use yii\helpers\Url;
?>
<?php if(!empty($pages)):?>
<ul class="static_pages">
    <?php foreach($pages as $page):?>
        <li>
            <a href="<?=Url::link('page/'.$page['page']['slug'])?>"><?=$page['page']['name']?></a>
        </li>
    <?php endforeach;?>
</ul>
<?php endif;?>