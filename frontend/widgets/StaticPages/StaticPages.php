<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/18/16
 * Time: 4:17 PM
 */

namespace frontend\widgets\StaticPages;
use frontend\models\Page;
use yii\base\Widget;

class StaticPages extends Widget{
    public $widget;
    public $pages;
    public function init(){
        $this->pages = Page::find()
            ->where(['status'=>'1'])
            ->with('page')
            ->orderBy('order')
            ->asArray()
            ->all();
    }
    public function run(){
        return $this->render('staticPages',['pages'=>$this->pages]);
    }
}