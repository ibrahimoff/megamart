<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Categories;

use frontend\models\Category;
use yii\base\Widget;
use yii\helpers\VarDumper;

class Categories extends Widget{

    public $categories;
    public function init(){
        $this->categories = Category::find()->where(['status'=>'1'])->andWhere(['in','level',[1,2]])->with('categoryTranslate','child')->orderBy(['nleft'=>SORT_ASC])->all();
    }
    public function run(){
        return $this->render('categories',['categories'=>$this->categories]);
    }
}