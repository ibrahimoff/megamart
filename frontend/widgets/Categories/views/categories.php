<?php
use yii\helpers\Html;
$level = 0;

$id = '';

$class = 'dropdown-menu width100';
echo Html::beginTag('ul',['class'=>'nav navbar-nav resize_category']);

echo Html::beginTag('li',['class'=>'dropdown category_list']);
echo '<a href="#" class=" category_drop" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-menu-hamburger"></span>'.Yii::t('frontend','all_categories').' <span class="glyphicon glyphicon-triangle-bottom" style="    margin-left: 5px;"></span></a>';

foreach ($categories as $n => $category) {

    if ($category->level == $level){
        echo Html::endTag('li') . "\n";
    }else if ($category->level > $level){
        echo Html::beginTag('ul',['id'=>$id,'class'=>$class]) . "\n";
        $class='hidden';


    }   else {
        echo Html::endTag('li') . "\n";
        for ($i = $level - $category->level; $i; $i--) {
            echo Html::endTag('ul') . "\n";
            echo Html::endTag('li') . "\n";
        }
    }

    if($category->parent_id == 1){
        $id=$category->id;
        echo Html::beginTag('li',['data-parent-id'=>$category->id,'class'=>'parent-node']);
    }else{
        echo Html::beginTag('li');
    }
    echo $category->level == 1 && !empty($category->child)? '<i class="fa fa-chevron-right"></i>' :'';
    echo Html::beginTag('a',['href'=>!empty($category->child)? '#' : \yii\helpers\Url::link($category->categoryTranslate->slug,'by-category')]).Html::encode($category->categoryTranslate->name).Html::endTag('a');

    $level = $category->level;
}

for ($i = $level; $i; $i--) {
    echo Html::endTag('li') . "\n";
    echo Html::endTag('ul') . "\n";
}
echo Html::endTag('li') . "\n";
echo Html::endTag('ul') . "\n";