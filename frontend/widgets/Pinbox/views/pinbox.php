<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:06 PM
 */
use frontend\widgets\Product\Product;
?>
<?php if($products):?>
<?=\frontend\widgets\Product\Product::widget(['type'=>Product::WHITE_PRODUCT,'products'=>$products,'widgetId'=>$widgetId])?>
<?php else:?>
    <div class="col-md-60">
        <div class="alert alert-info fade in">
            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
            Still your checklist is empty.
        </div>
    </div>
<?php endif;?>