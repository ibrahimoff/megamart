<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 * Sale out widget.This widget gets a onSale products.
 */

namespace frontend\widgets\Pinbox;

use frontend\models\Product;
use frontend\models\ProductLang;
use yii\base\Widget;
use yii\helpers\VarDumper;

class PinboxWidget extends Widget{
    public $_widgetId = 'PinboxWidget';
    public $products;
    public function init(){
        if(!isset($_SESSION['user']) && empty($_SESSION['user']['pins'])){
            $this->products = [];
        } else{
            $this->products = ProductLang::find()
                ->where(['language'=>\Yii::$app->language])
                ->with(['discount','productOnly','productCoverImage','pins'])
                ->andFilterWhere(['in','product_id', $_SESSION['user']['pins']])
                ->asArray()
                ->all();
        }
    }
    public function run(){
        return $this->render('pinbox',['products'=>$this->products,'widgetId'=>$this->_widgetId]);
    }
}