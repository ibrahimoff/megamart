<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:52 PM
 */
?>
<div class="navbar navbar-inverse">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                        <a href="<?=\yii\helpers\Url::link('logout')?>" data-method="post">Выход (<?=Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname?>)</a>
                </li>
                <li>
                        <a href="<?=\yii\helpers\Url::link('admin')?>" data-method="post">Dashboard</a>
                </li>
            </ul>
        </div>
    </div>
</div>