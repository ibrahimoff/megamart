<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Settings;

use yii\base\Widget;

class Settings extends Widget{

    public function init(){

    }
    public function run(){
        return $this->render('settings');
    }
}