<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/1/16
 * Time: 6:07 PM
 */
?>
    <ul class="pull-right header_links" style="">
<?php if(!empty($productLangs->slugTranslate)){?>
        <?php foreach ($languages as $language):?>
            <li class="">
                <a href="<?='/'.$language.'/_p/'.$productLangs->slugTranslate->slug?>" class="<?=Yii::$app->language == $language ? 'selected_lang':''?>"><?=$language?></a>
            </li>
        <?php endforeach;?>
<?php }elseif(!empty($pageLangs->pageSlugTranslate)){?>
        <?php foreach ($languages as $language):?>
            <li class="">
                <a href="<?='/'.$language.'/page/'.$pageLangs->pageSlugTranslate->slug?>" class="<?=Yii::$app->language == $language ? 'selected_lang':''?>"><?=$language?></a>
            </li>
        <?php endforeach;?>
<?php }elseif(!empty($categoryLangs->slugTranslate)){?>
        <?php foreach ($languages as $language):?>
            <li class="">
                <a href="<?='/'.$language.'/by-category/'.$categoryLangs->slugTranslate->slug?>" class="<?=Yii::$app->language == $language ? 'selected_lang':''?>"><?=$language?></a>
            </li>
        <?php endforeach;?>
<?php }else{?>
    <?php foreach ($languages as $language):?>
    <li class="">
        <a href="<?='/'.$language.Yii::$app->request->url?>" class="<?=Yii::$app->language == $language ? 'selected_lang':''?>"><?=$language?></a>
    </li>
    <?php endforeach;?>
<?php }?>
    </ul>

