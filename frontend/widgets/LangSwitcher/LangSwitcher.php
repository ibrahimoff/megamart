<?php

namespace frontend\widgets\LangSwitcher;

use frontend\models\CategoryLang;
use frontend\models\PageLang;
use frontend\models\ProductLang;
use yii\base\Widget;
use Yii;
use yii\helpers\VarDumper;

class LangSwitcher extends Widget{
    public $languages;
    public $productLangs;
    public $pageLangs;
    public $categoryLangs;
    public function init(){
        $this->languages = Yii::$app->params['langs'];
        if(!empty(Yii::$app->request->get('productSlug'))){
            $this->productLangs = ProductLang::find()->where(['slug'=>Yii::$app->request->get('productSlug')])->with('slugTranslate')->one();
        }elseif(Yii::$app->request->get('slug')){
            $this->pageLangs = PageLang::find()->where(['slug'=>Yii::$app->request->get('slug')])->with('pageSlugTranslate')->one();
        }elseif(Yii::$app->request->get('categorySlug')){
            $this->categoryLangs = CategoryLang::find()->where(['slug'=>Yii::$app->request->get('categorySlug')])->with('slugTranslate')->one();
        }

    }
    public function run(){
        return $this->render('langSwitcher',['languages'=>$this->languages,'productLangs'=>$this->productLangs,'pageLangs'=>$this->pageLangs,'categoryLangs'=>$this->categoryLangs]);
    }
}