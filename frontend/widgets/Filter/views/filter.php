<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/25/16
 * Time: 12:24 PM
 *
 */
/**
@TODO вытащить тайтл категории
 */
$attrArr = !empty($_GET['Filter']['attributes']) ? explode(',',$_GET['Filter']['attributes']) : [];
$manufacturerArr = !empty($_GET['Filter']['manufacturers']) ? explode(',',$_GET['Filter']['manufacturers']) : [];
$filters = !empty(Yii::$app->session['switcher']) ? Yii::$app->session['switcher'] :[];

?>
<div class="filters_list " id="<?=$categoryId?>">
    <div class="price-fields">
        <h3 class="switcher"><span><?=Yii::t('frontend','price')?></span></h3>
        <div id="slider-handles"></div>
            <input type="text" data-min="<?=$minPrice?>" id="slider-snap-value-lower" class="form-control" data-type="gte" value="<?=!empty($_GET['Filter']['gte'])?$_GET['Filter']['gte']:$minPrice?>"/>
            <input type="text" data-max="<?=$maxPrice?>" data-type="lte" id="slider-snap-value-upper" class="form-control" value="<?=!empty($_GET['Filter']['lte'])?$_GET['Filter']['lte']:$maxPrice?>"/>
        <div class="clearfix"></div>
    </div>
    <div class="list_container">
        <h3 class="switcher" data-switch="manufacturer"><span><?=Yii::t('frontend','manufacturer')?></span> <i class="fa <?=in_array('manufacturer',$filters) ? 'fa-caret-left':'fa-caret-down'?> pull-right"></i></h3>
        <ul class="filters_list" <?=in_array('manufacturer',$filters) ? 'style="display: none;"':''?>>
            <?php foreach($manufacturers as $manufacturer):?>
                <?php if(!empty($_GET['Filter']['manufacturers'])):?>
                    <li>
                        <input type="checkbox" class="hidden" id="manufacturer_<?=$manufacturer->manufacturer_id?>" data-type="manufacturers" value="<?=$manufacturer->manufacturer_id?>" <?=in_array($manufacturer->manufacturer_id,$manufacturerArr)?'checked':''?>/>
                        <label class="checkbox_attr" for="manufacturer_<?=$manufacturer->manufacturer_id?>">
                                                    <span class="checkbox_span">
                            <i class="fa <?=in_array($manufacturer->manufacturer_id,$manufacturerArr)?'fa-check-square':'fa-square-o'?>"></i>
                        </span>
                            <?=$manufacturer->manufacturerName->name?>
                        </label>
                    </li>
                <?php else:?>
                    <li>
                        <input type="checkbox" class="hidden" id="manufacturer_<?=$manufacturer->manufacturer_id?>" data-type="manufacturers" value="<?=$manufacturer->manufacturer_id?>" />

                        <label class="checkbox_attr" for="manufacturer_<?=$manufacturer->manufacturer_id?>">
                            <span class="checkbox_span">
                                <i class="fa fa-square-o"></i>
                            </span>
                            <?=$manufacturer->manufacturerName->name?>
                        </label>
                    </li>
                <?php endif?>
            <?php endforeach;?>
        </ul>
    </div>
        <?php foreach($attributes as $attribute):?>
            <div class="list_container">
                <h3 class="switcher"  data-switch="attributes_<?=$attribute->id?>"><span><?=$attribute->name?></span><i class="fa <?=in_array('attributes_'.$attribute->id,$filters) ? 'fa-caret-left':'fa-caret-down'?> pull-right"></i></h3>
                <ul class="filters_list" <?=in_array('attributes_'.$attribute->id,$filters) ? 'style="display: none;"':''?>>
                    <?php foreach($attribute->catAttrValueTranslate as $attributeValue):?>
                        <?php if(!empty($_GET['Filter']['attributes'])):?>
                            <li>
                                <input type="checkbox" class="hidden" id="attr_value_id_<?=$attributeValue->cat_attr_value_id?>" name="Filter[attributes][]" data-type="attributes" value="<?=$attributeValue->cat_attr_value_id?>" <?=in_array($attributeValue->cat_attr_value_id,$attrArr)?'checked':''?>/>
                                <label class="checkbox_attr" for="attr_value_id_<?=$attributeValue->cat_attr_value_id?>">
                                    <span class="checkbox_span">
                                        <i class="fa <?=in_array($attributeValue->cat_attr_value_id,$attrArr)?'fa-check-square':'fa-square-o'?>"></i>
                                    </span>
                                    <?=$attributeValue->name?>
                                </label>
                            </li>
                        <?php else:?>
                            <li>
                                <input type="checkbox"  id="attr_value_id_<?=$attributeValue->cat_attr_value_id?>" class="hidden" name="Filter[attributes][]" data-type="attributes" value="<?=$attributeValue->cat_attr_value_id?>" />
                                <label class="checkbox_attr" for="attr_value_id_<?=$attributeValue->cat_attr_value_id?>">
                                    <span class="checkbox_span">
                                        <i class="fa fa-square-o"></i>
                                    </span>
                                    <?=$attributeValue->name?>
                                </label>
                            </li>
                        <?php endif?>
                    <?php endforeach;?>
                </ul>
            </div>

        <?php endforeach;?>
        <div style="margin-top: 30px;">
            <center><button id="submit-filters" type="button" class="btn btn-danger"><?=Yii::t('frontend','search')?></button></center>
        </div>

</div>
