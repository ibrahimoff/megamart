<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Filter;

use frontend\models\CatAttrLang;
use frontend\models\CategoryLang;
use frontend\models\Product;
use yii\base\Widget;
use yii\helpers\VarDumper;
use Yii;
class Filter extends Widget{

    public $categories;
    public $categorySlug;
    public $attributes;
    public $categoryId;
    public $manufacturers;
    public $minPrice;
    public $maxPrice;
    public function init(){
        $this->attributes = CatAttrLang::find()
            ->where(['cat_attr_lang.language'=>Yii::$app->language])
            ->with('catAttrValueTranslate')
            ->joinWith('attrByCategorySlug')
            ->andFilterWhere(['slug'=>$this->categorySlug])
            ->all();
        $this->manufacturers = Product::find()->where(['category_id'=>$this->categoryId])
            ->joinWith('manufacturerName')->andFilterWhere(['not','name',null])->groupBy('manufacturer_id')->all();
       // VarDumper::dump($this->manufacturers,6,1);die();
    }
    public function run(){
        return $this->render('filter',['attributes'=>$this->attributes,'manufacturers'=>$this->manufacturers,'categoryId'=>$this->categoryId,'maxPrice'=>$this->maxPrice,'minPrice'=>$this->minPrice]);
    }
}