<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Bestsellers;

use yii\base\Widget;

class Bestsellers extends Widget{

    public function init(){

    }
    public function run(){
        return $this->render('bestsellers');
    }
}