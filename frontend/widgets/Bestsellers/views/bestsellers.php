<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:12 PM
 */
use frontend\widgets\Product\Product;
?>

<section id="bestsellers-and-news">

    <div class="row">

        <div class="col-lg-48 col-md-30 col-sm-30">

            <div class="page-header">
                <h5 id="buttons"><?=Yii::t('frontend','bestsellers')?></h5>
            </div>

            <div class="row">
                <?=\frontend\widgets\Product\Product::widget(['type'=>Product::FLIPP_PRODUCT, 'grid'=>'col-lg-30 col-sm-60 col-xs-60'])?>
            </div>

        </div>
        <div class="col-lg-12 col-md-30 col-sm-30">
            <?=\frontend\widgets\News\News::widget()?>
        </div>
    </div>
</section>
