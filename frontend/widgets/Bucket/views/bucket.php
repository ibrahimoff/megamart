<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */
use yii\helpers\Url;

?>

<div id="bucket_button" data-lang="<?=Yii::$app->language?>">
     <span class="bucket_count_span">
      <?php $count=0; if(!empty($items)){
        foreach($items as $key=>$item){
            $count += intval($item['quantity']);
        }?>
      <?php } ?>
       <?=$count;?>
       </span>
  <span class="glyphicon glyphicon-shopping-cart"></span>
  <span class="glyphicon glyphicon-triangle-right"></span>
</div>
<div id="bucket_backdrop" class="bucket_close"></div>
<div id="bucket_window">
  <div class="bucket-header">
    <span class="bucket-info"><i class="fa fa-shopping-basket"></i> <?=sprintf(Yii::t('frontend','bucket_total_count'),!empty($count)? $count : 0)?></span>
    <i class="bucket_close fa fa-times fa-2"></i>
  </div>

  <div class="bucket-content scroll-bar-wrap">
    <div class="scroll-box">
      <ul id="products_list">
        <?php if(!empty($items)):?>
          <?php foreach($items as $key=>$item):?>
            <li>
              <a class="bucket-item-img" href="<?=Url::link($item['slug'],'_p')?>"><img src="<?=$item['image'].'-small.jpg'?>" alt=""></a>
              <div class="bucket-item-name">
                <a href="<?=Url::link($item['slug'],'_p')?>"><?=$item['name']?></a>
              </div>
              <div class="bucket-item-count" data-item-id="<?=$key?>">
                <i class="fa fa-angle-up fa-2"></i>
                <span class="quantity"><?=$item['quantity']?></span>
                <i class="fa fa-angle-down fa-2" ></i>
              </div>
              <div class="bucket-item-price"><?=$item['price']?></div>
              <i class="bucket-item-close fa fa-times fa-2" data-item-id="<?=$key?>"></i>
            </li>
          <?php endforeach?>
        <?php endif?>
      </ul>
    </div>
    <div class="cover-bar"></div>
  </div>
  <div class="bucket-footer">
    <div class="col-md-60 bucket-price-sum">
      <?=sprintf(Yii::t('frontend','bucket_total_price'),!empty($sumPrice) ? $sumPrice :0)?>
    </div>

    <div class="col-md-30 col-sm-30 col-xs-30" id="">
        <a href="<?=Url::link('checkout/index')?>" class="btn btn-primary make_order pull-right <?=empty($items) ? 'hidden':''?>"><?=Yii::t('frontend','make_checkout')?></a>
      <div class="clearfix"></div>
    </div>
    <div class="col-md-30 col-sm-30 col-xs-30">
      <a href="#" class="bucket_close btn btn-default pull-right"><?=Yii::t('frontend','continue_shopping')?></a>
      <div class="clearfix"></div>

    </div>
  </div></div>