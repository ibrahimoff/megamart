<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\Bucket;

use frontend\models\Cart;
use frontend\models\CartProduct;
use frontend\models\Pinbox;
use yii\base\Widget;
use yii\helpers\VarDumper;
use Yii;
class Bucket extends Widget{
    public $bucketCount;
    public $compareCount;
    public $pinsCount;
    public $items;
    public $cart;
    public $products;
    public $count;
    public $sumPrice;
    public function init(){
        $this->bucketCount = count(Yii::$app->session['bucket']);
        $this->compareCount = count(Yii::$app->session['compare']);
        if(!yii::$app->user->isGuest) {
            $this->pinsCount = Pinbox::find()->where(['user_id'=>yii::$app->user->id])->count();
        }

        if(Yii::$app->user->isGuest){
            $this->items = Yii::$app->session['bucket'];
            if(!empty(Yii::$app->session['bucket'])){
                foreach($this->items as $key=>$item){
                    $this->count +=$item['quantity'];
                    $this->sumPrice +=$item['quantity']*$item['price'];
                }
            }

        }else{
            $this->cart = Cart::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0'])->orderBy(['id'=>SORT_DESC])->with('cartProducts')->one();
            $this->items = [];
            if(!empty($this->cart)){
                foreach($this->cart->cartProducts as $item){
                    $this->items[$item->product_id] = [
                        'name'=>$item->itemLangs->name,
                        'slug'=>$item->itemLangs->slug,
                        'quantity'=>$item->quantity,
                        'price'=>$item->item->price,
                        'image'=>Yii::getAlias('@web/uploads/products/').$item->itemLangs->productCoverImage->product_id.'-'.$item->itemLangs->productCoverImage->id
                    ];
                    $this->count +=$item->quantity;
                    $this->sumPrice +=$item->quantity*$item->item->price;
                }
            }
        }

    }
    public function run(){
        return $this->render('bucket',['items'=>$this->items,'cart'=>$this->cart,'count'=>$this->count,'sumPrice'=>$this->sumPrice]);
    }
}