<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/25/16
 * Time: 9:46 AM
 */
?>


<?php if(!empty($randomDiscounts)):?>
<div  id="super-deals" class="img" style="background-image: url(<?=Yii::getAlias('@web/uploads/products/'.$randomDiscounts['productCoverImage']['product_id'].'-'.$randomDiscounts['productCoverImage']['id'].'-large.jpg')?>)" alt="">
    <div class="super-deal-product">
        <h2><a href="<?=\yii\helpers\Url::link($randomDiscounts['slug'],'_p')?>"><?=$randomDiscounts['name']?></a></h2>
        <div class="product-price">
            <span class="past-price"><?=$randomDiscounts['productOnly']['price']?><i class="mm-icon-azn"></i></span>
            <span class="present-price"><?=$randomDiscounts['discount']['price']?><i class="mm-icon-azn"></i></span>
        </div>
        <a href="<?=\yii\helpers\Url::link($randomDiscounts['slug'],'_p')?>" class="btn btn-primary"><?=Yii::t('frontend','buy_button')?></a>
    </div>
</div>
<?php endif?>