<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/25/16
 * Time: 9:45 AM
 */

namespace frontend\widgets\Discounts;

use frontend\models\ProductLang;
use Yii;
use yii\base\Widget;
use yii\db\Expression;
use yii\helpers\VarDumper;

class Discounts extends Widget
{
    private $byDiscount;
    public function init(){
        $this->byDiscount = ProductLang::find()->with('pins')->joinWith(['productOnly','discount','productCoverImage'])->where(['language'=>Yii::$app->language,'product.on_sale'=>1])->orderBy(new Expression('rand()'))->limit(20)->asArray()->one();
    }
    public function run(){
        return $this->render('discounts',['randomDiscounts'=>$this->byDiscount]);
    }
}