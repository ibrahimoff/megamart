<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 * Sale out widget.This widget gets a onSale products.
 */

namespace frontend\widgets\SaleOut;

use frontend\models\Cart;
use frontend\models\CartProduct;
use frontend\models\Product;
use frontend\models\ProductLang;
use yii\base\Widget;
use yii\helpers\VarDumper;
use Yii;
class SaleOut extends Widget{
    public $_widgetId = 'saleOut';
    public $products;
    public function init(){
        $this->products = ProductLang::find()
            ->where(['language'=>\Yii::$app->language])
            ->with(['discount','productOnly','productCoverImage','pins'])
            ->asArray()
            ->limit(20)
            ->all();
//        VarDumper::dump($this->inCart,6,1);


    }
    public function run(){
        return $this->render('saleOut',['products'=>$this->products,'widgetId'=>$this->_widgetId]);
    }
}