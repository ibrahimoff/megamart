<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:06 PM
 */
use frontend\widgets\Product\Product;
?>

<section id="sell-out">
    <div class="row">
        <div class="col-lg-60">
            <div class="page-header black-header">
                <div class="line-bg">
                    <h5 id="buttons"><?=Yii::t('frontend','sale_out')?></h5>
                </div>
            </div>
        </div>
        <div class="col-lg-60">
            <div class="items-slider">
                <?=\frontend\widgets\Product\Product::widget(['type'=>Product::WHITE_PRODUCT,'products'=>$products,'widgetId'=>$widgetId])?>
            </div>
        </div>
    </div>
</section>
