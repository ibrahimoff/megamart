<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 3:15 PM
 */
use frontend\widgets\Product\Product;
?>

<div class="page-header">
    <h5 id="buttons"><?=Yii::t('frontend','news')?></h5>
</div>

<div class="row">
    <?php foreach($news as $item):?>
    <div class="col-lg-60">
        <div class="card-news">
            <span class="date"><?=$item['date']?></span>
            <a class="title" href="<?=\yii\helpers\Url::to(['/news/view','id'=>$item['id']])?>"><?=$item['news']['name']?></a>
        </div>
    </div>
    <?php endforeach;?>

</div>
