<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/16/16
 * Time: 2:51 PM
 */

namespace frontend\widgets\News;

use yii\base\Widget;
use Yii;
use yii\helpers\VarDumper;

class News extends Widget{

    public $news;
    public function init(){
        $this->news = \frontend\models\News::find()
            ->where(['status'=>'1'])
            ->with(['news'])
            ->orderBy(['id'=>SORT_DESC])
            ->asArray()
            ->limit(20)
            ->all();

    }
    public function run(){
        return $this->render('news',['news'=>$this->news]);
    }
}