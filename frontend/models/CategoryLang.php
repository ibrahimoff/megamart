<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "category_lang".
 *
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $language
 * @property string $slug
 *
 * @property Category $category
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name', 'language', 'slug'], 'required'],
            [['category_id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            ['description', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'category_id' => Yii::t('frontend', 'Category ID'),
            'name' => Yii::t('frontend', 'Name'),
            'language' => Yii::t('frontend', 'Language'),
            'slug' => Yii::t('frontend', 'Slug'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function getCatAttrTranslate(){
        return $this->hasMany(CatAttrLang::className(), ['cat_id' => 'category_id']);
    }
    public function getSlugTranslate(){
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'category_id'])->select('slug')->where(['!=','language',Yii::$app->language]);
    }
}
