<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "sliders".
 *
 * @property integer $id
 * @property integer $position
 * @property string $name
 * @property integer $order
 * @property string $status
 *
 * @property SliderImage[] $sliderImages
 */
class Sliders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'name', 'order', 'status'], 'required'],
            [['position', 'order'], 'integer'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'position' => Yii::t('backend', 'Position'),
            'name' => Yii::t('backend', 'Name'),
            'order' => Yii::t('backend', 'Order'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderImages()
    {
        return $this->hasMany(SliderImage::className(), ['slider_id' => 'id'])->with('sliderImageLangs');
    }
}
