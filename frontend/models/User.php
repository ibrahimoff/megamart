<?php

namespace frontend\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Address[] $addresses
 * @property Cart[] $carts
 * @property CartProduct[] $cartProducts
 * @property Loyality[] $loyalities
 * @property Order[] $orders
 * @property Profile[] $profiles
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    //public $password;
    public $file;
    public $address;
    public $number;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname','lastname','gender','date_birtday','address','number'], 'required'],
            [['firstname','lastname'], 'string', 'max' => 32],
            [['image'], 'string'],
            [['file'],'file'],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('frontend','username'),
            'date_birtday' => Yii::t('frontend','date_birthday'),
            'firstname' => Yii::t('frontend','firstname'),
            'lastname' => Yii::t('frontend','lastname'),
            'file' => Yii::t('frontend','image'),
            'number' => Yii::t('frontend','number'),
            'address' => Yii::t('frontend','address'),
            'gender' => Yii::t('frontend','gender'),
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password' =>  Yii::t('backend','password'),
            'password_reset_token' => 'Password Reset Token',
            'email' => Yii::t('backend','email'),
            'status' => Yii::t('backend','status'),
            'role' => Yii::t('backend','role'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function getAddr(){
        return $this->hasMany(UserAddress::className(),['user_id'=>'id']);
    }
    public function getNumbers(){
        return $this->hasMany(UserNumber::className(),['user_id'=>'id']);
    }


    public function getFullname(){
        return $this->firstname.' '.$this->lastname;
    }


}
