<?php

namespace frontend\models;

use backend\models\CatAttrValueLang;
use Yii;

/**
 * This is the model class for table "cat_attr_lang".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $cat_attr_id
 * @property string $name
 * @property string $language
 *
 * @property CatAttr $catAttr
 */
class CatAttrLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'cat_attr_id', 'name', 'language'], 'required'],
            [['cat_id', 'cat_attr_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
            [['slug'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'cat_id' => Yii::t('frontend', 'Cat ID'),
            'cat_attr_id' => Yii::t('frontend', 'Cat Attr ID'),
            'name' => Yii::t('frontend', 'Name'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttr()
    {
        return $this->hasOne(CatAttr::className(), ['id' => 'cat_attr_id']);
    }
    public function getCatAttrValueTranslate(){
        return $this->hasMany(CatAttrValueLang::className(), ['cat_attr_id' => 'cat_attr_id'])->where(['language'=>Yii::$app->language]);
    }
    public function getAttrByCategorySlug(){
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'cat_id']);
    }
}
