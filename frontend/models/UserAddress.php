<?php

namespace frontend\models;
use frontend\models\User;
use Yii;

/**
 * This is the model class for table "user_address".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $address
 */
class UserAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['address'], 'required'],
            [['user_id','main'], 'integer'],
            [['address'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'address' => Yii::t('frontend', 'Address'),
        ];
    }
    public function getUsr(){
        return $this->hasOne(User::className(),['id'=>'user_id'])->select(['id','firstname','lastname']);
    }
}
