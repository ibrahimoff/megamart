<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $cart_id
 * @property integer $currency_id
 * @property integer $address_id
 * @property integer $payment_id
 * @property integer $total
 * @property string $invoice
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Loyality[] $loyalities
 * @property Cart $cart
 * @property Currency $currency
 * @property Address $address
 * @property OrderDetail[] $orderDetails
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['user_id', 'cart_id', 'currency_id', 'address_id', 'payment_id', 'total', 'invoice', 'date', 'created_at', 'updated_at'], 'required'],
            [['user_id','address_id', 'payment_id', 'total'], 'integer'],
            [['date', 'created_at', 'updated_at','email','number', 'currency_id','type','total_points', 'cart_id', 'address'], 'safe'],
            [['invoice'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'cart_id' => Yii::t('frontend', 'Cart ID'),
            'currency_id' => Yii::t('frontend', 'Currency ID'),
            'address_id' => Yii::t('frontend', 'Address ID'),
            'payment_id' => Yii::t('frontend', 'Payment ID'),
            'total' => Yii::t('frontend', 'Total'),
            'invoice' => Yii::t('frontend', 'Invoice'),
            'date' => Yii::t('frontend', 'Date'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalities()
    {
        return $this->hasMany(Loyality::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetail::className(), ['order_id' => 'id'])->with('productName');
    }
}
