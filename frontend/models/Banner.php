<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $image
 * @property string $position
 * @property string $status
 *
 * @property BannerLang[] $bannerLangs
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'position', 'status'], 'required'],
            [['position', 'status'], 'string'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'image' => Yii::t('backend', 'Image'),
            'position' => Yii::t('backend', 'Position'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBannerLangs()
    {
        return $this->hasMany(BannerLang::className(), ['banner_id' => 'id']);
    }
    public function getBannerTranslate()
    {
        return $this->hasOne(BannerLang::className(), ['banner_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
    public static function getFooterBanners(){
        return BannerLang::find()->where(['language'=>Yii::$app->language])->joinWith('banner')->andFilterWhere(['position'=>'3'])->all();
    }
}
