<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "slider_image".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $image
 * @property integer $order
 * @property string $status
 *
 * @property Sliders $slider
 * @property SliderImageLang[] $sliderImageLangs
 */
class SliderImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slider_id', 'image', 'order', 'status'], 'required'],
            [['slider_id', 'order'], 'integer'],
            [['status'], 'string'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'slider_id' => Yii::t('backend', 'Slider ID'),
            'image' => Yii::t('backend', 'Image'),
            'order' => Yii::t('backend', 'Order'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Sliders::className(), ['id' => 'slider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderImageLangs()
    {
        return $this->hasMany(SliderImageLang::className(), ['image_id' => 'id']);
    }
    public function getSliderImageTranslate()
    {
        return $this->hasOne(SliderImageLang::className(), ['image_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
}
