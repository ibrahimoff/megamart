<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;
use yii\helpers\VarDumper;

/**
 * Signup form
 */
class WithoutSignup extends Model
{
    public $email;
    public $firstname;
    public $lastname;
  //  public $number;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            ['firstname', 'required'],
            ['firstname', 'string', 'max' => 12],
            ['lastname', 'required'],
            ['lastname', 'string', 'max' => 12]
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save()
    {
        if ($this->validate()) {
            $newUser = new User();
            $newUser->email = $this->email;
            $newUser->firstname = $this->firstname;
            $newUser->lastname = $this->lastname;
            $newUser->role = 1;
            $newUser->save(false);
            $session = Yii::$app->session['user'];
            $session['guest_id'] = $newUser->id;
            $session['email'] = $newUser->email;
            Yii::$app->session['user'] = $session;
            return true;
        }

        return null;
    }
}
