<?php

namespace frontend\models;

use frontend\models\CategoryLang;
use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $level
 * @property string $status
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property CategoryLang[] $categoryLangs
 * @property Product[] $products
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'level', 'order'], 'integer'],
            [['level', 'status', 'order', 'created_at', 'updated_at'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at','top'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'parent_id' => Yii::t('frontend', 'Parent ID'),
            'level' => Yii::t('frontend', 'Level'),
            'status' => Yii::t('frontend', 'Status'),
            'order' => Yii::t('frontend', 'Order'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    public function getChild()
    {
        return $this->hasOne(Category::className(), ['parent_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLangs()
    {
        return $this->hasMany(CategoryLang::className(), ['category_id' => 'id']);
    }
    public function getCategoryTranslate()
    {
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
    public function getTranslateOnChangeLang(){
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
    public static function getTopCategories(){
        $menuItemsCategories = [];
        $topCategories = CategoryLang::find()
            ->where(['language'=>Yii::$app->language])
            ->joinWith('category')
            ->andFilterWhere(['status'=>'1','top'=>1])
            ->asArray()
            ->all();

        foreach($topCategories as $category){
            $menuItemsCategories[] = [
                'label'=>$category['name'],
                'url'=>Url::link($category['slug'],'by-category')
            ];
        }
        return $menuItemsCategories;
    }
    public static function getAllCategories(){
        $categories = Category::find()->where(['status'=>1])->andWhere(['in','level',[1,2]])->with('categoryTranslate')->orderBy(['nleft'=>SORT_ASC])->all();
        return $categories;
    }
}
