<?php

namespace frontend\models;


use backend\models\ProductToCategory;
use Yii;
use yii\helpers\VarDumper;
use yii\db\Expression;
use frontend\models\ProductImage;
/**
 * This is the model class for table "product_lang".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property string $slug
 * @property string $language
 *
 * @property Product $product
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'name', 'short_description', 'slug', 'language'], 'required'],
            [['product_id'], 'integer'],
            [['short_description', 'description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
            'name' => Yii::t('frontend', 'Name'),
            'short_description' => Yii::t('frontend', 'Short Description'),
            'description' => Yii::t('frontend', 'Description'),
            'slug' => Yii::t('frontend', 'Slug'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->with('productImages','productAttrs','category','manufacturerName');
    }

    public function getProductOnly(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getDiscount(){
        return $this->hasOne(Discount::className(), ['product_id' => 'product_id'])->orderBy(['id'=>SORT_DESC]);
    }
    public function getAccessory($productId){
        $accessories = Accessory::find()->where(['product_id'=>$productId])->asArray()->select('acc_id')->column();
        $products = ProductLang::find()->where(['and',['in','product_id',$accessories],['language'=>Yii::$app->language]])->with(['productOnly','discount','productCoverImage'])->asArray()->all();
        return $products;
    }
    public function getSlugTranslate(){
        return $this->hasOne(ProductLang::className(), ['product_id' => 'product_id'])->select('slug')->where(['!=','language',Yii::$app->language]);
    }
    public function getRandomProducts($categoryId){
        $byCategoryRandomProducts = ProductLang::find()->with('pins')->joinWith(['productOnly','discount','productCoverImage'])->where(['product.category_id'=>$categoryId,'language'=>Yii::$app->language])->orderBy(new Expression('rand()'))->limit(20)->asArray()->all();
        return $byCategoryRandomProducts;
    }
//    public function getRandomDiscount($categoryId){
//        $byDiscountRandomProducts = ProductLang::find()->with('pins')->joinWith(['productOnly','discount','productCoverImage'])->where(['product.category_id'=>$categoryId,'language'=>Yii::$app->language,'product.on_sale'=>1])->orderBy(new Expression('rand()'))->limit(20)->asArray()->all();
//        VarDumper::dump($byDiscountRandomProducts,6,1);die();
//        return $byDiscountRandomProducts;
//    }


    public function getProductImages(){
        return $this->hasMany(ProductImage::className(),['product_id'=>'product_id']);
    }
    public function getProductCoverImage(){
        return $this->hasOne(ProductImage::className(),['product_id'=>'product_id'])->where(['cover'=>1]);
    }
    public function getProductToCategory(){
        return $this->hasMany(ProductToCategory::className(),['product_id'=>'product_id']);
    }
    public function getProductAttr(){
        return $this->hasMany(ProductAttr::className(),['product_id'=>'product_id']);
    }
    public function getPins()
    {
        return $this->hasMany(Pinbox::className(), ['product_id' => 'product_id']);
    }
}
