<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cart_product".
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $user_id
 * @property integer $product_id
 *
 * @property Cart $cart
 */
class CartProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['cart_id', 'user_id', 'product_id'], 'required'],
            [['cart_id', 'user_id', 'product_id','quantity'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'cart_id' => Yii::t('frontend', 'Cart ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
    public function getItem()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->with('discounts');
    }
    public function getItemLangs()
    {
        return $this->hasOne(ProductLang::className(), ['product_id' => 'product_id'])->with('productCoverImage')->where(['language'=>Yii::$app->language]);
    }
}
