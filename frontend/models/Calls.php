<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "calls".
 *
 * @property integer $id
 * @property string $name
 * @property string $number
 * @property string $status
 */
class Calls extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'number'], 'required'],
            [['status'], 'string'],
            [['name', 'number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'name'),
            'number' => Yii::t('frontend', 'number'),
            'status' => Yii::t('frontend', 'Status'),
        ];
    }
}
