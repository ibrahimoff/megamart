<?php
namespace frontend\models;
use Elastica\Client;
use Elastica\Request;
use yii\base\ErrorException;
use yii\elasticsearch\ActiveRecord;
use Yii;
use yii\helpers\VarDumper;
use yii\web\HttpException;

class ProductElastic extends ActiveRecord
{
    public function rules()
    {
        return [
            [['name'], 'string'],
        ];
    }

    public function attributes()
    {
        return ['id', 'name', 'address', 'registration_date'];
    }
    public static function findItem($params){
        $client = new Client();
        $check = $client->request(Yii::$app->params['index'], $method = Request::HEAD)->getStatus();
        if($check == 200){
            $queryString = '';
            $prevKey = '';
            $i = 1;
            //$appLang = !empty(Yii::$app->request->post('lang'))?Yii::$app->request->post('lang'):Yii::$app->language;
//            foreach($params as $key=>$values){
//                foreach ($values as $k=>$value){
//
//                    if($key == $prevKey ){
//                        if($k == 'name'){
//                            $queryString .='&'.$k.':*'.$value.'*';
//                        }else{
//                            $queryString .='&'.$k.':'.$value;
//                        }
//                    }else{
//                        $queryString .= '&'.$key.'='.$k.':'.$value;
//                    }
//                    $prevKey = $key;
//                }
//            }

            if(Yii::$app->request->isAjax){
                $type = 'ru,az';
            }else{
                $type = Yii::$app->language;
            }

            $url = '/'.Yii::$app->params['index'].'/'.$type.'/_search?q='.$params['query'].'&pretty=true';
            $client = new Client();
            $result = $client->request($url, $method = Request::GET)->getData();
            return $result['hits']['hits'];
        }else{
            throw new HttpException($check,'No index founded');
        }

    }
}