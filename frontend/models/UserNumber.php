<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_number".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $number
 */
class UserNumber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_number';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number'], 'required'],
            [['user_id'], 'integer'],
            [['number'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'number' => Yii::t('frontend', 'Number'),
        ];
    }
}
