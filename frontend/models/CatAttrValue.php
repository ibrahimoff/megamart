<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cat_attr_value".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $cat_attr_id
 *
 * @property CatAttr $catAttr
 * @property CatAttrValueLang[] $catAttrValueLangs
 */
class CatAttrValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'cat_attr_id'], 'required'],
            [['cat_id', 'cat_attr_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'cat_id' => Yii::t('frontend', 'Cat ID'),
            'cat_attr_id' => Yii::t('frontend', 'Cat Attr ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttr()
    {
        return $this->hasOne(CatAttr::className(), ['id' => 'cat_attr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValueLangs()
    {
        return $this->hasMany(CatAttrValueLang::className(), ['cat_attr_value_id' => 'id']);
    }
}
