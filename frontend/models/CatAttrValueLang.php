<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cat_attr_value_lang".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property integer $cat_attr_id
 * @property integer $cat_attr_value_id
 * @property string $name
 * @property string $language
 *
 * @property CatAttrValue $catAttrValue
 */
class CatAttrValueLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_value_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'cat_attr_id', 'cat_attr_value_id', 'name', 'language'], 'required'],
            [['cat_id', 'cat_attr_id', 'cat_attr_value_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'cat_id' => Yii::t('frontend', 'Cat ID'),
            'cat_attr_id' => Yii::t('frontend', 'Cat Attr ID'),
            'cat_attr_value_id' => Yii::t('frontend', 'Cat Attr Value ID'),
            'name' => Yii::t('frontend', 'Name'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValue()
    {
        return $this->hasOne(CatAttrValue::className(), ['id' => 'cat_attr_value_id']);
    }
}
