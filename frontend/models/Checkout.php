<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/26/16
 * Time: 3:20 PM
 */
namespace frontend\models;
use backend\models\UserAddress;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Checkout extends \yii\base\Model{
    private $step;
    public $type;
    public $payment;

    public static function saveOrder($paymentId){
        $totalPoints = 0;
        $totalPrice = 0;
        if(!empty(Yii::$app->session['user']['guest_id'])){
            $userId = Yii::$app->session['user']['guest_id'];
            $products = Yii::$app->session['bucket'];
            foreach($products as $product){

                $totalPoints += $product['bonus'];
                $totalPrice += $product['new_price'] != '' ? $product['new_price'] :$product['price'];
            }
            $userAddress = Yii::$app->session['user']['address'];
        }else{
            if(!Yii::$app->user->isGuest){
                $card = Cart::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0'])->with('cartProducts')->one();
                $products = [];
                $address = UserAddress::find()->where(['id'=>Yii::$app->session['user']['address']])->one();
                if(!empty($address)){
                    $userAddress = $address->address;
                }else{
                    $userAddress = Yii::$app->session['user']['address'];
                }
                foreach($card['cartProducts'] as $product){
                    $products[$product['product_id']]['name'] = $product->itemLangs->name;
                    $products[$product['product_id']]['price'] = $product->item->price;
                    $products[$product['product_id']]['new_price'] = !empty($product->item->discounts) ? $product->item->discounts->price: '';
                    $products[$product['product_id']]['image'] = Yii::getAlias('@web/uploads/products/').$product->itemLangs->productCoverImage->product_id.'-'.$product->itemLangs->productCoverImage->id;
                    $products[$product['product_id']]['slug'] = $product->itemLangs->slug;
                    $products[$product['product_id']]['quantity'] = $product->quantity;
                    $products[$product['product_id']]['bonus'] = $product->item->bonus;
                    $totalPoints += $product->item->bonus;
                    $totalPrice += $product->item->on_sale == 1 ? $product->item->discounts->price : $product->item->price;
                }
                $userId = Yii::$app->user->id;
            }
        }
        $newOrder = new Order();
        $newOrder->user_id = $userId;
        $newOrder->email = !empty(Yii::$app->session['user']['guest_id']) ? Yii::$app->session['user']['email'] :Yii::$app->user->identity->email;
        $newOrder->cart_id =!empty(Yii::$app->session['user']['guest_id']) ? null : $card->id;
        $newOrder->number = Yii::$app->session['user']['number'];
        $newOrder->address = $userAddress;
        $newOrder->payment_id = $paymentId;
        $newOrder->status = '0';
        $newOrder->total_points = $totalPoints;
        $newOrder->total = $totalPrice;
        $newOrder->created_at = date('Y-m-d H:i:s');
        if($newOrder->save()){
            foreach($products as $key=>$value){
                $orderDetail = new OrderDetail();
                $orderDetail->order_id = $newOrder->id;
                $orderDetail->product_id = $key;
                $orderDetail->quantity = $value['quantity'];
                $orderDetail->price = $value['new_price'] != ''? $value['new_price'] : $value['price'];
                $orderDetail->bonus = $value['bonus'];
                $orderDetail->save(false);
            }
            if(!empty($card)){
                $card->status = '1';
                $card->save();
            }
            $hash = base64_encode($newOrder->id.','.$newOrder->created_at);
            if(!empty(Yii::$app->session['bucket'])){
                unset(Yii::$app->session['bucket']);
            }
            if(!empty(Yii::$app->session['user'])){
                unset(Yii::$app->session['user']);
            }

            return $hash;
        }

    }

    public static function saveToCard(){
        if(!empty(Yii::$app->session['bucket'])){
            $anotherCard = Cart::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0'])->one();
            if(empty($anotherCard)){
                $card = new Cart();
                $card->user_id = Yii::$app->user->id;
                $card->status = '0';
                $card->created_at = date('Y-m-d H:i:s');
                if($card->save()){
                    foreach(Yii::$app->session['bucket'] as $key=>$product){
                        $newCardProduct = new CartProduct();
                        $newCardProduct->product_id = $key;
                        $newCardProduct->user_id = Yii::$app->user->id;
                        $newCardProduct->cart_id = $card->id;
                        $newCardProduct->quantity = $product['quantity'];
                        $newCardProduct->save(false);
                    }
                    unset(Yii::$app->session['bucket']);
                }
            }else{
                unset(Yii::$app->session['bucket']);
            }
        }
    }
    public static function getPayments(){
        $payments = PaymentLang::find()
            ->where(['language'=>Yii::$app->language])
            ->joinWith('payment')
            ->andFilterWhere(['status'=>'1'])
            ->orderBy(['id'=>SORT_DESC])
            ->asArray()
            ->all();

        return array_reverse($payments);
    }

    protected static function saveSession($key,$val){
        $session = Yii::$app->session;
        $session[$key] = $val;
        Yii::$app->session[$key] = $session;
    }
}