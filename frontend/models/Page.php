<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $order
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PageLang[] $pageLangs
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order', 'status', 'created_at', 'updated_at'], 'required'],
            [['order'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'order' => Yii::t('frontend', 'Order'),
            'status' => Yii::t('frontend', 'Status'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageLangs()
    {
        return $this->hasMany(PageLang::className(), ['page_id' => 'id']);
    }
    public function getPage()
    {
        return $this->hasOne(PageLang::className(), ['page_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
}
