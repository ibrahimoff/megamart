<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/26/16
 * Time: 3:20 PM
 */
namespace frontend\models;
use Yii;
use yii\helpers\VarDumper;

class AddressForm extends \yii\base\Model{
    public $address;
    public $number;
    public function rules()
    {
        return [
            [['address','number'],'required'],
            ['address','string'],
        ];
    }

    public function save(){
        $session = Yii::$app->session['user'];
        $session['address'] = $this->address;
        $session['number'] = $this->number;
        Yii::$app->session['user'] = $session;
        return true;
    }

}