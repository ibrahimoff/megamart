<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "page_lang".
 *
 * @property integer $id
 * @property integer $page_id
 * @property string $name
 * @property string $description
 * @property string $slug
 * @property string $language
 *
 * @property Page $page
 */
class PageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'name', 'description', 'slug', 'language'], 'required'],
            [['page_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'page_id' => Yii::t('frontend', 'Page ID'),
            'name' => Yii::t('frontend', 'Name'),
            'description' => Yii::t('frontend', 'Description'),
            'slug' => Yii::t('frontend', 'Slug'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    public function getPageSlugTranslate(){
        return $this->hasOne(PageLang::className(), ['page_id' => 'page_id'])->where(['!=','language',Yii::$app->language]);
    }
}
