<?php

namespace frontend\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Response;

/**
 * This is the model class for table "cart".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $status
 * @property integer $session_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CartProduct[] $cartProducts
 * @property Order[] $orders
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['id', 'user_id', 'status', 'session_id', 'created_at', 'updated_at'], 'required'],
            [['id', 'user_id', 'session_id'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'status' => Yii::t('frontend', 'Status'),
            'session_id' => Yii::t('frontend', 'Session ID'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartProducts()
    {
        return $this->hasMany(CartProduct::className(), ['cart_id' => 'id'])->with('itemLangs','item');
    }
    public function getItems()
    {
        return $this->hasMany(CartProduct::className(), ['cart_id' => 'id']);
    }
    public static function checkCard(){
        if(Yii::$app->user->isGuest){
            $cardProduct = is_array(Yii::$app->session['bucket'])?array_keys(Yii::$app->session['bucket']):[];
        }else{
            $cardProduct = CartProduct::find()->where(['cart_product.user_id'=>Yii::$app->user->id])->joinWith('cart')->andFilterWhere(['status'=>'0'])->select('product_id')->column();
        }
        return $cardProduct;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['cart_id' => 'id']);
    }
    public static function bucket(){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->post('id');
            $data = [];

            if(Yii::$app->user->isGuest){
                $product = ProductLang::find()
                    ->where(['product_id'=>$id,'language'=>Yii::$app->language])
                    ->with(['product','discount','productCoverImage'])
                    ->asArray()
                    ->one();
                    Yii::$app->session->open();
                    $session = Yii::$app->session['bucket'];
                    $id = $id . '';
                    $session[$id] = [
                        'name' => $product['name'],
                        'price' => $product['product']['price'],
                        'new_price' => !empty($product['discount']['price']) ? $product['discount']['price'] : '',
                        'image' => Yii::getAlias('@web/uploads/products/' . $product['productCoverImage']['product_id'] . '-' . $product['productCoverImage']['id']),
                        'slug' => Url::link($product['slug'], '_p'),
                        'quantity' => 1,
                        'bonus'=>$product['product']['bonus']*1
                    ];
                    Yii::$app->session['bucket'] = $session;
                    $data[$id] = [
                        'name' => $product['name'],
                        'price' => $product['product']['price'],
                        'image' => Yii::getAlias('@web/uploads/products/' . $product['productCoverImage']['product_id'] . '-' . $product['productCoverImage']['id'] . '-small.jpg'),
                        'slug' => Url::link($product['slug'], '_p'),
                        'quantity' => 1
                    ];
                    return $data;
            }else{
                $card = Cart::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0'])->orderBy(['id'=>SORT_DESC])->one();
                if(!empty($card)){
                    $cardProduct = CartProduct::find()
                        ->where(['cart_id'=>$card->id,'user_id'=>Yii::$app->user->id,'product_id'=>$id])
                        ->select('product_id')
                        ->column();
                    if(!in_array($id,$cardProduct)){
                        $newCardProduct = new CartProduct();
                        $newCardProduct->user_id = Yii::$app->user->id;
                        $newCardProduct->product_id = $id;
                        $newCardProduct->cart_id = $card->id;
                        $newCardProduct->quantity = 1;
                        $newCardProduct->save();
                    }else{
                        return false;
                    }
                }else{
                    $newCard = new Cart();
                    $newCard->user_id = Yii::$app->user->id;
                    $newCard->status = '0';
                    $newCard->created_at = date('Y-m-d H:i:s');
                    if($newCard->save()){
                        $newCardProduct = new CartProduct();
                        $newCardProduct->user_id = Yii::$app->user->id;
                        $newCardProduct->product_id = $id;
                        $newCardProduct->cart_id = $newCard->id;
                        $newCardProduct->quantity = 1;
                        $newCardProduct->save();
                    }
                }
                $data[$newCardProduct->product_id]=[
                    'name'=>$newCardProduct->itemLangs->name,
                    'slug'=>Url::link($newCardProduct->itemLangs->slug,'_p'),
                    'price'=>$newCardProduct->item->price,
                    'quantity'=>$newCardProduct->quantity,
                    'image'=>Yii::getAlias('@web/uploads/products/'.$newCardProduct->itemLangs->productCoverImage->product_id.'-'.$newCardProduct->itemLangs->productCoverImage->id.'-small.jpg'),
                ];
                return $data;

            }
        }
    }
    public static function quantity(){
        if(Yii::$app->request->isAjax){
            $type = Yii::$app->request->post('type');
            $id = Yii::$app->request->post('id');
            $item='';
            if(Yii::$app->user->isGuest){
                $session = Yii::$app->session['bucket'];
                if($type == 'decrease'){
                    if(Yii::$app->session['bucket'][$id]['quantity'] > 1){
                        $item= Yii::$app->session['bucket'][$id]['quantity']-1;
                    }
                }elseif($type == 'increase'){
                    $item =  Yii::$app->session['bucket'][$id]['quantity']+1;
                };
                $session[$id]['quantity'] = $item;
                Yii::$app->session['bucket'] = $session;
            }else{
                $userId = Yii::$app->user->id;
                $cardId = Cart::find()->where(['user_id'=>$userId,'status'=>'0'])->orderBy(['id'=>SORT_DESC])->one();
                $cardProduct = CartProduct::find()->where(['cart_id'=>$cardId->id,'product_id'=>$id])->one();
                if($type == 'decrease'){

                    if($cardProduct->quantity > 1){
                        $item= $cardProduct->quantity -1;
                        if($item == 0){
                            $item =1;
                        }
                    }
                }elseif($type == 'increase'){

                    $item =  $cardProduct->quantity+1;
                };
                $cardProduct->quantity = $item;
                $cardProduct->save(false);
            }
            return true;
        }
        return false;
    }
}
