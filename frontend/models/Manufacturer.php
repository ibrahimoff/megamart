<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "manufacturer".
 *
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property string $status
 * @property integer $deleted
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'image', 'status', 'deleted'], 'required'],
            [['status'], 'string'],
            [['deleted'], 'integer'],
            [['name', 'image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'name' => Yii::t('frontend', 'Name'),
            'image' => Yii::t('frontend', 'Image'),
            'status' => Yii::t('frontend', 'Status'),
            'deleted' => Yii::t('frontend', 'Deleted'),
        ];
    }
}
