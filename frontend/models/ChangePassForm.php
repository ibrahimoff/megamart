<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Login form
 */
class ChangePassForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $confirmPassword;

    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['oldPassword', 'newPassword', 'confirmPassword'], 'required'],
            // rememberMe must be a boolean value
            // password is validated by validatePassword()
            [['oldPassword'], 'validatePassword'],
            [['confirmPassword'], 'checkPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'oldPassword' => Yii::t('frontend','old_pass'),
            'newPassword' => Yii::t('frontend','new_pass'),
            'confirmPassword' => Yii::t('frontend','confirm_pass'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->oldPassword)) {
                $this->addError($attribute, 'Şifrə düzgün deyil.');
            }
        }
    }

    public function checkPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->newPassword != $this->confirmPassword) {
                $this->addError($attribute, 'Şifrələr uyğun deyil.');
            }
        }
    }
    public function setPassword()
    {
        $user = $this->getUser();
        $user->setPassword($this->confirmPassword);
        $user->save();
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername(Yii::$app->user->identity->username);
        }

        return $this->_user;
    }
}