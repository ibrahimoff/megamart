<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "slider_image_lang".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $language
 * @property string $link
 * @property string $value
 *
 * @property SliderImage $image
 */
class SliderImageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_image_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'language', 'link', 'value'], 'required'],
            [['image_id'], 'integer'],
            [['language'], 'string', 'max' => 2],
            [['link', 'value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'image_id' => Yii::t('backend', 'Image ID'),
            'language' => Yii::t('backend', 'Language'),
            'link' => Yii::t('backend', 'Link'),
            'value' => Yii::t('backend', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(SliderImage::className(), ['id' => 'image_id']);
    }
}
