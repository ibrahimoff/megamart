<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "payment_lang".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property string $name
 * @property string $description
 * @property string $language
 *
 * @property Payment $payment
 */
class PaymentLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_id', 'name', 'language'], 'required'],
            [['payment_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'payment_id' => Yii::t('frontend', 'Payment ID'),
            'name' => Yii::t('frontend', 'Name'),
            'description' => Yii::t('frontend', 'Description'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }
}
