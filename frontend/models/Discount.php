<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $group_id
 * @property integer $old_price
 * @property integer $price
 * @property string $from_date
 * @property string $to_date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $product
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'user_id', 'group_id', 'old_price', 'price', 'from_date', 'to_date', 'status', 'created_at', 'updated_at'], 'required'],
            [['product_id', 'user_id', 'group_id', 'old_price', 'price'], 'integer'],
            [['from_date', 'to_date', 'created_at', 'updated_at'], 'safe'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'group_id' => Yii::t('frontend', 'Group ID'),
            'old_price' => Yii::t('frontend', 'Old Price'),
            'price' => Yii::t('frontend', 'Price'),
            'from_date' => Yii::t('frontend', 'From Date'),
            'to_date' => Yii::t('frontend', 'To Date'),
            'status' => Yii::t('frontend', 'Status'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
