<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property NewsLang[] $newsLangs
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'status', 'created_at', 'updated_at'], 'required'],
            [['date', 'created_at', 'updated_at'], 'safe'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'date' => Yii::t('frontend', 'Date'),
            'status' => Yii::t('frontend', 'Status'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsLangs()
    {
        return $this->hasMany(NewsLang::className(), ['news_id' => 'id']);
    }
    public function getNews(){
        return $this->hasOne(NewsLang::className(), ['news_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
}
