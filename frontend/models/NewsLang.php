<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "news_lang".
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property string $slug
 * @property string $language
 *
 * @property News $news
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_id', 'name', 'description', 'short_description', 'slug', 'language'], 'required'],
            [['news_id'], 'integer'],
            [['description', 'short_description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'news_id' => Yii::t('frontend', 'News ID'),
            'name' => Yii::t('frontend', 'Name'),
            'description' => Yii::t('frontend', 'Description'),
            'short_description' => Yii::t('frontend', 'Short Description'),
            'slug' => Yii::t('frontend', 'Slug'),
            'language' => Yii::t('frontend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
