<?php

namespace frontend\models;

use backend\models\CatAttrValueLang;
use Yii;

/**
 * This is the model class for table "product_attr".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attr_id
 * @property integer $attr_value_id
 *
 * @property Product $product
 */
class ProductAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'attr_id', 'attr_value_id'], 'required'],
            [['product_id', 'attr_id', 'attr_value_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
            'attr_id' => Yii::t('frontend', 'Attr ID'),
            'attr_value_id' => Yii::t('frontend', 'Attr Value ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getAttributeName(){
        return $this->hasOne(CatAttrLang::className(), ['cat_attr_id' => 'attr_id']);
    }
    public function getAttributeValueName(){
        return $this->hasOne(CatAttrValueLang::className(), ['cat_attr_value_id' => 'attr_value_id']);
    }
}
