<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property string $logo
 * @property string $status
 * @property integer $deleted
 *
 * @property PaymentLang[] $paymentLangs
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'status', 'deleted'], 'required'],
            [['status'], 'string'],
            [['deleted'], 'integer'],
            [['logo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'logo' => Yii::t('frontend', 'Logo'),
            'status' => Yii::t('frontend', 'Status'),
            'deleted' => Yii::t('frontend', 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLangs()
    {
        return $this->hasMany(PaymentLang::className(), ['payment_id' => 'id']);
    }
}
