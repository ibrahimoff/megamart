<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "cat_attr".
 *
 * @property integer $id
 * @property integer $cat_id
 * @property string $unit
 * @property string $status
 *
 * @property CatAttrLang[] $catAttrLangs
 * @property CatAttrValue[] $catAttrValues
 */
class CatAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cat_id', 'unit', 'status'], 'required'],
            [['cat_id'], 'integer'],
            [['status'], 'string'],
            [['unit'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'cat_id' => Yii::t('frontend', 'Cat ID'),
            'unit' => Yii::t('frontend', 'Unit'),
            'status' => Yii::t('frontend', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrLangs()
    {
        return $this->hasMany(CatAttrLang::className(), ['cat_attr_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValues()
    {
        return $this->hasMany(CatAttrValue::className(), ['cat_attr_id' => 'id']);
    }
}
