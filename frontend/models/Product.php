<?php

namespace frontend\models;

use backend\models\Loyality;
use backend\models\ProductToCategory;
use frontend\models\ProductLang;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $manufacturer_id
 * @property integer $supplier_id
 * @property string $image
 * @property integer $on_sale
 * @property integer $quantity
 * @property integer $price
 * @property integer $wholesale_price
 * @property integer $cost_price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Discount[] $discounts
 * @property Loyality[] $loyalities
 * @property Category $category
 * @property ProductAttr[] $productAttrs
 * @property ProductImage[] $productImages
 * @property ProductLang[] $productLangs
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'manufacturer_id', 'supplier_id', 'image', 'on_sale', 'quantity', 'price', 'wholesale_price', 'cost_price', 'status', 'created_at', 'updated_at'], 'required'],
            [['category_id', 'manufacturer_id', 'supplier_id', 'on_sale', 'quantity', 'price', 'wholesale_price', 'cost_price'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at','manufacturer'], 'safe'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'category_id' => Yii::t('frontend', 'Category ID'),
            'manufacturer_id' => Yii::t('frontend', 'Manufacturer ID'),
            'supplier_id' => Yii::t('frontend', 'Supplier ID'),
            'image' => Yii::t('frontend', 'Image'),
            'on_sale' => Yii::t('frontend', 'On Sale'),
            'quantity' => Yii::t('frontend', 'Quantity'),
            'price' => Yii::t('frontend', 'Price'),
            'wholesale_price' => Yii::t('frontend', 'Wholesale Price'),
            'cost_price' => Yii::t('frontend', 'Cost Price'),
            'status' => Yii::t('frontend', 'Status'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasOne(Discount::className(), ['product_id' => 'id'])->orderBy(['id'=>SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalities()
    {
        return $this->hasMany(Loyality::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id'])->with('categoryTranslate');
    }
    public function getCategoryT()
    {
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'category_id'])->where(['language'=>'ru'])->with('category');
    }
    public function getManufacturerName(){
        return $this->hasOne(Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttrs()
    {
        return $this->hasMany(ProductAttr::className(), ['product_id' => 'id'])->with('attributeName','attributeValueName');
    }

    public function getAccessory(){
        return $this->hasMany(Accessory::className(), ['product_id' => 'id'])->with('product');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    public static function getDiscountPercent($oldPrice,$newPrice){
        $diff = ($newPrice/$oldPrice)*100;
        $percent = 100 - $diff;
        $percentRounded = round($percent,0);
        return $percentRounded.'%';
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLangs()
    {
        return $this->hasMany(ProductLang::className(), ['product_id' => 'id']);
    }
    public function getProductName(){
        return $this->hasOne(ProductLang::className(),['product_id'=>'id'])->where(['language'=>Yii::$app->language]);
    }

    public static function getPinCount($id){
        return Pinbox::find()->where(['product_id'=>$id])->count();
    }
    public function getProductToCategory(){
        return $this->hasMany(ProductToCategory::className(),['product_id'=>'id']);
    }
}
