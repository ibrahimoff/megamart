<?php

namespace frontend\models;


use Yii;

/**
 * This is the model class for table "accessory".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $acc_id
 */
class Accessory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accessory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'acc_id'], 'required'],
            [['product_id', 'acc_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
            'acc_id' => Yii::t('frontend', 'Acc ID'),
        ];
    }

//    public function getProduct()
//    {
//        return $this->hasOne(Product::className(), ['id' => 'product_id'])->with('productName');
//    }
}
