<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/18/16
 * Time: 3:27 PM
 */

namespace frontend\controllers;


use backend\models\ProductToCategory;
use Elastica\Client;
use Elastica\Request;
use frontend\models\Category;
use frontend\models\CategoryLang;
use frontend\models\Product;
use frontend\models\ProductLang;
use frontend\widgets\Filter\Filter;
use yii\data\Pagination;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\Session;

class ProductController extends Controller
{
    public function actionView($productSlug){
        $model = $this->findProduct($productSlug);

        $accessories = $model->getAccessory($model->product_id);
        $randomItems = $model->getRandomProducts($model->product->category_id);
        $randomItemsWidget = \frontend\widgets\Product\Product::widget(['products'=>$randomItems,'type'=>1]);
        $accessoryWidget = \frontend\widgets\Product\Product::widget(['products'=>$accessories,'type'=>1,'widgetId'=>'accessory']);
        return $this->render('view',['model'=>$model,'accessoryWidget'=>$accessoryWidget,'randomItemsWidget'=>$randomItemsWidget]);
    }
    public function actionFilterProduct($categorySlug){
        $category = $this->findCategory($categorySlug);
        $description = $category->description;
        $selector = Product::find()->joinWith('productToCategory')->andFilterWhere(['product_to_category.category_id'=>$category->category_id]);
        $minPrice = $selector->min('price');
        $maxPrice = $selector->max('price');
        $filterWidget = Filter::widget(['categorySlug'=>$categorySlug,'categoryId'=>$category->category_id,'maxPrice'=>$maxPrice,'minPrice'=>$minPrice]);
        $gte = !empty(Yii::$app->request->get('Filter')['gte'])?Yii::$app->request->get('Filter')['gte']: 0;
        $priceArray = [];
        $terms =[];
        if(!empty(Yii::$app->request->get('Filter')['lte'])){
            $lte = Yii::$app->request->get('Filter')['lte'];
            $priceArray = [
                '_product.productOnly.price'=>[
                    'gte'=>$gte,
                    'lte'=>$lte
                ]
            ];
        }else{
            $priceArray = [
                '_product.productOnly.price'=>[
                    'gte'=>$gte,
                ]
            ];
        }
        $terms = [
           // ['range'=>$priceArray],
           // ['term'=>['_product.productToCategory.category_id'=>$category->category_id]],

        ];
        if(!empty(Yii::$app->request->get('Filter'))){

            if(!empty(Yii::$app->request->get('Filter')['attributes'])){
                $attributesArray = explode(',',Yii::$app->request->get('Filter')['attributes']);
                $terms[] = ['terms'=>['_product.productAttr.attr_value_id'=>$attributesArray]];
            }
            if(!empty(Yii::$app->request->get('Filter')['manufacturers'])){
                $manufacturersArray = explode(',',Yii::$app->request->get('Filter')['manufacturers']);
                $terms[] = ['terms'=>['_product.productOnly.manufacturer_id'=>[1]]];
            }
            $query = [
                'query'=>[
                    'filtered'=>[
                        'query'=>[
                            ['term'=>['_product.language'=>Yii::$app->language]],
                        ],
                        'filter'=>[
                            'bool' => [
                                'must' =>$terms
                            ],

                        ]
                    ],
                ]
            ];
            //VarDumper::dump($query['query']['filtered']['filter']['bool']['must'],6,1);
            $path = '/products/ru,az/_search';
            $client = new Client();
            $response = $client->request($path, Request::GET, $query);
            $responseArray = $response->getData();
            return $this->render('index',['products'=>$responseArray['hits']['hits'],'filterWidget'=>$filterWidget,'category'=>$category]);
        }

        $products = ProductLang::find()->where(['language'=>Yii::$app->language])->with('productCoverImage','productOnly','discount')->joinWith('productToCategory')->andFilterWhere(['category_id'=>$category->category_id])->orderBy(['product_lang.product_id'=>SORT_DESC])->asArray();
        $countQuery = clone $products;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $products->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $productWidget = \frontend\widgets\Product\Product::widget(['products'=>$models,'type'=>\frontend\widgets\Product\Product::WHITE_PRODUCT,'grid'=>'col-lg-15 col-md-20 col-sm-30 col-xs-60']);
        return $this->render('index',['productWidget'=>$productWidget,'pages'=>$pages,'filterWidget'=>$filterWidget,'category'=>$category,'description'=>$description]);
    }
    public function actionGetProduct(){
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->post('id');
            $product = Product::find()->where(['id'=>$id])->one();
            $data = [
                'product'=>$product
            ];
            return $data;
        }
        return false;
    }
    public function actionSwitcher(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $session = Yii::$app->session['switcher'];
            if($session != null){
                if(in_array($id,$session)){
                    unset($session[$id]);
                }else{
                    $session[$id] = $id;
                }
                Yii::$app->session['switcher'] = $session;
            }else{
                $session = Yii::$app->session['switcher'];
                $session[$id] = $id;
                Yii::$app->session['switcher'] = $session;
            }
            return true;
        }
        return false;
    }
    protected function findProduct($slug){
        if (($product = ProductLang::find()->where(['slug'=>$slug])->joinWith('product')->andFilterWhere(['status'=>'1'])->one()) !== null) {
            return $product;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    protected function findCategory($slug)
    {
        if (($categorySlug = CategoryLang::find()->where(['slug'=>$slug])->one()) !== null) {
            $category = CategoryLang::find()->where(['and',['category_id'=>$categorySlug->category_id],['language'=>Yii::$app->language]])->one();
            return $category;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}

