<?php


namespace frontend\controllers;

use Elastica\Client;
use Elastica\Document;
use Elastica\Request;
use frontend\models\Banner;
use frontend\models\Calls;

use frontend\models\Category;
use frontend\models\CategoryLang;
use frontend\models\ChangePassForm;
use frontend\models\Checkout;
use frontend\models\ProductElastic;
use frontend\models\ProductLang;
use common\models\User;
use frontend\models\Pinbox;
use frontend\models\PageLang;
use frontend\models\UserAddress;
use frontend\models\UserNumber;
use frontend\widgets\Banners\Banners;
use frontend\widgets\Categories\Categories;
use frontend\widgets\HotDeals\HotDeals;
use frontend\widgets\SaleOut\SaleOut;
use frontend\widgets\Pinbox\PinboxWidget;
use yii\data\Pagination;
use yii\imagine\Image;
use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\base\InvalidParamException;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','login'],
                'rules' => [
                    [
                        'actions' => ['signup','login','error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'checklist'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'successUrl'=>strpos(Yii::$app->request->referrer, 'login') !==false? '/?'.time().rand(99999,9999999) : Yii::$app->request->referrer.'?'.time().rand(99999,9999999)
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
     
     
    public function successCallback($client) {
      $userAttributes = $client->getUserAttributes();
      $model = new LoginForm();
      $model->socialLogin($userAttributes);

    }
         
    public function actionIndex()
    {
        $hotDealsWidget = HotDeals::widget();
        $categories = Categories::widget();
        $bannersWidget = Banners::widget(['position'=>'2']);
        $saleOutWidget = SaleOut::widget();
        $latestProductsWidget = SaleOut::widget();
        $bestsellersWidget = SaleOut::widget();
        $footerBanner = Banner::getFooterBanners();
        //$latestProductsWidget = Latest::widget();
        //$bestsellersWidget = Bestsellers::widget();

        return $this->render('index',[
            'footerBanner'=>$footerBanner,
            'categories'=>$categories,
            'hotDealsWidget'=>$hotDealsWidget,
            'bannersWidget'=>$bannersWidget,
            'saleOutWidget'=>$saleOutWidget,
            'latestProductsWidget'=>$latestProductsWidget,
            'bestsellersWidget'=>$bestsellersWidget
        ]);
    }

    public function actionDiscounts(){
        $products = ProductLang::find()
            ->where(['language'=>\Yii::$app->language])
            ->with(['discount','productCoverImage','pins'])
            ->joinWith('productOnly')
            ->andFilterWhere(['on_sale'=>'1'])
            ->asArray();
        $countQuery = clone $products;
        $pages = new Pagination(['totalCount' => $countQuery->count()]);
        $models = $products->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $productWidget = \frontend\widgets\Product\Product::widget(['products'=>$models,'type'=>1]);
        return $this->render('discounts',['productWidget'=>$productWidget,'pages'=>$pages]);
    }
    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                 Checkout::saveToCard();
                $_SESSION['user']['pins'] = [];
                $pins = Pinbox::find()->where(['user_id'=>Yii::$app->user->id])->select('product_id')->asArray()->all();
                foreach($pins as $pin){
                    $_SESSION['user']['pins'][] = $pin['product_id'];
                }

            }

            return $this->redirect('/');
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionPage($slug){
        $pageContent = PageLang::find()->where(['language'=>Yii::$app->language,'slug'=>$slug])->asArray()->one();
        return $this->render('static',['pageContent'=>$pageContent]);
    }
    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        unset(Yii::$app->session['inCart']);
        unset(Yii::$app->session['bucket']);
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending email.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionPinUnpin(){
        if(Yii::$app->request->isAjax){
            if(!isset($_SESSION['user']['pins'])){
                $_SESSION['user'] = [];
                $_SESSION['user']['pins'] = [];
            }

            if(!yii::$app->user->isGuest){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $productId = Yii::$app->request->get('id');
                $userId = Yii::$app->user->id;
                $pinbox = Pinbox::find()->where(['user_id' => $userId, 'product_id' => $productId])->one();
                if(!$pinbox){
                    $pinbox = new Pinbox();
                    $pinbox->user_id = $userId;
                    $pinbox->product_id = $productId;

                    if($pinbox->save()){
                        $_SESSION['user']['pins'][] = $productId;
                        return ['checked'=>1];
                    }
                } else {
                    $pinbox->delete();
                    $key=array_search($productId,$_SESSION['user']['pins']);
                    if($key!==false)
                        unset($_SESSION['user']['pins'][$key]);
                    $_SESSION['user']['pins'] = array_values($_SESSION['user']['pins']);
                    return ['checked'=>0];
                }
            } else{
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

                $productId = Yii::$app->request->get('id');
                if(!in_array($productId,$_SESSION['user']['pins'])){
                    $_SESSION['user']['pins'][] = $productId;
                    return ['checked'=>1];
                } else {
                    $key=array_search($productId,$_SESSION['user']['pins']);
                    if($key!==false)
                        unset($_SESSION['user']['pins'][$key]);
                    $_SESSION['user']['pins'] = array_values($_SESSION['user']['pins']);
                    return ['checked'=>0];
                }
            }
        }
    }

    public function actionChecklist()
    {
        $productWidget = PinboxWidget::widget();

        return $this->render('pins',['productWidget'=>$productWidget]);
    }

    public function actionEditProfile()
    {
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->id;
            $user = \frontend\models\User::find()->where(['id' => $id])->with('addr')->one();
            $addresses = UserAddress::find()->where(['user_id'=>$id])->select('address')->asArray()->all();
            $numbers = UserNumber::find()->where(['user_id'=>$id])->select('number')->asArray()->all();
            // VarDumper::dump($numbers,6,1);die();
            if ($user->load(Yii::$app->request->post())) {
                $user->file = UploadedFile::getInstance($user, 'file');
                if (!empty($user->file)) {
                    if(!empty($user->image)){
                        if (file_exists(Yii::getAlias('@frontend/web/uploads/users/') . $user->image)) {
                            unlink(Yii::getAlias('@frontend/web/uploads/users/') . $user->image);
                        }
                    }

                    $imageName = substr(sha1($user->file . time() . rand(1000, 9999)), 0, 5);
                    $user->file->saveAs(Yii::getAlias('@frontend/web/uploads/users/') . $imageName . '.' . $user->file->extension);
                    Image::thumbnail(Yii::getAlias('@frontend/web/uploads/users/') . $imageName . '.' . $user->file->extension, 160, 160)
                        ->save(Yii::getAlias('@frontend/web/uploads/users/') . $imageName . '.' . $user->file->extension, ['quality' => 100]);
                    $user->image = $imageName . '.' . $user->file->extension;
                }
                $addrArray = [];
                $numberArray = [];
                foreach(array_values($addresses) as $value){
                    $addrArray[] = $value['address'];
                }
                foreach(array_values($numbers) as $value){
                    $numberArray[] = $value['number'];
                }

                foreach ($user->address as $key=>$value){
                    if(is_array($value)){
                        foreach ($value as $k=>$v){
                            if($k != $v){
                                $a = UserAddress::findOne(['id'=>$key]);
                                $a->address = $v;
                                $a->save();
                            }
                        }
                    }else{
                        $userAddress = new UserAddress();
                        $userAddress->user_id = $id;
                        $userAddress->address = $value;
                        $userAddress->main = $key == 0 ? 1 : 0;
                        $userAddress->save();
                    }
                }
                foreach ($user->number as $key=>$value){
                    if(is_array($value)){
                        foreach ($value as $k=>$v){
                            if($k != $v){
                                $a = UserNumber::findOne(['id'=>$key]);
                                $a->number = $v;
                                $a->save(false);
                            }
                        }
                    }else{
                        $userAddress = new UserNumber();
                        $userAddress->user_id = $id;
                        $userAddress->number = $value;
                        $userAddress->save(false);
                    }
                }
                if($user->save()){
                    Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['profile-changes'],'Profile changes','hello from profile-changes.php',Yii::$app->user->identity->email,'Changed profile');
                }
                return $this->redirect('/edit-profile');
            } else {
                return $this->render('edit', ['user' => $user]);
            }
        }else{
            return new NotFoundHttpException('404');
        }
    }


    public function actionRemoveAddress(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $userAddress = UserAddress::find()->where(['user_id'=>Yii::$app->user->id,'id'=>$id])->one();
            $userAddress->delete();
            return true;
        }
        return false;
    }
    public function actionRemoveNumber(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $userAddress = UserNumber::find()->where(['user_id'=>Yii::$app->user->id,'id'=>$id])->one();
            $userAddress->delete();
            return true;
        }
        return false;
    }

    public function actionCheckPassword(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->user->id;
            $user = User::find()->where(['id'=>$id])->one();
            $oldPassword = Yii::$app->request->post('oldPasswordValue');
            if($user->validatePassword($oldPassword) == true){
                $data = true;
            }else{
                $data = false;
            }
        }
        return $data;
    }
    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::$app->language.'/site/success-signup');
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    public function actionSuccessSignup(){
        $message = 12313123;
        return $this->render('success',['message'=>$message]);
    }
    /**
     * Requests password reset.
     *
     * @return mixed
     */


    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionSendPasswordEmail()
    {
        $model = new PasswordResetRequestForm();
        $users = User::find()->where(['in','id',[6116,6117]])->all();

        $model::sendGroupEmail($users);
        VarDumper::dump("success");
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');
            if(yii::$app->user->isGuest){
                $user = $model->_user;
                $loginForm = new LoginForm();
                $loginForm->username = $user->email;
                $loginForm->password = $model->password;
                $loginForm->login();
                return $this->goHome();
            }
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionAjaxElastic(){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;

            $params = Yii::$app->request->post('params');
            $attribute = !empty($params['attribute']) ? $params['attribute'] : [];
            $manufacturer = !empty($params['manufacturer']) ? $params['manufacturer'] : [];
            $price = ['price'=>$params['price']];
            $query = [
                        'query'=>[
                            'filtered'=>[
                                'filter'=>[
                                        ['terms'=>['attr_value_id'=>$attribute]],
                                        ['terms'=>['manufacturer_id'=>$manufacturer]],
                                        [
                                            'range'=>$price
                                        ]
                                ],
                            ]
                        ]
                    ];

            $path = '/products/product/_search';
            $client = new Client();
            $response = $client->request($path, Request::GET, $query);
            $responseArray = $response->getData();
            $data = [
                'data'=>$responseArray
            ];
            return $data;
            //VarDumper::dump($responseArray,6,1);die();
        }
    }


    public function actionBulk(){
        $elasticaClient = new Client([
            'host'=>'localhost',
            'port'=>9200
        ]);

        $elasticaIndex = $elasticaClient->getIndex('tests');
        $elasticaIndex->create(
            [
                'number_of_shards' => 4,
                'number_of_replicas' => 1,
            ],
            true
        );
        $elasticaType = $elasticaIndex->getType('test');

        $mapping = new \Elastica\Type\Mapping();
        $mapping->setType($elasticaType);

//        $mapping->setProperties([
//            'id'      => ['type' => 'integer', 'include_in_all' => FALSE],
//            'user'    => [
//                'type' => 'object',
//                'properties' => [
//                    'name'      => ['type' => 'string', 'include_in_all' => TRUE],
//                    'fullName'  => ['type' => 'string', 'include_in_all' => TRUE]
//                ],
//            ],
//            'msg'     => ['type' => 'string', 'include_in_all' => TRUE],
//            'tstamp'  => ['type' => 'date', 'include_in_all' => FALSE],
//        ]);


        $mapping->setProperties([
            'id'      => ['type' => 'integer'],
            'name' =>['type'=>'string'],
            'description'=>['type'=>'string'],
            'language'=>['type'=>'string'],
            'slug'=>['type'=>'slug'],
            'on_sale'=>['type'=>'integer'],
            'manufacturer_id'=>['type'=>'integer'],
            'bonus'=>['type'=>'integer'],
            'price'=>['type'=>'float'],
            'quantity'=>['type'=>'integer'],
            'supplier_id'=>['type'=>'integer'],
            'created_at'=>['type'=>'date'],
            'status'=>['type'=>'integer'],
            'user'    => [
                'type' => 'object',
                'properties' => [
                    'name'      => ['type' => 'string', 'include_in_all' => TRUE],
                    'fullName'  => ['type' => 'string', 'include_in_all' => TRUE]
                ],
            ],
            'msg'     => ['type' => 'string', 'include_in_all' => TRUE],
            'tstamp'  => ['type' => 'date', 'include_in_all' => FALSE],
        ]);

        $mapping->send();
        $id = 1;
        $tweet = array(
            'id'      => $id,
            'user'    => array(
                'name'      => 'mewantcookie',
                'fullName'  => 'Cookie Monster'
            ),
            'msg'     => 'Me wish there were expression for cookies like there is for apples. "A cookie a day make the doctor diagnose you with diabetes" not catchy.',
            'tstamp'  => '1238081389',
            'location'=> '41.12,-71.34',
            '_boost'  => 1.0
        );
        $tweetDocument = new \Elastica\Document($id, $tweet);
        $elasticaType->addDocument($tweetDocument);
        $elasticaType->getIndex()->refresh();
        $documents = array();
        $i = 0;;
        while ($i < 3) {
            $documents[] = new \Elastica\Document(
                $id,
                [$i]
            );
        }
        $elasticaType->addDocuments($documents);
        $elasticaType->getIndex()->refresh();
    }

    public function actionMakeCall(){
        if(Yii::$app->request->isAjax){
            $model = new Calls();
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $validate =  ActiveForm::validate($model);
                if(!$model->hasErrors()){
                    $model->save(false);
                }
                return $validate;
            }
        }
    }
    public function actionSearch(){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $value = Yii::$app->request->post('value');
            $product = ProductElastic::findItem(['name'=>$value]);
            return $product;
        }
    }

    public function actionFind($_product_name,$_product_productOnly_price=null){
        $result = ProductElastic::findItem(['query'=>$_product_name,'sort'=>$_product_productOnly_price]);
        return $this->render('search',['result'=>$result]);
    }
    public function actionChangepassword()
    {
        $model = new ChangePassForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->setPassword();
            Yii::$app->session->setFlash('success', 'Password Changed.');
            Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['profile-changes'],'Profile changes','hello from profile-changes.php',Yii::$app->user->identity->email,'Pass changed');
            return $this->refresh();
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

}


