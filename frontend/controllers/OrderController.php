<?php

namespace frontend\controllers;



use frontend\models\Order;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class OrderController extends Controller
{
    public function actionMyOrders(){
        if(!Yii::$app->user->isGuest){
            $orders = Order::find()->where(['user_id'=>Yii::$app->user->id])->orderBy(['id'=>SORT_DESC])->with('orderDetails')->all();
            return $this->render('orders',['orders'=>$orders]);
        }else{
            throw new NotFoundHttpException('not found');
        }
    }
}
