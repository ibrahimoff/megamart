<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 2/26/16
 * Time: 3:31 PM
 */

namespace frontend\controllers;


use backend\models\LoginForm;
use frontend\models\AddressForm;
use frontend\models\Cart;
use frontend\models\CartProduct;
use frontend\models\Checkout;
use frontend\models\PaymentLang;
use frontend\models\SignupForm;
use frontend\models\UserAddress;
use frontend\models\UserNumber;
use frontend\models\WithoutSignup;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CheckoutController extends Controller
{
    private $products;
    private $summaryPrice;
    public function actionIndex(){
        if(Yii::$app->user->isGuest){
            $this->products = Yii::$app->session['bucket'];
            if(empty(Yii::$app->session['bucket']) && !empty(Yii::$app->session['user'])){
                unset(Yii::$app->session['user']);
                return $this->goHome();
            }
            foreach($this->products as $product){
                $this->summaryPrice += $product['price'];
            }
            $params = ['products'=>$this->products,'sumPrice'=>$this->summaryPrice];
        }else{
            $this->products = [];
            $productsArray = CartProduct::find()->with('itemLangs','item')->joinWith('cart')->filterWhere(['cart.user_id'=>Yii::$app->user->id,'status'=>'0'])->orderBy(['id'=>SORT_ASC])->all();
            if(empty($productsArray)){
                if(!empty(Yii::$app->session['user'])){
                    unset(Yii::$app->session['user']);
                    return $this->goHome();
                }else{
                    throw new NotFoundHttpException('asdasddasd');
                }
            }
            foreach($productsArray as $product){
                $this->products[$product->product_id]['name'] = $product->itemLangs->name;
                $this->products[$product->product_id]['price'] = $product->item->price;
                $this->products[$product->product_id]['new_price'] = !empty($product->item->discounts->price) ? $product->item->discounts->price : '';
                $this->products[$product->product_id]['image'] = Yii::getAlias('@web/uploads/products/').$product->itemLangs->productCoverImage->product_id.'-'.$product->itemLangs->productCoverImage->id;
                $this->products[$product->product_id]['slug'] = $product->itemLangs->slug;
                $this->products[$product->product_id]['quantity'] = $product->quantity;
                $this->products[$product->product_id]['bonus'] = $product->item->bonus;
            }
            $addresses = UserAddress::find()->where(['user_id'=>Yii::$app->user->id])->all();
            $addrArray = ArrayHelper::map($addresses,'id','address');
            $params = ['products'=>$this->products,'sumPrice'=>$this->summaryPrice,'addrArray'=>$addrArray];
        }

        return $this->render('checkout',$params);
    }
    public function actionLogin(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        if(empty(Yii::$app->session['bucket'])){
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if($model->login()){
                Checkout::saveToCard();
            }

        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionSuccessOrder($token){
        $decoded = explode(',',base64_decode($token));
        $orderID = $decoded[0];
        $message = Yii::t('frontend','success_order');
        $text = Yii::t('frontend','success_order_text');
        $content = Yii::t('frontend','success_order_content');
        return $this->render('success',['message'=>$message,'text'=>$text,'content'=>$content,'orderId'=>$orderID]);
    }
    public function actionSignup(){
        if(empty(Yii::$app->session['bucket'])){
            return $this->goHome();
        }
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    Checkout::saveToCard();
                    return $this->goBack(Yii::$app->request->referrer);
                }
            }
        }
    }
    public function actionWithoutSignup(){
        if(Yii::$app->user->isGuest){
            if(empty(Yii::$app->session['bucket'])){
                return $this->goHome();
            }
            $model = new WithoutSignup();
            if($model->load(Yii::$app->request->post())){
                $model->save();
            }
            return $this->goBack(Yii::$app->request->referrer);
        }else{
            throw new NotFoundHttpException('Not found');
        }
    }
    public function actionAddress(){
            $model = new AddressForm();
            if ($model->load(Yii::$app->request->post())) {
             //   if($model->save()){
                $model->save();
                   return $this->redirect(Url::link('checkout/index'));
//
//                }
            }
        return false;
    }

    public function actionPayment($id){
            $payment = PaymentLang::find()->where(['payment_id'=>$id])->one();
            if(!empty($payment)){
                if(Yii::$app->user->isGuest){
                    $email = Yii::$app->session['user']['email'];
                    if(empty(Yii::$app->session['bucket'])){

                        return $this->goHome();
                    }
                }else{
                    $email = Yii::$app->user->identity->email;
                    $card = Cart::find()->where(['user_id'=>Yii::$app->user->id,'status'=>'0'])->one();
                    if(empty($card)){
                        return $this->goHome();
                    }
                }
                Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['buy-product'],'buy-product','hello from buy-product.php',$email,'Purchasing');

                $hash = Checkout::saveOrder($id);
                return $this->redirect(['success-order','token'=>$hash]);
            }else{
                throw new NotFoundHttpException('Not found');
            }
    }


    protected function saveToSession($key,$value){
        $session = Yii::$app->session['user'];
        $session['checkout'][$key] = $value;
        Yii::$app->session['user'] = $session;
        return true;
    }
}