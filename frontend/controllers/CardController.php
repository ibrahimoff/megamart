<?php

namespace frontend\controllers;
use frontend\models\Cart;
use frontend\models\CartProduct;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Response;
use frontend\models\ProductLang;
class CardController extends \yii\web\Controller
{

    public function actionSaveToBucket(){
        return Cart::bucket();
    }

    public function actionChangeQuantity(){
        return Cart::quantity();
    }
    public function actionRemoveItem(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            if(!Yii::$app->user->isGuest){
                $model = CartProduct::find()
                    ->where(['cart_product.user_id'=>Yii::$app->user->id])
                    ->joinWith('cart')
                    ->andFilterWhere(['status'=>'0'])
                    ->indexBy('product_id')
                    ->all();
                if(count($model) == 1){
                    Cart::deleteAll(['id'=>$model[$id]->cart_id]);
                }
                return $model[$id]->delete();
            }else{
                $session = Yii::$app->session['bucket'];
                unset($session[$id]);
                 Yii::$app->session['bucket'] = $session;
                return true;
            }
        }
        return false;
    }
}
