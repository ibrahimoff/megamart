<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 3/17/16
 * Time: 12:18 PM
 */

namespace frontend\controllers;


use frontend\models\Product;
use frontend\models\ProductLang;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

class CompareController extends Controller
{
    public function actionIndex(){
        $products = [];
        if(!empty(Yii::$app->session['compare'])){
            $products = ProductLang::find()
                ->where(['and',['in','product_id',Yii::$app->session['compare']],['language'=>Yii::$app->language]])
                ->with(['productCoverImage','product','discount'])
                ->all();
        }
        return $this->render('index',['products'=>$products]);

    }
    public function actionAdd(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $session = Yii::$app->session['compare'];
            $session[$id] = $id;
            Yii::$app->session['compare'] = $session;
            return true;
        }
        return false;
    }
    public function actionRemove(){
        if(Yii::$app->request->isAjax){
            $id = Yii::$app->request->post('id');
            $session = Yii::$app->session['compare'];
            unset($session[$id]);
            Yii::$app->session['compare'] = $session;
            VarDumper::dump(Yii::$app->session['compare'],6,1);die();
        }
    }

}