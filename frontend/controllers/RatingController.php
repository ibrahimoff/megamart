<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/18/16
 * Time: 3:27 PM
 */

namespace frontend\controllers;


use common\models\Rating;
use frontend\models\Category;
use frontend\models\CategoryLang;
use frontend\models\Manufacturer;
use frontend\models\Product;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;


class RatingController extends Controller
{

//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['index'],
//                'rules' => [
//                    [
//                        'actions' => ['index'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//        ];
//    }

    public function actionIndex(){
        $model = Rating::find()->where(['status'=>'1'])->all();

        return $this->render('index',
            ['ratings'=>$model]);
    }

    public function actionRate($token){
        $model = Rating::find()->where(['token'=>$token])->one();
        if($model) {
            if(Yii::$app->request->get() && Yii::$app->request->isAjax){
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $rating_status = '0';
                if(isset($_GET['Rating'])){
                    $rating_point = intval(yii::$app->request->get('Rating',0)['rating']);
                    $comment = yii::$app->request->get('Rating',"")['comment'];
                } else{
                    if(isset($_GET['comment'])) {
                        $comment = yii::$app->request->get('comment');
                    } else {
                        $comment = $model->comment;
                    }
                    if(isset($_GET['rating'])) {
                        $rating_point = intval(yii::$app->request->get('rating',0));
                        $rating_status = '1';
                    }else {
                        $rating_point = $model->rating;
                    }
                }

                $model->rating = $rating_point;
                $model->comment = $comment;
                $model->status = $rating_status;
                $model->save();
                if($rating_point==0) {
                    return [
                        'message'=>'Please select your rating.',
                        'created'=>0
                    ];
                } else {
                    return [
                        'message'=>'Rated successfully.',
                        'created'=>1,
                        'approveText'=>'Your rate will be published after approved by admin.'
                    ];
                }

            }
            return $this->render('rate',
                ['model'=>$model]);
        } else {
            throw new ForbiddenHttpException;
        }

    }


    public function actionManufacturersImage(){
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand("SELECT DISTINCT(category_lang.name),category_lang.category_id FROM `category` left join `category_lang` on category.id=category_lang.category_id where nleft+1=nright and category_lang.language='ru'");
        $result = $command->queryAll();
        $count = 0;
        $without_img = 0;
        $after_img = 0;
        foreach($result as $r){
            $count++;
            //add manufacturer
            $m = new Manufacturer();
            $m->id = $r['category_id'];
            $m->name = $r['name'];
            $m->image = $r['category_id'].'.jpg';
            $m->status = '1';
            $m->deleted = 0;
            $m->save();

            $category = Category::find()->where(['id'=>$r['category_id']])->one();
            //update product which category_id is manufacturer id, set manufacturer id.
            Product::updateAll(['category_id'=>$category->parent_id,'manufacturer_id'=>$category->id],['category_id'=>$r['category_id']]);

            //delete category which removed to manufacturer
            CategoryLang::deleteAll(['category_id'=>$r['category_id']]);
            $category->delete();

            //move image from last category to manufacturer
            if(file_exists(yii::getAlias('@frontend/web/uploads/categories/'.$m->image))){
                copy(yii::getAlias('@frontend/web/uploads/categories/'.$m->image),yii::getAlias('@frontend/web/uploads/manufacturer/'.$m->image));
            } else {
                $without_img++;
                //if image don't exist find image from with same name categories(if exist)
               $_categories = CategoryLang::find()->where(['name'=>$m->name])->all();
                foreach($_categories as $cat) {
                    $_img = $cat->id.'.jpg';
                    if(file_exists(yii::getAlias('@frontend/web/uploads/categories/'.$_img))){
                        $after_img++;
                        copy(yii::getAlias('@frontend/web/uploads/categories/'.$_img),yii::getAlias('@frontend/web/uploads/manufacturer/'.$m->image));
                        break;
                    }
                }
            }

            if(!file_exists(yii::getAlias('@frontend/web/uploads/manufacturer/'.$m->image))){
                echo $m->image."<br/>";
            }
        }

        //statistics
        echo "<br/> count:".$count;
        echo "<br/> without image:".$without_img."<br>";
        echo "after image:".$after_img."<br>";
        echo "with image1:".(intval($count)-intval($without_img))."<br>";
        echo "with image2:".(intval($count)-intval($without_img)+intval($after_img))."<br>";

    }

}