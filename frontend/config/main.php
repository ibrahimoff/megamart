<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['debug'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'debug' => [
            'class' => 'yii\\debug\\Module',
            'panels' => [
                'elasticsearch' => [
                    'class' => 'yii\\elasticsearch\\DebugPanel',
                ],
            ],
        ],
    ],
    'components' => [

        'authClientCollection' => [
          'class' => 'yii\authclient\Collection',
          'clients' => [
            'google' => [
                'class' => 'yii\authclient\clients\GoogleOAuth',
                'clientId' => '537881452412-mnh2nu05hdppavfmg9qj61c27t2p0at3.apps.googleusercontent.com',
                'clientSecret' => 'uFGfm4_axz5FOWZiE8EEca_b',
                //'returnUrl'=>'test'
            ],
            'facebook' => [
              'class' => 'yii\authclient\clients\Facebook',
              'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
              'clientId' => '422815147908429',
              'clientSecret' => '2831a9c70aa7ff399940cc99c2898ad9',
                
            ],
          ],
        ],
//        'assetManager' => [
//            'bundles' => [
//                'yii\bootstrap\BootstrapAsset' => [
//                    'css' => [],
//
//                ],
//            ],
//        ],
        'request'=>[
            'class' => 'common\components\Request',
            'web'=> '/frontend/web'
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '/'=>'site/index',
                'about'=>'site/about',
                'contact'=>'site/contact',
                'checklist'=>'site/checklist',
                'discounts'=>'site/discounts',
                'login'=>'site/login',
                'logout'=>'site/logout',
                'signup'=>'site/signup',
                'page/<slug>'=>'site/page',
                'by-category/<categorySlug:([\w\d\-\_]+)\/?>'=>'product/filter-product',
                '_p/<productSlug>'=>'product/view',
                'edit-profile'=>'site/edit-profile',
                '<controller:\w+>/<action:[\w\-]+>'=>'<controller>/<action>',

            ]
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
