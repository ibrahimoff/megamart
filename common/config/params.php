<?php
return [
    'url'=>'http://meqamart.az/',
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'mailTemplates'=>[
        'signup'=>'site/signup',
        'profile-changes'=>'site/profile-changes',
        'buy-product'=>'site/buy-product',
    ],
    'languages'=>[
//        'en'=>[
//            'code'=>'en',
//            'lang'=>'English'
//        ],
        'ru'=>[
            'code'=>'ru',
            'lang'=>'Русский'
        ],
        'az'=>[
            'code'=>'az',
            'lang'=>'Azərbaycan'
        ],
    ],
    'langs'=>['ru','az']
];
