<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->params['url'].'rating/rate?token='.$rating->token;
?>
<div class="password-reset">
    <p>Dear, <?= Html::encode($rating->user->fullname) ?>,</p>

    <p>Your order has been delivered. Please rate and write review about your order
        by clicking following link or copy and paste it your browser:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
