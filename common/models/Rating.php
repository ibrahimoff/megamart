<?php

namespace common\models;

use backend\models\User;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;


/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $
 *
 * @property User $user
 */
class Rating extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'rating';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','rating'], 'integer'],
            [['user_id'], 'required'],
            [['status','comment','token'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'rating' => 'Rating',
            'comment' => 'Comment',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getMine()
    {
        if(!yii::$app->user->isGuest && (yii::$app->user->id==$this->user_id)){
            return true;
        } return false;
    }

}
