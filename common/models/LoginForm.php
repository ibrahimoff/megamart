<?php
namespace common\models;

use frontend\models\Cart;
use frontend\models\CartProduct;
use frontend\models\Checkout;
use frontend\models\Pinbox;
use Yii;
use yii\base\Model;
use common\models\User;
use yii\helpers\VarDumper;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
    private $_user = false;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('backend','username'),
            'email' => Yii::t('backend','email'),
            'password' => Yii::t('backend','password')
        ];
    }
    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
                $this->_user = User::find()
                ->where(['email'=>$this->username,'status'=>10])
                ->one();
        }
        return $this->_user;
    }
    
    
    public function socialLogin($params){
       
        $imageName = (substr(sha1(date('Y-m-d H:i:s').time().rand(1000, 9999)),0,9)).'.jpg';
        if(Yii::$app->request->get('authclient') == 'facebook'){
            $user = User::find()->where(['email'=>$params['email']])->one();
            if(!empty($user)){
                $_SESSION['user']['pins'] = [];
                $pins = Pinbox::find()->where(['user_id'=>Yii::$app->user->id])->select('product_id')->asArray()->all();
                foreach($pins as $pin){
                    $_SESSION['user']['pins'][] = $pin['product_id'];
                }
                if(Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0)){
                        Checkout::saveToCard();
                }
            }else{
                $url = 'http://graph.facebook.com/'.$params['id'].'/picture?width=160&height=160';
                $img = Yii::getAlias('@frontend/web/uploads/users/'.$imageName);
                file_put_contents($img, file_get_contents($url));
                $model = new User();
                $model->username = $params['name'];
                $model->firstname = $params['first_name'];
                $model->lastname = $params['last_name'];
                $model->gender = $params['gender'] == 'male' ? 1:2;
                $model->fb_uid =  $params['id'];
                $model->email =  $params['email'];
                $model->image =  $imageName;
                $model->status = 10;
                $model->role = 1;
                if($model->save(false)){
                    Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['signup'],'Sign up','hello from signup.php',$params['email'],'Signed up from facebook');
                    if(Yii::$app->user->login($model, $this->rememberMe ? 3600 * 24 * 30 : 0)){
                        Checkout::saveToCard();
                    }
                }

            }
        }
        if(Yii::$app->request->get('authclient') == 'google'){
            $user = User::find()->where(['email'=>$params['emails'][0]['value']])->one();
            if(!empty($user)){
                $_SESSION['user']['pins'] = [];
                $pins = Pinbox::find()->where(['user_id'=>Yii::$app->user->id])->select('product_id')->asArray()->all();
                foreach($pins as $pin){
                    $_SESSION['user']['pins'][] = $pin['product_id'];
                }
                if(Yii::$app->user->login($user, $this->rememberMe ? 3600 * 24 * 30 : 0)){
                    Checkout::saveToCard();
                }
            }else{
                $link = substr($params['image']['url'], 0, -2);
                $url = $link.'160';
                $img = Yii::getAlias('@frontend/web/uploads/users/'.$imageName);
                file_put_contents($img, file_get_contents($url));
                $model = new User();
                $model->username = $params['displayName'];
                $model->lastname = $params['name']['familyName'];
                $model->firstname = $params['name']['givenName'];
                $model->g_uid =  $params['id'];
                $model->email =  $params['emails'][0]['value'];
                $model->image =  $imageName;
                $model->status = 10;
                $model->role = 1;
                if($model->save(false)){
                    Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['signup'],'Sign up','hello from signup.php',$this->email,'Signed up from google');
                    if(Yii::$app->user->login($model, $this->rememberMe ? 3600 * 24 * 30 : 0)){
                        Checkout::saveToCard();
                    }
                }
            }
        }
    }
}
