<?php

$models = $result->all();

?>

<?php foreach($models as $model):?>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->

    <ol class="carousel-indicators">

        <?php for($i=0;$i<count($model->sliderImages);$i++){?>
            <li data-target="#carousel-example-generic" data-slide-to="<?=$i?>" class=<?=$i == 0?"active":"" ?>></li>
        <?php }?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <?php foreach($model->sliderImages as $key=>$item):?>
        <div class="item <?=$key==0 ?'active':''?>">
            <img src="/frontend/web/uploads/slider/<?=$item->slider_id;?>/<?=$item->image;?>" alt="..." />
            <div class="carousel-caption">

            </div>
        </div>
        <?php endforeach;?>

    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<?php endforeach;?>