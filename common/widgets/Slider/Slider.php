<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/12/16
 * Time: 5:44 PM
 */

namespace common\widgets\Slider;

use backend\models\Sliders;
use yii\base\Widget;

class Slider extends Widget{
    public $position;
    public $result;
    public $query;
    public function init(){
        $this->query = Sliders::find();
        switch($this->position){
            case 1:
                $this->query->where(['position'=>1]);
                break;
            case 2:
                $this->query->where(['position'=>2]);
                break;
            case 3:
                $this->query->where(['position'=>3]);
                break;
            case 4:
                $this->query->where(['position'=>4]);
                break;
        }
        $this->query->andWhere(['status'=>'1'])->with('sliderImages');


    }
    public function run(){
        return $this->render('slider',['result'=>$this->query]);
    }
}