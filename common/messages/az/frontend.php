<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/11/16
 * Time: 7:19 PM
 */
return [
    'all_categories'=>'Bütün kateqoriyalar',
    'delivery'=>'Çatdırılma',
    'add_pin'=>'',
    'availability'=>'Available',
    'warranty'=>'Zəmanət və servis',
    'specifications'=>'Xüsusiyyətlər',
    'success_order'=>'Sifariş eləməyinizə görə taşəkkür edirik',
    'sign_up'=>'',
    'log_in'=>'',
    'search_placeholder'=>'',
    'ask_call'=>'',
    'buy_button'=>'',
    'compare_button'=>'',
    'remove'=>'',
    'sale_out'=>'',
];