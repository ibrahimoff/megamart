<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/11/16
 * Time: 7:19 PM
 */
return [
    'all_categories'=>'Все категории',
    'delivery'=>'Доставка',
    'add_pin'=>'',
    'availability'=>'Есть в наличии',
    'warranty'=>'Гарантия и сервис',
    'specifications'=>'Характеристики',
    'success_order'=>'Спасибо за покупку',
    'success_order_text'=>'Для подтверждения Вашего заказа наш менеджер скоро свяжется с Вами.',
    'success_order_content'=>'Ваш заказ № %s передан на исполнение.',
    'sign_up'=>'Зарегистрироваться',
    'log_in'=>'Вход',
    'search_placeholder'=>'Поиск по товарам',
    'ask_call'=>'Заказать звонок',
    'buy_button'=>'Купить',
    'compare_button'=>'Сравнить',
    'remove_button'=>'Удалить',
    'add_button'=>'Добавить',
    'sale_out'=>'РАСПРОДАЖА',
    'latest'=>'НОВИНКИ',
    'bestsellers'=>'БЕСТСЕЛЛЕРЫ',
    'megamart_info'=>'Megamart - онлайн мегамаркет',
    'bucket_total_count'=>'В корзине <span class="number" id="item_counter">%s</span> товаров',
    'bucket_total_price'=>'<div class="key">Итого:</div><div class="price"><span class="number" id="sum_price">%s</span><span class="currency"><i class="mm-icon-azn"></i></span></div>',
    'make_checkout'=>'Оформить заказ',
    'continue_shopping'=>'Продолжить покупки',
    'news'=>'НОВОСТИ',
    'get_bonus'=>'<span style="color: #000;">%s</span> бонусов',
    'accessory'=>'Aксессуары',
    'same_products'=>'ПОХОЖИЕ ТОВАРЫ',
    'articul'=>'Артикул: %s',
    'items'=>'ТОВАР',
    'price'=>'ЦЕНА',
    'quantity'=>'КОЛИЧЕСТВО',
    'total_price'=>'СУММА',
    'total_checkout'=>'ИТОГО',
    'checkout'=>'Оформление заказа',
    'delivery_address'=>'Адрес доставки',
    'payment'=>'Оплата',
    'social_login'=>'',
    'select_button'=>'Выбрать',
    'client_code'=>'Код клиента'
];