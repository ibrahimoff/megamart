<?php
/**
 * Created by PhpStorm.
 * User: Kate
 * Date: 09.02.15
 * Time: 12:43
 */
namespace  common\components;
use yii\helpers\Html;
use yii\helpers\VarDumper;
use yii\web\UrlManager;
use Yii;

class ZUrlManager  extends UrlManager
{
    public function createUrl($params=array())
    {
//        if(!isset($params['language'])){
//            $s = '';
//            return $s.$this->fixPathSlashes(parent::createUrl($params));
//        }
        $lang= isset($params['language'])?$params['language']:Yii::$app->language;

        if(isset($params['language'])){
            unset($params['language']);
        }

        if(($lang == Yii::$app->sourceLanguage)){
            $s = '';
//            unset($params['language']);
            return $s.$this->fixPathSlashes(parent::createUrl($params));
        }

        if($lang != Yii::$app->sourceLanguage){
            $s = '/'.$lang;
//            unset($params['language']);
            return $s.parent::createUrl($params);
        }
    }

    protected  function fixPathSlashes($url)
    {
        return preg_replace('|\%2F|i', '/', $url);
    }

    public static function langSwitcher(){
        $langs = Yii::$app->params['languages'];
        $link = '<ul class="navbar-nav navbar-right nav">';

        foreach($langs as $key=>$value){
            if(Yii::$app->language == $key){
                $styleDisplay = "display:none";
            }else{
                $styleDisplay = "display:block";
            }
            $link .= '<li style="'.$styleDisplay.'" ><a href="/'.$key.Yii::$app->request->url.'">'.$value['lang'].'</a></li>';
        }
        $link .= '</ul>';
        return $link;
    }
    public static function languageUrl(){
        $languages = Yii::$app->params['languages'];
        $ulElement = Html::beginTag('ul');
        foreach($languages as $language){
            $ulElement .= Html::beginTag('li');
            $ulElement .= Html::beginTag('a',['href'=>'/'.$language.Yii::$app->request->url]);
            $ulElement .= $language.Yii::$app->request->url;
            $ulElement .= Html::endTag('a');
            $ulElement .= Html::endTag('li');

        }
        $ulElement .= Html::endTag('ul');
        return $ulElement;
    }
}