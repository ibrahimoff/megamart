<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 12/30/15
 * Time: 11:37 AM
 */
namespace common\components ;
class ActiveField extends \yii\widgets\ActiveField

{

    private $_filed_id = '';

    function adjustLabelFor ( $options)

    {

        if ( isset( $options [ 'id'])) {

            $this-> _filed_id = $options [ 'id'] ;

        }

        parent:: adjustLabelFor( $options );

    }

    function getClientOptions (){

        $data = parent:: getClientOptions () ;

        if( $this-> _filed_id ){

            $data[ 'id'] = $this -> _filed_id;

            $data[ 'input'] = isset ( $this-> selectors [ 'input']) ? $this -> selectors[ 'input' ] : "#$this -> _filed_id ";

        }

        return $data ;

    }

}