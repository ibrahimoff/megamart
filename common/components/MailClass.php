<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 4/13/16
 * Time: 3:56 PM
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class MailClass extends Component
{
    public function send($template,$model,$title,$email,$subject)
    {
        Yii::$app->mailer->compose(['html' => $template], ['model' => $model, 'title' => $title])
            ->setTo($email)
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setSubject($subject)
            ->send();
        
    }

}