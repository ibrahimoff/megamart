<?php

namespace backend\controllers;

use backend\models\Product;
use backend\models\ProductLang;
use Elastica\Client;
use Yii;
use backend\models\Discount;
use backend\models\DiscountSearch;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DiscountController implements the CRUD actions for Discount model.
 */
class DiscountController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin','content_manager','moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Discount models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DiscountSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Discount model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Discount model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Discount();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            $model->created_at = Yii::$app->formatter->asDatetime(date('Y-m-d H:i:s'));
            if($model->save()){
                $product = Product::find()->where(['id'=>$model->product_id])->one();
                $product->on_sale = 1;
                $product->save();
                $this->toElastic($model->product_id);
            }

            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetItems($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '','price'=>'']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('product_id as id, name AS text')
                ->from('product_lang')->where(['and',['like', 'name', $q],['language'=>Yii::$app->language]]);
            $query->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ProductLang::find($id)->name,'price'=>Product::find($id)->price];
        }
        return $out;
    }

    public function actionGetOldPrice(){
        if(Yii::$app->request->isAjax){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $id = Yii::$app->request->post('id');
            $product = Product::findOne($id);
            $data = [
                'oldPrice'=>$product->price
            ];
        }
        return $data;
    }

    /**
     * Updates an existing Discount model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $productLang = ProductLang::find()->where(['product_id'=>$model->product_id,'language'=>Yii::$app->language])->one();
        $values = [$productLang->product_id=>$productLang->name];
        if ($model->load(Yii::$app->request->post())) {
            $model->updated_at = Yii::$app->formatter->asDatetime(date('Y-m-d H:i:s'));
            $model->save();
            $this->toElastic($model->product_id);
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
                'values' => $values
            ]);
        }
    }

    /**
     * Deletes an existing Discount model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $products = ProductLang::find()->where(['product_id'=>$id])->with(['productOnly','discount','productCoverImage'])->asArray()->all();
        $client = new Client();
        foreach($products as $key=>$value){
            $client->request('/products/product/'.$value['id'], $method = \Elastica\Request::DELETE, $data = [],  $query = array());
        }
        $model = $this->findModel($id);
        $product = Product::find()->where(['id'=>$model->product_id])->one();
        $product->on_sale = 0;
        $product->save();
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Discount model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Discount the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function toElastic($id){
        $products = ProductLang::find()->where(['product_id'=>$id])->with(['productOnly','discount','productCoverImage'])->asArray()->all();
        $client = new Client();
        foreach($products as $key=>$value){
            $client->request('/products/product/'.$value['id'], $method = \Elastica\Request::PUT, $data = ['_product'=>$value],  $query = array());
        }
    }
}
