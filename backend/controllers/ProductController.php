<?php

namespace backend\controllers;

use backend\models\Accessory;
use backend\models\Category;
use backend\models\ProductForm;
use backend\models\ProductImage;
use backend\models\ProductLang;
use backend\models\ProductToCategory;
use common\components\MultilingualBehavior;
use common\components\Request;
use Elastica\Client;
use frontend\models\CategoryLang;
use Yii;
use backend\models\Product;
use backend\models\ProductSearch;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin','moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGetItems($productId = null, $q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('product_id as id, name AS text')
                ->from('product_lang')->where(['and',['like', 'name', $q],['language'=>Yii::$app->language]]);
            if($productId != null){
                $query->andWhere(['!=','product_id',$productId]);
            }
            $query->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => ProductLang::find($id)->name];
        }
        return $out;
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model= new ProductForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->saveProduct();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = Product::getModelAttributes($id);
        $accessories = Accessory::find()->where(['product_id'=>$id])->with('acc')->all();
        $productCategories = ProductToCategory::find()->where(['product_id'=>$id])->indexBy('category_id')->asArray()->all();
        if ($model->load(Yii::$app->request->post())) {
            $model->updateProduct($id);
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
                'productCategories'=>$productCategories,
                'accessories'=>$accessories
            ]);
        }
    }
    public function actionGetImages($id){
        if(Yii::$app->request->isAjax){
            $model = Product::getAllImages($id);
            return $model;
        }
    }

    public function actionChangeCategory(){
        if(Yii::$app->request->isAjax){
            $categories = Category::find()->where(['parent_id'=>1])->joinWith('categoryTranslate')->andFilterWhere(['is not','name',null])->all();
            $categoriesArray = ArrayHelper::map($categories,'id',function($q){
                return $q->categoryTranslate->name;
            });
            $html = Html::dropDownList('ProductToCategory[]',null,$categoriesArray,['prompt'=>'Select category','class'=>'form-control categories-select']);
            return $html;
        }
        return false;
    }

    public function actionCategories($id){
        if(Yii::$app->request->isAjax){
            $model = Product::categoryList($id);
            return $model;
        }
    }
    public function actionGetChild(){
        return Product::getChild();
    }
    public function actionUpdateMain($id){
        if(Yii::$app->request->isAjax){
            $images = ProductImage::find()->where(['id'=>$id])->asArray()->one();
            $product = Product::find()->where(['id'=>$images['product_id']])->one();
            $product->image = $images['image'];
            $product->save();
            die('1');
        }
    }
    public function actionRemoveImage($id){
        if(Yii::$app->request->isAjax){
            $productImage = ProductImage::findOne($id);
            $productImage->delete();
            die('1');
        }
    }

    public function actionChangePrice($id,$price){
        if(Yii::$app->request->isAjax){
            VarDumper::dump($id,10,1);
            return true;
        }
        return false;
    }
    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $products = ProductLang::find()->where(['product_id'=>$id])->with(['productOnly','discount','productCoverImage'])->asArray()->all();
        $client = new Client();
        foreach($products as $key=>$value){
            $client->request('/products/product/'.$value['id'], $method = \Elastica\Request::DELETE, $data = [],  $query =[]);
        }
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    public function actionRemoveAccessory(){
        if(Yii::$app->request->isAjax){
            $productId = Yii::$app->request->post('productId');
            $accId = Yii::$app->request->post('accId');
            $model = Accessory::find()->where(['product_id'=>$productId,'acc_id'=>$accId])->one();
            if(!empty($model)){
                $model->delete();
            }
        }
        die('1');
    }
    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
