<?php

namespace backend\controllers;

use backend\models\User;
use common\models\Rating;
use Yii;
use backend\models\Order;
use backend\models\OrderSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin','content_manager','moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->status = '1';
        $model->save(false);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionCreate()
//    {
//        $model = new Order();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
            $this->findModel($id)->delete();
            return $this->redirect(['index']);
    }

    public function actionConfirm($id){
        $model = Order::find()->where(['id'=>$id])->with('orderDetails')->one();
        if($model->status == '1'){
            $model->status = '2';
            $model->save(false);
            $model = Order::find()->where(['id'=>$id])->with('orderDetails')->one();
            $user = User::find()->select(['id','points'])->where(['id'=>$model->user_id])->one();
            $totalBonus = '';
            foreach($model->orderDetails as $item){
                $totalBonus += $item->orderItem->bonus*$item->quantity;

            }
            $user->points =$user->points + $totalBonus;
            $model->total_points = $totalBonus;
            $model->save(false);
            $user->save(false);

            $existRating = Rating::find()->where(['user_id'=>$user->id])->one();
            $ratingUser = User::find()->where(['id'=>$user->id,'status'=>10])->one();
            if(!$existRating || !$ratingUser){
                $rating = new Rating();
                $rating->user_id = $user->id;
                $rating->token = Yii::$app->security->generateRandomString(32);
                $rating->status = '0';
                $rating->save();
                Order::sendEmail($rating);
            }

            return $this->redirect('index');
        }else{
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    public function actionDecline($id){
        $model = $this->findModel($id);
        if($model->status == '1'){
            $model->status = '3';
            $model->save(false);
            Yii::$app->mailclass->send(Yii::$app->params['mailTemplates']['profile-changes'],'Declined','hello from declined.php',$model->email,'Declined');

            return $this->redirect('index');
        }else{
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    public function actionComment(){

        $id = $_POST['hidden_id'];
        $text = $_POST['Order']['comment'];
            $model = Order::find()->where(['id'=>$id])->with('orderDetails')->one();
            $user = User::find()->select(['id','points'])->where(['id'=>$model->user_id])->one();

            if($model->status == '2'){
                $totalBonus = '';
                foreach($model->orderDetails as $item){
                    $totalBonus += $item->orderItem->bonus*$item->quantity;

                }
                $user->points += $totalBonus;
                $user->save(false);
            }
        if($model->status == '1'){
            $model->comment = $text;
            $model->save(false);
            return $this->redirect('index');
        }else{
            return $this->redirect(Yii::$app->request->referrer);
        }

    }
    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
