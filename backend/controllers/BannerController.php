<?php

namespace backend\controllers;

use backend\models\BannerLang;
use common\components\MultilingualBehavior;
use Yii;
use backend\models\Banner;
use backend\models\BannerSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BannerController implements the CRUD actions for Banner model.
 */
class BannerController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin','content_manager','moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banner models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Banner model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Banner model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banner();
        $model->attachBehavior('ml',[
            'class'=>MultilingualBehavior::className(),
            'relationClassName'=>BannerLang::className(),
            'langForeignKey'=>'banner_id',
            'languageField'=>'language',
            'relationName'=>'bannerLangs',
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model,'file');
            if(!empty($model->file)){
                $imageName = substr(sha1($model->image.time().rand(1000, 9999)),0,5);
                if(!file_exists(Yii::getAlias('@frontend/web/uploads/banners/'))){
                    mkdir (Yii::getAlias('@frontend/web/uploads/banners/'));
                }
                $model->file->saveAs(Yii::getAlias('@frontend/web/uploads/banners/').$imageName.'.'.$model->file->extension);
                $model->image = $imageName.'.'.$model->file->extension;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Banner model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->attachBehavior('ml',[
            'class'=>MultilingualBehavior::className(),
            'relationClassName'=>BannerLang::className(),
            'langForeignKey'=>'banner_id',
            'languageField'=>'language',
            'relationName'=>'bannerLangs',
        ]);
        if ($model->load(Yii::$app->request->post())) {
            $model->file = UploadedFile::getInstance($model,'file');
            if(!empty($model->file)){
                $imageName = substr(sha1($model->image.time().rand(1000, 9999)),0,5);
                if(!file_exists(Yii::getAlias('@frontend/web/uploads/banners/'))){
                    mkdir (Yii::getAlias('@frontend/web/uploads/banners/'));
                }
                $model->file->saveAs(Yii::getAlias('@frontend/web/uploads/banners/').$imageName.'.'.$model->file->extension);
                $model->image = $imageName.'.'.$model->file->extension;
            }
            $model->save();
            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Banner model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Banner model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Banner the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banner::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
