<?php

namespace backend\controllers;

use backend\models\CatAttr;
use backend\models\CatAttrLang;
use backend\models\CatAttrValue;
use backend\models\CatAttrValueLang;
use backend\models\CategoryLang;
use backend\models\ProductForm;
use common\components\MultilingualBehavior;
use Yii;
use backend\models\Category;
use backend\models\CategorySearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'allow' => true,
                        'roles' => ['admin','content_manager','moder'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Category();

        $model->attachBehavior('ml',[
            'class'=>MultilingualBehavior::className(),
            'relationClassName'=>CategoryLang::className(),
            'langForeignKey'=>'category_id',
            'languageField'=>'language',
            'relationName'=>'categoryLangs',
        ]);



        if ($model->load(Yii::$app->request->post())) {
            $parentId = !empty(Yii::$app->request->post('Category')['parent_id']) ? Yii::$app->request->post('Category')['parent_id']: 1;
            $parent = Category::find()->where(['id'=>$parentId])->one();
            $model->level = $parent->level + 1;
            $model->parent_id = $parentId;
            $model->appendTo($parent);
            $model->created_at = date('Y-m-d H:i:s');
            if($model->save()){
                if(!empty($_POST['CatAttrLang']) && !empty($_POST['CatAttrValueLang'])){
                    $catAttrArray = $_POST['CatAttrLang'];
                    $catAttrValueArray = $_POST['CatAttrValueLang'];
                    foreach($catAttrArray as $catAttrIndex){
                        $catAttrModel = new CatAttr();
                        $catAttrModel->cat_id = $model->id;
                        $catAttrModel->save();
                        foreach($catAttrIndex as $key=>$value){
                            $catAttrLangModel = new CatAttrLang();
                            $catAttrLangModel->cat_id = $model->id;
                            $catAttrLangModel->cat_attr_id = $catAttrModel->id;
                            $catAttrLangModel->language = $key;
                            $catAttrLangModel->name = $value;
                            $catAttrLangModel->save();
                        }
                    }
                    foreach($catAttrValueArray as $catAttrValueIndex){
                        $catAttrValueModel = new CatAttrValue();
                        $catAttrValueModel->cat_id = $model->id;
                        $catAttrValueModel->cat_attr_id = $catAttrModel->id;
                        $catAttrValueModel->save();
                        foreach($catAttrValueIndex as $key=>$value){
                            $catAttrValueLangModel = new CatAttrValueLang();
                            $catAttrValueLangModel->cat_id = $model->id;
                            $catAttrValueLangModel->cat_attr_id = $catAttrModel->id;
                            $catAttrValueLangModel->cat_attr_value_id = $catAttrValueModel->id;
                            $catAttrValueLangModel->language = $key;
                            $catAttrValueLangModel->name = $value;
                            $catAttrValueLangModel->save();
                        }
                    }
                }

            }
            return $this->redirect('index');
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->attachBehavior('ml',[
            'class'=>MultilingualBehavior::className(),
            'relationClassName'=>CategoryLang::className(),
            'langForeignKey'=>'category_id',
            'languageField'=>'language',
            'relationName'=>'categoryLangs',
        ]);
        $oldParentId = $model->parent_id;
        if ($model->load(Yii::$app->request->post())) {

            if($oldParentId != Yii::$app->request->post('Category')['parent_id']){
                $parentModel = $this->findModel(Yii::$app->request->post('Category')['parent_id']);
                $parentModel->nright +=2;
                $parentModel->save();
                $lastChild = Category::find()->where(['parent_id'=>$parentModel->id])->orderBy(['nleft'=>SORT_DESC])->one();
                $model->nleft = $lastChild->nleft+2;
                $model->nright = $lastChild->nright+2;
            }
            $model->updated_at = date('Y-m-d H:i:s');
            $model->save();
            if(!empty($_POST['CatAttrLang']['update'])){
                foreach($_POST['CatAttrLang']['update'] as $key=>$values){
                    $catAttrModels = CatAttrLang::find()->where(['cat_attr_id'=>$key])->all();
                    foreach($catAttrModels as $catAttrModel){
                        foreach($values as $key=>$value){
                            if($value != ''){
                                if($catAttrModel->language == $key){
                                    $catAttrModel->name =$value;
                                    $catAttrModel->save();
                                }
                            }

                        }
                    }
                }
            }
            if(!empty($_POST['CatAttrLang']['newKeys'])){
                foreach($_POST['CatAttrLang']['newKeys'] as $newKeys){
                    $newCatAttr = new CatAttr();
                    $newCatAttr->cat_id = $id;
                    if($newCatAttr->save()){
                        foreach($newKeys as $key=>$value){
                            if($value != ''){
                                $newCatAttrLang = new CatAttrLang();
                                $newCatAttrLang->cat_id = $id;
                                $newCatAttrLang->cat_attr_id = $newCatAttr->id;
                                $newCatAttrLang->name = $value;
                                $newCatAttrLang->language = $key;
                                $newCatAttrLang->save();
                            }

                        }
                        foreach($_POST['CatAttrValueLang']['addedValues'] as $valuesArray){
                            $newCatAttrValueAdded = new CatAttrValue();
                            $newCatAttrValueAdded->cat_id = $id;
                            $newCatAttrValueAdded->cat_attr_id = $newCatAttr->id;
                            if($newCatAttrValueAdded->save()){
                                foreach($valuesArray as $key=>$value){
                                    if($value != ''){
                                        $newCatAttrValueAddedLangs = new CatAttrValueLang();
                                        $newCatAttrValueAddedLangs->cat_id = $id;
                                        $newCatAttrValueAddedLangs->cat_attr_id = $newCatAttr->id;
                                        $newCatAttrValueAddedLangs->cat_attr_value_id = $newCatAttrValueAdded->id;
                                        $newCatAttrValueAddedLangs->name = $value;
                                        $newCatAttrValueAddedLangs->language = $key;
                                        $newCatAttrValueAddedLangs->save();
                                    }

                                }
                            }
                        }
                    }
                }
            }
            if(!empty($_POST['CatAttrValueLang']['update'])){
                foreach($_POST['CatAttrValueLang']['update'] as $key=>$values){
                    $catAttrValueLangModels = CatAttrValueLang::find()->where(['cat_attr_value_id'=>$key])->all();
                    foreach($catAttrValueLangModels as $catAttrValueLangModel){
                        foreach($values as $key=>$value){
                            if($value != ''){
                                if($catAttrValueLangModel->language == $key){
                                    $catAttrValueLangModel->name = $value;
                                    $catAttrValueLangModel->save();
                                }
                            }

                        }
                    }
                }
            }


            if(!empty($_POST['CatAttrValueLang']['newValues'])){
                foreach($_POST['CatAttrValueLang']['newValues'] as $key=>$values){
                    $newValuesCatAttrValues = new CatAttrValue();
                    $newValuesCatAttrValues->cat_id = $id;
                    $newValuesCatAttrValues->cat_attr_id = $key;
                    $newValuesCatAttrValues->save();

                    foreach($values as $k=>$value){
                        foreach($value as $lang=>$v){
                            if($v != ''){
                                $newValuesCatAttrValuesLangs = new CatAttrValueLang();
                                $newValuesCatAttrValuesLangs->cat_id = $id;
                                $newValuesCatAttrValuesLangs->cat_attr_id = $key;
                                $newValuesCatAttrValuesLangs->cat_attr_value_id = $newValuesCatAttrValues->id;
                                $newValuesCatAttrValuesLangs->name = $v;
                                $newValuesCatAttrValuesLangs->language = $lang;
                                $newValuesCatAttrValuesLangs->save();
                            }
                        }

                    }
                }
            }

            return $this->redirect('index');
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->deleteWithChildren();
//
//        return $this->redirect(['index']);
//    }

    public function actionGetData($id){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $catAttrId = CatAttr::find()->where(['cat_id'=>$id])->all();
            $catAttrLang = CatAttrLang::find()->where(['cat_id'=>$id])->indexBy('name')->all();
            $catAttrValue = CatAttrValue::find()->where(['cat_id'=>$id])->all();
            $catAttrValueLangs = CatAttrValueLang::find()->where(['cat_id'=>$id])->all();
            $array = [];
            $i = 0;
            foreach($catAttrId as $attr){
                foreach($catAttrValueLangs as $catAttrValueLang){
                    if($attr['id'] == $catAttrValueLang['cat_attr_id']){
                        $array[$i]['items'][] = $catAttrValueLang;
                    }
                }
                foreach($catAttrLang as $catAttr=>$val){
                    if($val['cat_attr_id'] == $attr['id']){
                        $array[$i]['id'] = $attr['id'];
                        $array[$i][$val['language']] = $catAttr;
                    }
                }
                $i++;
            }
            $data = [
                'catAttrId'=>$catAttrId,
                'catAttrLang'=>$catAttrLang,
                'catAttrValue'=>$catAttrValue,
                'catAttrValueLangs'=>$catAttrValueLangs,
                'attrValuesArray'=>$array
            ];
            return $data;
        }
    }

    public function actionRemoveElements($element,$id){
        if(Yii::$app->request->isAjax){
            if($element == '1'){
                CatAttr::deleteAll(['id'=>$id]);

            }elseif($element == '2'){
                CatAttrValue::deleteAll(['id'=>$id]);
            }
        }
       return $element;
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
