/**
 * Created by fuad on 12/18/15.
 * @TODO Add name to defaultLangValues
 */
var childrenArray = [];
childrenArray[0] = [];
var increment = 0;
var models;
var catAttrId;
var rows = [];
var catAttrValueLangRows = [];
var newModel = document.getElementById('example').getAttribute('new-model');
var modelId = document.getElementById('example').getAttribute('model-id');
var displayNone = {"display": "none"};
var displayBlock = {"display": "block"};
var removeBtn = "removeBtnPosition btn btn-danger";
var catAttrInput = "cat_attr_key highlight form-control";
var catAttrValueInput = "cat_attr_value highlight form-control";
var countCatAttr = 0;
var countCatAttrValue = 0;
var valueFields = [];
var valueFieldsArray = [];
var locale = [{
    az:{
        catAttr:'Kateqoriya atributu',
        catAttrValue:'Kateqoriya atributun dəyəri',
        addValue:'Dəyəri əlavə et',
        addAttr:'Atributu əlavə et',
        remove:'Sil'
    },
    ru:{
        catAttr:'Атрибут категории',
        catAttrValue:'Значение аттрибута категории',
        addValue:'Добавить значение',
        addAttr:'Добавить аттрибут',
        remove:'Удалить'
    }
}];
document.getElementById('submitBtn').onclick = function (e) {
    var highlight = $('.highlight');
    if (highlight.length > 0) {
        var formValidated = true;
        highlight.each(function (item, key) {
            if (key.value.trim() == '') {
                formValidated = false;
                e.preventDefault();
                key.style.borderColor = 'red';
            }
        });
        if (formValidated == true) {
            document.getElementById("category-form").submit();
        }
    } else {
        document.getElementById("category-form").submit();
    }
};
var CatAttrKeyField = React.createClass({
    getInitialState: function () {
        return ({
            fields: [],
            valueFields: [],
            activeLanguage: null,
            increment: 0
        });
    },
    handleClick: function (e) {
        e.preventDefault();
        var fields = this.state.fields;
        var newFields = fields.concat([this.state.fields.length + 1]);
        this.setState({fields: newFields}, function () {
            childrenArray[this.state.fields.length - 1] = [];
        });
        ++increment;
        $('#submitBtn').css('display','block');
        $('#changeButton').hide()
    },
    handleRemove: function (e) {
        var parentId = e.target.dataset.remove;
        delete this.state.valueFields[parentId];
        this.state.valueFields.splice(parentId, 1);
        delete this.state.fields[parentId];
        this.state.fields.splice(parentId, 1);

        this.setState({fields: this.state.fields});
        this.setState({valueFields: this.state.valueFields});
    },
    addValue: function (e) {
        var parentId = e.target.parentElement.id;
        if (childrenArray[parentId] == undefined) {
            childrenArray[parentId] = []
        }
        childrenArray[parentId].push(childrenArray[parentId].length + 1);
        this.state.valueFields = childrenArray;
        this.setState({valueFields: this.state.valueFields});
        this.setState({increment: ++this.state.increment});
        ++increment;
    },

    removeChild: function (e) {
        var parentId = e.target.parentElement.dataset.parent;
        var childId = e.target.parentElement.dataset.child;
        delete this.state.valueFields[parentId][childId];
        this.setState({valueFields: this.state.valueFields})
    },
    render: function () {
        var self = this;
        var fields = this.state.fields.map(function (value, key) {

            var parentId = key;
            var removeButton = key != 0 || newModel == 'false' ? <button onClick={self.handleRemove} data-remove={key} type="button" className={removeBtn}>{locale[0][self.props.activeLang]['remove']}</button> : null;
            var addValueButton = <button onClick={self.addValue} type="button" ref="addValueButton" className="addChildInput btn btn-success">{locale[0][self.props.activeLang]['addValue']}</button>;
            var lngInputAttr = self.props.langs.map(function (lang) {
                var inputName = newModel == 'true' ? "CatAttrLang[" + key + "][" + lang + "]" : "CatAttrLang[newKeys][" + key + "][" + lang + "]";
                var activeLangInput = lang == self.props.activeLang ?
                    <input type="text" onBlur={self.props.handleBlur} onKeyDown={self.props.handleChanges} placeholder={lang} className={catAttrInput} style={{"display":"block"}} name={inputName}/> : <input type="text" className={catAttrInput} style={{"display":"none"}} name={inputName}/>;
                return activeLangInput;
            });

            var defaultLangValues = langs.map(function (lng) {
                var inputName = newModel == 'true' ? "CatAttrValueLang[" + key + "][" + lng + "]" : "CatAttrValueLang[addedValues][" + key + "][" + lng + "]";
                var activeInputDefault = lng == self.props.activeLang ?
                    <input type="text" onBlur={self.props.handleBlur} onKeyDown={self.props.handleChanges}
                           placeholder={lng} name={inputName} className={catAttrValueInput} style={displayBlock}/> :
                    <input type="text" placeholder={lng} style={displayNone} name={inputName}
                           className={catAttrValueInput}/>;
                return activeInputDefault
            })

            if (self.state.valueFields[key] != undefined) {
                var valueInput = self.state.valueFields[key].map(function (value, key) {

                    var lngInput = self.props.langs.map(function (lang) {
                        var inputName = newModel == 'true' ? "CatAttrValueLang[" + value + "][" + lang + "]" : "CatAttrValueLang[addedValues][" + value + "][" + lang + "]";
                        var activeLangInput = lang == self.props.activeLang ?
                            <input type="text" onBlur={self.props.handleBlur} onKeyDown={self.props.handleChanges}
                                   className={catAttrValueInput} placeholder={lang} style={{"display":"block"}}
                                   name={inputName} key={lang}/> :
                            <input type="text" className={catAttrValueInput} style={{"display":"none"}} name={inputName}
                                   key={lang}/>
                        return activeLangInput;

                    });
                    return (
                        <div data-child={value -1} data-parent={parentId} className="inputParent">
                            {lngInput}
                            <button onClick={self.removeChild} type="button" className={removeBtn}>{locale[0][self.props.activeLang]['remove']}</button>
                        </div>
                    )
                })
            }
            return (
                <div className="inputParent">
                    <label htmlFor="cat_attr_key">{locale[0][self.props.activeLang]['catAttr']}</label>
                    <div className="inputParent">
                        {lngInputAttr}
                        {removeButton}
                    </div>
                    <div id={key}>
                        <div><label htmlFor="cat_attr_value">{locale[0][self.props.activeLang]['catAttrValue']}</label></div>
                        <div className="inputParent">
                            {defaultLangValues}
                            <span></span>
                        </div>
                        {valueInput}
                        {addValueButton}
                    </div>
                </div>
            );
        })
        if(locale[0][self.props.activeLang] != undefined){
            var localeString = locale[0][self.props.activeLang]['addAttr'];
        }
        return (<div>
            {fields}
            <button onClick={this.handleClick} className="btn btn-success">{localeString}</button>
        </div>)
    }
});

var CatAttrModels = React.createClass({
    componentWillMount: function () {
        var self = this;
        if (newModel == "false") {
            $.get('/admin/category/get-data?id=' + modelId, function (data) {
                models = data;
                countCatAttr = document.getElementsByClassName('catAttrInput').length;
                countCatAttrValue = document.getElementsByClassName('catAttrValueInput').length;
                self.forceUpdate();
            })
        }

    },
    handleClick: function (e) {
        var parentId = e.target.parentNode.id;
        var parentChildLength = models.attrValuesArray[parentId].items.length;
        if (valueFields[parentId] == undefined) {
            valueFields[parentId] = [];
        }
        valueFields[parentId].push(valueFields[parentId].length + parentChildLength);
        console.log(valueFields)
        this.forceUpdate();
    },
    removeChild: function (parent, child) {
        delete valueFields[parent][child];
        this.forceUpdate();
        console.log(valueFields[parent])
    },
    render: function () {
        var self = this;
        var i = 0;
        if (rows.length == 0) {
            if (models != undefined) {
                var addValueButton = <button onClick={this.handleClick} type="button" className="btn btn-success">add
                    value</button>;
                var modelsArray = models.attrValuesArray.map(function (item, key) {
                    var parentId = item.id;
                    i++;
                    var languages = langs.map(function (lng) {
                        var inputName = "CatAttrLang[update][" + item.id + "][" + lng + "]";
                        var activeInput = lng == self.props.activeLanguage ?
                            <input type="text" defaultValue={item[lng]} name={inputName} style={displayBlock}
                                   className="cat_attr_key form-control"/> :
                            <input type="text" defaultValue={item[lng]} style={displayNone} name={inputName}
                                   className="cat_attr_key form-control"/>;
                        return (<div className="inputParent">
                            {activeInput}
                        </div>)
                    })
                    var catAttrValues = item.items.map(function (child) {
                        var inputName = "CatAttrValueLang[update][" + child.cat_attr_value_id + "][" + child.language + "]";
                        var activeInput = child.language == self.props.activeLanguage ?
                            <input type="text" defaultValue={child.name} name={inputName}
                                   className="cat_attr_value form-control"/> :
                            <input type="text" defaultValue={child.name} style={displayNone} name={inputName}
                                   className="cat_attr_value form-control"/>;
                        return (<div className="inputParent">
                            {activeInput}
                        </div>)
                    })

                    if (valueFields[key] != undefined) {
                        var addInputs = valueFields[key].map(function (item, k) {
                            var languages = langs.map(function (lng) {
                                var inputName = "CatAttrValueLang[newValues][" + parentId + "][" + item + "][" + lng + "]";
                                var activeInput = lng == self.props.activeLanguage ? <input type="text" className={catAttrValueInput} onBlur={self.props.handleBlur} onChange={self.props.handleChanges} name={inputName} style={displayBlock}/> : <input type="text" name={inputName} style={displayNone} className={catAttrValueInput}/>;
                                return activeInput
                            });
                            return (<div className="inputParent">
                                {languages}
                                <button className={removeBtn} onClick={self.removeChild.bind(null,key, k)} type="button">{locale[0][self.props.activeLanguage]['remove']}</button>
                            </div>)


                        });

                    }
                    return (<div id={key}>
                        {languages}
                        {catAttrValues}
                        {addInputs}
                        {addValueButton}
                    </div>)
                })

            }
        }
        return (
            <div>{modelsArray}</div>
        )

    }
});
var MyForm = React.createClass({
    getInitialState: function () {
        return ({
            fields: [1],
            valueFields: [],
            activeLanguage: null,
            increment: 0
        });
    },
    handleBlur: function (e) {
        var input = e.target;
        if (input.value.trim() == '') {
            input.style.borderColor = 'red';
            //e.target.parentNode.childNodes[3].style.display= 'block';
        } else {
            e.target.style.borderColor = 'green';
        }
    },
    handleChanges: function (e) {
        var input = e.target;
        if (input.value.trim != '') {
            e.target.style.borderColor = 'green';
        }
       // e.target.parentNode.childNodes[3].style.display= 'none';
    },
    componentWillMount: function () {
        var self = this;
        if (newModel == "false") {
            $.get('/admin/category/get-data?id=' + modelId, function (data) {
                models = data;
                self.forceUpdate();
            })
        }
    },
    componentDidMount: function () {
        var self = this;
        this.setState({fields: this.state.fields});
        $(document).ready(function () {
            var langCode = $('#category-form .active').data('lang');
            $('#example').attr('lang', langCode);
            self.setState({activeLanguage: document.getElementById('example').getAttribute("lang")})

        });
        $(document).on('click', '#category-form li', function () {
            var langCode = $('#category-form .active').data('lang');
            $('#example').attr('lang', langCode);
            self.setState({activeLanguage: document.getElementById('example').getAttribute("lang")})
            self.forceUpdate();
        })
    },
    render: function () {
        var self = this;
        var textTranslate = self.state.activeLanguage == 'az' ? 'Xana bosh ola bilmez' : 'Заполните поле!';
        var activeInputByLang = <div className="error-block" data-lng={self.state.activeLanguage}>{textTranslate}</div>;
        var fields = this.state.fields.map(function (value, key) {
            return (
                <div id={key} key={key}>
                    <CatAttrModels activeLanguage={self.state.activeLanguage} errorTextLang={activeInputByLang} handleBlur={self.handleBlur} handleChanges={self.handleChanges} />
                </div>
            );
        });
        return (
            <div className="container-form">
                <div className="field-list field-input">
                    {fields}
                </div>
                <CatAttrKeyField langs={langs} activeLang={self.state.activeLanguage} errorTextLang={activeInputByLang} handleBlur={self.handleBlur} handleChanges={self.handleChanges}/>
            </div>
        );
    }
});

var langs = ["az", "ru"];
ReactDOM.render(<MyForm langs={langs}/>, document.getElementById('example'));
