/**
 * Created by fuad on 1/6/16.
 */
function toObject(arr) {
    var rv = {};
    for (var i = 0; i < arr.length; ++i)
        rv[i] = arr[i];
    return rv;
}
var ImageUpload = React.createClass({
    getInitialState:function(){
        return ({
            images:[],
            fileChosen:0
        })
    },
    componentWillMount:function(){
        var self = this;
        var newModel = document.getElementById('imageUpload');
        if(newModel.getAttribute('newModel') == 'false'){
            $.ajax({
                url:'/admin/product/get-images?id='+newModel.getAttribute('modelid'),
                type:'json'
            }).success(function(result){
               var images = result.models;
               $.each(images,function(key,item){
                   self.setState({models:self.state.images.push(item)});
               })
            })
        }
    },
    componentDidMount:function(){
        var fileInput = document.getElementById('fileInput');
        var mainImageInput = document.getElementById('mainImageInput');
        var newModel = document.getElementById('imageUpload');
        if(newModel.getAttribute('newModel') != 'false'){
            $('#submitButton').click(function(){
                if(fileInput.value == ''){
                    alert('Please upload product picture')
                }else{
                    if(mainImageInput.value == ''){
                        alert('Please select main picture')
                    }else{
                        $('#w0').submit();
                    }
                }
            });
        }else{
            $('#submitButton').click(function(){
                $('#w0').submit();
            });
        }
    },
    render:function(){
        var self = this;
        var clearButton = this.state.fileChosen == 1 ? <button type="button" onClick={self._clearInput} className="btn btn-danger btn-lg block margin-bottom-15">clear</button> : '';
        return (
            <div>
                <div>
                    <div id="previewImages">
                        {self.state.images.map(function(item,key){
                            var selectedItem =item.cover == 1 ? 'previewItem selectedMain' : 'previewItem';
                            var setMainButton = item.id == null? <button type="button" onClick={self._setMain.bind(null,item.name,key)} className="btn btn-success setMainPic" style={{bottom:0}}>set main picture 1</button>: <button type="button"  onClick={self._updateMain.bind(null,item.id,key)} className="btn btn-success setMainPic">set main picture 2</button>;
                            var removeButton = item.id != null?<button type="button" onClick={self._removeImage.bind(null,key,item.id)} className="btn btn-danger removePic"><span className="glyphicon glyphicon-remove"></span></button>:'';
                            var mainButton = item.cover != 1 ? setMainButton : '';
                            var newImages = item.id == null ? 'newImage':'';
                            return (<div className={selectedItem+" "+newImages} id={"item_"+key} key={key}>
                                <img src={item.data} className="itemImage" alt=""/>
                                <div className="previewItemLayer">
                                    {mainButton}
                                    {removeButton}
                                </div>
                            </div>)
                        })}
                    </div>
                    <input type="hidden" name="ProductForm[image]" id="mainImageInput"/>
                    <input type="file" multiple className="hidden" onChange={self._loadPreview} name="ProductImages[]" id="fileInput"/>
                    {clearButton}
                    <button type="button" onClick={self._openUploadDialog} className="btn btn-primary btn-lg block margin-bottom-15">add</button>
                </div>
            </div>
        )
    },
    _openUploadDialog:function(){
        $('#fileInput').click();
    },
    _loadPreview:function(e){
        var self = this;
        var filesArray = e.target.files;
        self._getPreviews(filesArray);
    },
    _getPreviews:function(items){
        var self = this;
        $.each(items,function(key,item){
            var reader = new FileReader();
            reader.onload = function (e) {
                self.setState({images:self.state.images.concat({id:null,name:item.name,data:e.target.result})});
                self.setState({fileChosen:1});
            };
            reader.readAsDataURL(item);
        });
    },
    _setMain:function(name,key){
        this._setActiveFrame(key);
        $('#mainImageInput').val(key)
    },
    _updateMain:function(id,key){
        var self = this;
        $.get('/admin/product/update-main?id='+id).success(function(){
            self._setActiveFrame(key);
            document.getElementById('mainImageInput').value = '';
        })
    },
    _clearInput: function () {
        var self = this;
        this.state.images.map(function(item,key){
            if(item.id == null){
                self.state.images.splice(self.state.models,document.getElementById('fileInput').files.length);
                delete self.state.images[key];
            }
        })
        document.getElementById('fileInput').value = '';
        document.getElementById('mainImageInput').value = '';

        $('.unselectedMain').addClass('selectedMain');
        $('.selectedMain').removeClass('uselectedMain');
        this.setState({fileChosen:0})
    },
    _removeImage:function(key,id){
        var self = this;
        var imageName = this.state.images[key].name;
        var fileInput = document.getElementById('mainImageInput');
        if(imageName == fileInput.value){
            fileInput.value = '';
        }
        if(this.state.images.length == 0){
            document.getElementById('fileInput').value = ''
        }

        if(id != null){
            $.get('/admin/product/remove-image?id='+id).success(function(){
                delete self.state.images[key];
                self.state.images.splice(key,1);
                self.setState({images:self.state.images});
                if(self.state.images.length == 0){
                    document.getElementById('fileInput').value = ''
                }
            });
        }
    },
    _setActiveFrame:function(key){
        $('.setMainPic').removeClass('hidden');
        var parent = $('#item_'+key);
        $('.previewItem').each(function(){
            if($(this).hasClass('selectedMain')){
                $(this).addClass('unselectedMain')
            }
            $(this).removeClass('selectedMain')
        });
        $('.previewItemLayer').each(function(){
            $(this).removeClass('hidden')
        });
        parent.addClass('selectedMain');
        parent.find('.setMainPic').addClass('hidden');
    }
});
ReactDOM.render(<ImageUpload />,document.getElementById('imageUpload'));