var pageItem =  $('.pageItem');
var optionItem;
var activeLanguage;
function __(arg){
    console.log(arg);
}
var level = 1;
//select product category
$(document).on('change','.categories-select',function(e){
    var _this = $(this)

    if(_this.parent().hasClass('form-group')){
        $('#categories-block').html('')
    }else{
        _this.nextAll('select').remove()
    }
    var id = $(this).val();
    $.get('/admin/product/categories?id='+id).success(function(result){
            $('#catAttrValBlock').html(result);
        });
    $.ajax({
        url:'/admin/product/get-child',
        method:'post',
        data:{
            id:id
        }
    }).success(function(success){
        if(success != false){
            var options = '';
            $.each(success,function(key,item){
                options += '<option value="'+item.category_id+'">'+item.name+'</option>'
            });

            var html = '<select class="form-control categories-select" name="ProductToCategory[]">'+options+'</select>';
            level++;
                $('#categories-block').append(html)

        }
    }).error(function(err){
        console.error(err)
    })
});
//click on tab
$(document).on('click','.nav-tabs>li>a',function(){
    activeLanguage = $(this).attr('language');
    if(pageItem.length > 0){
        pageItem.each(function(){
            if($(this).data('language') == activeLanguage){
                $(this).css({'display':'block'})
            }else{
                $(this).css({'display':'none'})
            }
        });
    }
    var attrSelect = $('.attrSelect');
    if(attrSelect.length >= 1){
        attrSelect.each(function(){
            $(this).children('option').each(function(){
                $(this).text($(this).attr(activeLanguage))
            })
        })
    }
});
$(document).on('click','.removeAccessory',function(){
    var accId = $(this).data('id');
    if($('.accessory_'+accId).length == 2){
        $('#accessoryList').remove();
    }
    var productId = $(this).data('product');
    $.post('/admin/product/remove-accessory',{
        productId:productId,
        accId:accId
    },function(){
        $('.accessory_'+accId).each(function(){
            $(this).remove();
        });
    });
});

$(document).on('click','.change-status',function(){
        $('#comment').submit();
});


$(document).on('click','.change_cat',function(){
    $.ajax({
        url:'/admin/product/change-category',
        method:'post'
    }).success(function(response){
        $('.cat_block').html(response);
        console.log(response)
    }).error(function(err){
        console.error(err)
    });
    $('.category_name').css('display','none');
});