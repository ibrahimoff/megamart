var pageItem =  $('.pageItem');
var optionItem;
var activeLanguage;
//click on tab
$(document).on('click','.nav-tabs>li>a',function(){
    activeLanguage = $(this).attr('language');
    pageItem.each(function(){
        if($(this).data('language') == activeLanguage){
            $(this).css({'display':'block'})
        }else{
            $(this).css({'display':'none'})
        }
    });
    var attrSelect = $('.attrSelect');
    if(attrSelect.length >= 1){
        attrSelect.each(function(){
            $(this).children('option').each(function(){
                $(this).text($(this).attr(activeLanguage))
            })
        })
    }
});

