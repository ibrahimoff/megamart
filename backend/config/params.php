<?php
return [
    'adminEmail' => 'admin@example.com',
    'size'=>[
        'small'=>[
            'width'=>45,
            'height'=>45,
        ],
        'medium'=>[
            'width'=>80,
            'height'=>80,
        ],
        'home'=>[
            'width'=>130,
            'height'=>130,
        ],
        'large'=>[
            'width'=>300,
            'height'=>300,
        ],
        'thickbox'=>[
            'width'=>600,
            'height'=>600,
        ],

    ],
    'items'=>[
        [
            'url' => '/admin',
            'label' => 'Home',
            'icon' => 'home'
        ],
        [
            'url' => '/admin/category/index',
            'label' => 'Categories',
            'icon' => 'list'
        ],
        [
            'url' => '/admin/product/index',
            'label' => 'Product',
            'icon' => 'file'
        ],
        [
            'url' => '/admin/order/index',
            'label' => 'Orders',
            'icon' => 'barcode'
        ],
        [
            'url' => '/admin/rating/index',
            'label' => 'Ratings',
            'icon' => 'barcode'
        ],
        [
            'url' => '/admin/manufacturer/index',
            'label' => 'Manufacturers',
            'icon' => 'barcode'
        ],
        [
            'url' => '/admin/supplier/index',
            'label' => 'Suppliers',
            'icon' => 'certificate'
        ],
        [
            'url' => '/admin/discount/index',
            'label' => 'Discounts',
            'icon' => 'hourglass'
        ],
        [
            'url' => '/admin/page/index',
            'label' => 'Pages',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/sliders/index',
            'label' => 'Sliders',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/banner/index',
            'label' => 'Banners',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/slider-image/index',
            'label' => 'Slider Images',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/country/index',
            'label' => 'Countries',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/city/index',
            'label' => 'Cities',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/currency/index',
            'label' => 'Currencies',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/payment/index',
            'label' => 'Payments',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/source-message/index',
            'label' => 'Source Messages',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/news/index',
            'label' => 'News',
            'icon' => 'user'
        ],
        [
            'url' => '/admin/calls/index',
            'label' => 'Calls',
            'icon' => 'user'
        ],
    ]
];
