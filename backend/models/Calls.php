<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "calls".
 *
 * @property integer $id
 * @property string $name
 * @property string $number
 * @property string $status
 */
class Calls extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calls';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'number', 'status'], 'required'],
            [['status'], 'string'],
            [['name', 'number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'name' => Yii::t('backend', 'Name'),
            'number' => Yii::t('backend', 'Number'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }
}
