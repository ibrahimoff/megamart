<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `backend\models\Payment`.
 */
class PaymentSearch extends Payment
{
    /**
     * @inheritdoc
     */
    public $name;
    public function rules()
    {
        return [
            [['id', 'deleted'], 'integer'],
            [['logo', 'status','name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'name',
                'status'
            ]
        ]);

        if (!$this->load($params) && $this->validate()) {
            $query->joinWith('payment');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'deleted' => $this->deleted,
        ]);
        $query->joinWith('payment');
        $query->andFilterWhere(['like', 'payment_lang.name', $this->name])
            ->andFilterWhere(['=', 'status', $this->status]);

        return $dataProvider;
    }
}
