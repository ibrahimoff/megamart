<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_attr_lang".
 *
 * @property integer $id
 * @property integer $cat_attr_id
 * @property string $name
 * @property string $language
 *
 * @property CatAttr $catAttr
 */
class CatAttrLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['name', 'language'], 'required'],
            [['cat_attr_id','cat_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
           // [['cat_attr_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatAttr::className(), 'targetAttribute' => ['cat_attr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cat_attr_id' => Yii::t('backend', 'Cat Attr ID'),
            'name' => Yii::t('backend', 'Category attribute name'),
            'language' => Yii::t('backend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttr()
    {
        return $this->hasOne(CatAttr::className(), ['id' => 'cat_attr_id']);
    }
    public function getCatAttValLang(){
        return $this->hasMany(CatAttrValueLang::className(),['cat_attr_id'=>'cat_attr_id']);
    }
}
