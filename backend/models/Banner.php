<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $image
 * @property string $position
 * @property string $status
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'status'], 'required'],
            [['position', 'status'], 'string'],
            [['image'], 'string', 'max' => 255],
            [['file'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Image'),
            'position' => Yii::t('app', 'Position'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function getBannerLangs(){
        return $this->hasMany(BannerLang::className(),['banner_id'=>'id'])->indexBy('language');
    }
}
