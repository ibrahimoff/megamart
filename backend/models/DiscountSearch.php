<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Discount;

/**
 * DiscountSearch represents the model behind the search form about `backend\models\Discount`.
 */
class DiscountSearch extends Discount
{
    /**
     * @inheritdoc
     */
    public $product;
    public $user;
    public function rules()
    {
        return [
            [['id', 'product_id', 'user_id', 'group_id', 'old_price', 'price'], 'integer'],
            [['from_date', 'to_date', 'status', 'created_at', 'updated_at','product','user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Discount::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'product'=>[
                    'asc'=>['product_id'=>SORT_ASC],
                    'desc'=>['product_id'=>SORT_DESC,],
                    'label'=>Yii::t('backend','product'),
                    'default'=>SORT_ASC
                ],
                'user'=>[
                    'asc'=>['user_id'=>SORT_ASC],
                    'desc'=>['user_id'=>SORT_DESC,],
                    'label'=>Yii::t('backend','user'),
                    'default'=>SORT_ASC
                ],
                'old_price',
                'price',
                'from_date',
                'to_date',
                'status',
            ]
        ]);
        if (!$this->load($params) && $this->validate()) {
            $query->joinWith(['productName','user']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'discount.id' => $this->id,
            'discount.old_price' => $this->old_price,
            'discount.price' => $this->price,
            'discount.from_date' => $this->from_date,
            'discount.to_date' => $this->to_date,
            'discount.created_at' => $this->created_at,
            'discount.updated_at' => $this->updated_at,
        ]);
        $query->joinWith(['productName','user']);

        $query->andFilterWhere(['=', 'discount.status', $this->status]);
        $query->andFilterWhere(['like','product_lang.name',$this->product]);
        $query->andFilterWhere(['like','user.username',$this->user]);

        return $dataProvider;
    }
}
