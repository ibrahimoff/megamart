<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property integer $id
 * @property string $logo
 * @property string $status
 * @property integer $deleted
 *
 * @property PaymentLang[] $paymentLangs
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string'],
            [['deleted'], 'integer'],
            [['logo'], 'string', 'max' => 255],
            [['file'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'logo' => Yii::t('backend', 'logo'),
            'status' => Yii::t('backend', 'status'),
            'deleted' => Yii::t('backend', 'deleted'),
            'file' => Yii::t('backend', 'file'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentLangs()
    {
        return $this->hasMany(PaymentLang::className(), ['payment_id' => 'id'])->indexBy('language');
    }
    public function getPayment()
    {
        return $this->hasOne(PaymentLang::className(), ['payment_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
}
