<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_number".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $number
 */
class UserNumber extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_number';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'number'], 'required'],
            [['user_id'], 'integer'],
            [['number'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'user_id' => Yii::t('backend', 'User ID'),
            'number' => Yii::t('backend', 'Number'),
        ];
    }
}
