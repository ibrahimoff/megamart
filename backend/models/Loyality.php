<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loyality".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $point
 * @property integer $user_id
 * @property string $creted_at
 *
 * @property Order $order
 */
class Loyality extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loyality';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id', 'order_id', 'point', 'user_id', 'creted_at'], 'required'],
            [['product_id', 'order_id', 'point', 'user_id'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'point' => Yii::t('app', 'Point'),
            'user_id' => Yii::t('app', 'User ID'),
            'creted_at' => Yii::t('app', 'Creted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
}
