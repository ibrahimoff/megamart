<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sliders".
 *
 * @property integer $id
 * @property integer $position
 * @property string $name
 * @property integer $order
 * @property string $status
 *
 * @property SliderImage[] $sliderImages
 */
class Sliders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const POS_HEAD = 1;
    const POS_FOOTER = 2;
    const POS_LEFT = 3;
    const POS_RIGHT = 4;
    public static function tableName()
    {
        return 'sliders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position', 'name', 'status'], 'required'],
            [['position', 'order'], 'integer'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position' => Yii::t('backend','position'),
            'name' => Yii::t('backend','name'),
            'order' => 'Order',
            'status' => Yii::t('backend','status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSliderImages()
    {
        return $this->hasMany(SliderImage::className(), ['slider_id' => 'id']);
    }
}
