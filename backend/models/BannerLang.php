<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "banner_lang".
 *
 * @property integer $id
 * @property string $language
 * @property integer $banner_id
 * @property string $text
 *
 * @property Banner $banner
 */
class BannerLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['banner_id'], 'integer'],
            [['text'], 'string'],
            [['language'], 'string', 'max' => 2]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'banner_id' => Yii::t('app', 'Banner ID'),
            'text' => Yii::t('app', 'Text'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBanner()
    {
        return $this->hasOne(Banner::className(), ['id' => 'banner_id']);
    }
}
