<?php

namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider_image".
 *
 * @property integer $id
 * @property integer $slider_id
 * @property integer $order
 * @property string $status
 *
 * @property Sliders $slider
 */
class SliderImage extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $file;

    public static function tableName()
    {
        return 'slider_image';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['status','slider_id'], 'required'],
            [['slider_id', 'order'], 'integer'],
            [['status','image'], 'string'],
            [['file'],'file']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slider_id' => Yii::t('backend','Slider'),
            'file' => Yii::t('backend','file'),
            'order' => 'Order',
            'status' =>  Yii::t('backend','status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(Sliders::className(), ['id' => 'slider_id']);
    }
    public function getSliderLangs()
    {
        return $this->hasMany(SliderImageLang::className(), ['image_id' => 'id'])->indexBy('language');
    }
    public function getSliderLang()
    {
        return $this->hasOne(SliderImageLang::className(), ['image_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
    public static function getSlidersList(){
        $sliders = Sliders::find()->all();
        $sliderArray = ArrayHelper::map($sliders,'id','name');
        return $sliderArray;
    }

}
