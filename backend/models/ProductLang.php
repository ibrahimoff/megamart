<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_lang".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $name
 * @property string $short_description
 * @property string $description
 * @property string $slug
 * @property string $language
 *
 * @property Product $product
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id', 'name', 'short_description', 'slug', 'language'], 'required'],
            [['product_id'], 'integer'],
            [['short_description', 'description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'slug' => 'Slug',
            'language' => 'Language',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductOnly(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductCategory(){
        return $this->hasMany(ProductToCategory::className(),['product_id'=>'product_id'])->with('categoryNames');
    }
    public function getProductToCategory(){
        return $this->hasMany(ProductToCategory::className(),['product_id'=>'product_id']);
    }
    public function getDiscount(){
        return $this->hasOne(Discount::className(), ['product_id' => 'product_id'])->orderBy(['id'=>SORT_DESC]);
    }
    public function getProductCoverImage(){
        return $this->hasOne(ProductImage::className(),['product_id'=>'product_id'])->where(['cover'=>1]);
    }
    public function getProductAttr(){
        return $this->hasMany(ProductAttr::className(),['product_id'=>'product_id']);
    }
}
