<?php

namespace backend\models;
/**
 * @TODO сделать сортировку и поиск по полям что подключены связями
 */
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Product;
use yii\helpers\VarDumper;

/**
 * ProductSearch represents the model behind the search form about `backend\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     * public variables of search model for product
     *
     */
    public $name;
    public $category;
    public $manufacturer;
    public $supplier;

    public function rules()
    {
        return [
            [['id','on_sale', 'quantity', 'price', 'wholesale_price', 'cost_price'], 'integer'],
            [['image', 'status', 'created_at', 'updated_at'], 'safe'],
            [['category','manufacturer','name','supplier'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $dataProvider->setSort([
            'attributes'=>[
                'id'=>[
                    'asc'=>['id'=>SORT_ASC],
                    'desc'=>['id'=>SORT_DESC,],
                    'label'=>Yii::t('backend','on_sale'),
                    'default'=>['id'=>SORT_DESC]
                ],
                'name'=>[
                    'asc'=>['product_lang.name'=>SORT_ASC],
                    'desc'=>['product_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','name'),
                ],
                'category'=>[
                    'asc'=>['category_lang.name'=>SORT_ASC],
                    'desc'=>['category_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','category'),
                ],
                'manufacturer'=>[
                    'asc'=>['manufacturer.name'=>SORT_ASC],
                    'desc'=>['manufacturer.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','manufacturer'),
                ],
                'supplier'=>[
                    'asc'=>['supplier.name'=>SORT_ASC],
                    'desc'=>['supplier.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','supplier'),
                ],
                'on_sale'=>[
                    'asc'=>['on_sale'=>SORT_ASC],
                    'desc'=>['on_sale'=>SORT_DESC,],
                    'label'=>Yii::t('backend','on_sale'),
                ],
                'quantity',
                'price',
                'wholesale_price',
                'cost_price',
                'status'
            ]
        ]);

        if (!$this->load($params) && $this->validate()) {
            $query->joinWith(['category','manufacturer','supplier','productName']);
            return $dataProvider;
        }
        $query->joinWith(['category','manufacturer','supplier','productName']);
        $query->andFilterWhere(['like','product_lang.name',$this->name]);
        $query->andFilterWhere(['like','category_lang.name',$this->category]);
        $query->andFilterWhere(['like','manufacturer.name',$this->manufacturer]);
        $query->andFilterWhere(['like','supplier.name',$this->supplier]);
        $query->andFilterWhere(['=','product.id',$this->id]);
        $query->andFilterWhere(['=','product.on_sale',$this->on_sale]);
        $query->andFilterWhere(['=','product.quantity',$this->quantity]);
        $query->andFilterWhere(['=','product.price',$this->price]);
        $query->andFilterWhere(['=','product.wholesale_price',$this->wholesale_price]);
        $query->andFilterWhere(['=','product.cost_price',$this->cost_price]);
        $query->andFilterWhere(['=','product.status',$this->status]);

        return $dataProvider;
    }
}
