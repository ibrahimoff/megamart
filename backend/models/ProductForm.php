<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 12/30/15
 * Time: 10:34 AM
 */
namespace backend\models;
use Elastica\Client;
use Elastica\Document;
use Elastica\Request;
use frontend\models\ProductAttr;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use Yii;
use yii\imagine\Image;

class ProductForm extends Model{

    public $productName;
    public $short_description;
    public $description;
    public $category;
    public $manufacturer;
    public $supplier;
    public $on_sale;
    public $quantity;
    public $price;
    public $wholesale_price;
    public $cost_price;
    public $slug;
    public $status;
    public $catAttrValues;
    public $accessory;
    public $image;
    public $categories;
    public $availability;
    public function rules()
    {
        return [
            [['productName','short_description',
              'description','manufacturer',
              'supplier','on_sale','quantity','price',
              'wholesale_price','cost_price','status','availability'], 'required'],
            [['catAttrValues','slug','accessory','image','categories','category',],'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'productName' => Yii::t('backend', 'Product Name'),
            'short_description' => Yii::t('backend', 'short_description'),
            'description' => Yii::t('backend', 'description'),
            'category' => Yii::t('backend', 'category'),
            'manufacturer' => Yii::t('backend', 'manufacturer'),
            'supplier' => Yii::t('backend', 'supplier'),
            'on_sale' => Yii::t('backend', 'on_sale'),
            'quantity' => Yii::t('backend', 'quantity'),
            'price' => Yii::t('backend', 'price'),
            'wholesale_price' => Yii::t('backend', 'wholesale_price'),
            'cost_price' => Yii::t('backend', 'cost_price'),
            'status' => Yii::t('backend', 'status'),
            'slug' => Yii::t('backend', 'slug'),
            'accessory' => Yii::t('backend', 'accessory'),

        ];
    }

    public static function getCategoryList(){
        $categoryArray = Category::find()->where(['status'=>'1','parent_id'=>'1'])->with('categoryTranslate')->all();
        $categoryList = ArrayHelper::map($categoryArray,'id',function($query){
            return $query->categoryTranslate->name;
        });
        return $categoryList;
    }
    public static function getManufacturerList(){
        $manufacturerArray = Manufacturer::find()->where(['status'=>'1'])->all();
        $manufacturerList = ArrayHelper::map($manufacturerArray,'id','name');
        return $manufacturerList;
    }
    public static function getSupplierList(){
        $supplierArray = Supplier::find()->where(['status'=>'1'])->all();
        $supplierList = ArrayHelper::map($supplierArray,'id','name');
        return $supplierList;
    }
    public function saveProduct(){

        $productFormArray = Yii::$app->request->post('ProductForm');
        $productLangArray = Yii::$app->request->post('ProductLang');
        $productAttrValArray = Yii::$app->request->post('ProductAttributesValues');
        $productImagesArray = Yii::$app->request->post('ProductImages');
        $productToCategory = Yii::$app->request->post('ProductToCategory');

        if(!empty($productFormArray)){
            $newProduct = new Product();
            $newProduct->category_id = $productFormArray['category'];
            if(!empty($productFormArray['image'])){
                $newProduct->image = (substr(sha1(basename($productFormArray['image']).time().rand(1000, 9999)),0,7)).basename($productFormArray['image']);
            }else{
                $newProduct->image = (substr(sha1(basename($productImagesArray[0]).time().rand(1000, 9999)),0,7)).basename($productImagesArray[0]);
            }

            $newProduct->manufacturer_id = $productFormArray['manufacturer'];
            $newProduct->supplier_id = $productFormArray['supplier'];
            //$newProduct->on_sale = $productFormArray['on_sale'];
            //$newProduct->quantity = $productFormArray['quantity'];
            $newProduct->price = $productFormArray['price'];
            $newProduct->availability = $productFormArray['availability'];
            $newProduct->bonus = round($productFormArray['price']/10);
            $newProduct->wholesale_price = $productFormArray['wholesale_price'];
            $newProduct->cost_price = $productFormArray['cost_price'];
            $newProduct->status = $productFormArray['status'];
            $newProduct->created_at = date('Y-m-d H:i:s');

            if($newProduct->save()){
                if(!empty($productFormArray['accessory'])){
                    foreach($productFormArray['accessory'] as $value){
                        $newProductAccessory = new Accessory();
                        $newProductAccessory->product_id = $newProduct->id;
                        $newProductAccessory->acc_id = $value;
                        $newProductAccessory->save();
                    }
                }
                if(!empty($productToCategory)){
                    $keys = array_keys($productToCategory);
                    $values = array_values($productToCategory);
                    $merged = array_merge($keys,$values);
                    $uniqueArray = array_unique($merged);
                    foreach($uniqueArray as $value){
                        $productCategory = new ProductToCategory();
                        $productCategory->product_id = $newProduct->id;
                        $productCategory->category_id = $value;
                        $productCategory->save(false);
                    }
                }
                if(!empty($productLangArray)){
                    foreach(Yii::$app->params['languages'] as $key=>$value){
                        $newProductLang = new ProductLang();
                        $newProductLang->product_id = $newProduct->id;
                        $newProductLang->name = $productLangArray[$key]['productName'];
                        $newProductLang->short_description = $productLangArray[$key]['short_description'];
                        $newProductLang->description = $productLangArray[$key]['description'];
                        $newProductLang->language = $key;
                        $newProductLang->slug = $productLangArray[$key]['slug'];
                        $newProductLang->save();
                    }
                }
                if(!empty($productAttrValArray)){
                    foreach($productAttrValArray as $key=>$value){
                        $productAttrModel = new ProductAttr();
                        $productAttrModel->product_id = $newProduct->id;
                        $productAttrModel->attr_id = $key;
                        $productAttrModel->attr_value_id = $value;
                        $productAttrModel->save();
                    }
                }
                ProductForm::generateImages($newProduct);
                $products = ProductLang::find()
                    ->where(['product_id'=>$newProduct->id])
                    ->with(['productOnly','discount','productCoverImage','productCategory','productAttr','productToCategory'])
                    ->asArray()
                    ->indexBy('language')
                    ->all();

                $client = new Client();
                foreach($products as $key=>$value){
                   $client->request('/products/'.$key.'/'.$value['id'], $method = Request::PUT, $data =  ['_product'=>$value],  $query = array());
                }
            }
        }
    }

    public function updateProduct($id){
        $productForm = Yii::$app->request->post('ProductForm');
        $productLang = Yii::$app->request->post('ProductLang');
        $productValues = Yii::$app->request->post('ProductAttributesValues');
        $productToCategory = Yii::$app->request->post('ProductToCategory');
        $model = Product::find()->where(['id'=>$id])->one();
        $modelLangs = ProductLang::find()->where(['product_id'=>$id])->indexBy('language')->all();
        $modelAttributes = ProductAttr::find()->where(['product_id'=>$id])->indexBy('attr_id')->all();
        $modelAccessory = Accessory::find()->where(['product_id'=>$id])->select('acc_id')->asArray()->all();
        $productToCategoryArray = ProductToCategory::find()->where(['product_id'=>$id])->indexBy('category_id')->all();
        $modelAccessoryArray = [];
        ///VarDumper::dump($productForm,6,1);die();
        foreach($modelAccessory as $value){
            $modelAccessoryArray[] = $value['acc_id'];
        }
        if(is_array($productForm['accessory'])){
            $differenceAccessoryArray = array_diff($productForm['accessory'],$modelAccessoryArray);
        }
        if(!empty($model)){
            if(!empty($productForm)){
                    $model->supplier_id = $productForm['supplier'];
                    if(!empty($productForm['image'])){
                        $model->image = (substr(sha1(basename($productForm['image']).time().rand(1000, 9999)),0,7)).basename($productForm['image']);
                    }
                    //$model->quantity = $productForm['quantity'];
                    $model->availability = $productForm['availability'];
                    $model->price = $productForm['price'];
                    $model->bonus = round($productForm['price']/10);
                    $model->manufacturer_id = $productForm['manufacturer'];
                    $model->wholesale_price = $productForm['wholesale_price'];
                    $model->cost_price = $productForm['cost_price'];
                    $model->status = $productForm['status'];
                    $model->updated_at = date('Y-m-d H:i:s');
                    $model->save();
                if(is_array($productForm['accessory'])){
                        if(!empty($differenceAccessoryArray)){
                            foreach($differenceAccessoryArray as $value){
                                $newAccessory = new Accessory();
                                $newAccessory->product_id = $model->id;
                                $newAccessory->acc_id = $value;
                                $newAccessory->save();
                            }
                        }
                    }
            }
        }
        ProductToCategory::deleteAll(['product_id'=>$id]);
        if(!empty($modelLangs)){
            foreach($modelLangs as $key=>$value){
                $value->name = $productLang[$key]['productName'];
                $value->short_description = $productLang[$key]['short_description'];
                $value->description = $productLang[$key]['description'];
                $value->slug = $productLang[$key]['slug'];
                $value->save();
            }
        }
        if(!empty($productToCategory)){

            foreach($productToCategory as $category){
                    $productCategory = new ProductToCategory();
                    $productCategory->product_id = $id;
                    $productCategory->category_id = $category;
                    $productCategory->save(false);
            }
        }


        if(!empty($productValues)){
            foreach($productValues as $key=>$value){
                if(!empty($modelAttributes[$key])){
                    if($modelAttributes[$key]->attr_value_id != $value){
                        $modelAttributes[$key]->attr_value_id = $value;
                        $modelAttributes[$key]->save();
                    }
                }else{
                        $newAttrValue = new ProductAttr();
                        $newAttrValue->product_id = $id;
                        $newAttrValue->attr_id = $key;
                        $newAttrValue->attr_value_id = $value;
                        $newAttrValue->save(false);
                }
            }
        }
        if(!empty($_FILES['ProductImages']['name'][0])){
            ProductForm::generateImages($model);
        }
        $products = ProductLang::find()
            ->where(['product_id'=>$id])
            ->with(['productOnly','discount','productCoverImage','productAttr','productToCategory'])
            ->asArray()
            ->indexBy('language')
            ->all();
        $client = new Client();
        foreach($products as $key=>$value){
            $client->request('/products/'.$key.'/'.$value['id'], $method = Request::PUT, $data =  ['_product'=>$value],  $query = array());
        }
    }

    public static function generateImages($model){
        $sizeParams = array_reverse(Yii::$app->params['size']);
        $uploaddir = Yii::getAlias('@frontend/web/uploads/products/');
        foreach($_FILES['ProductImages']['name'] as $key=>$image){
            $newProductImage = new ProductImage();
            $newProductImage->product_id = $model->id;
            $newProductImage->order = $key;
            if($key == $_POST['ProductForm']['image']){
                $newProductImage->cover = 1;
            }
            $newProductImage->created_at = date('Y-m-d');
            if($newProductImage->save()){
                $imageName = $newProductImage->product_id.'-'.$newProductImage->id;
                $tmp_image_name = $_FILES['ProductImages']['tmp_name'][$key];
                $uploadedFile = $uploaddir.$imageName;
                move_uploaded_file($tmp_image_name,$uploadedFile.'.jpg');
                $thickboxImage = Image::thumbnail($uploadedFile.'.jpg',600,600);

                if($thickboxImage->save($uploadedFile.'-thickbox.jpg',['quality'=>100])){
                    unset($sizeParams['thickbox']);
                    if(file_exists($uploadedFile.'.jpg')){
                        unlink($uploadedFile.'.jpg');
                    }
                    foreach($sizeParams as $size=>$param){
                        $image = Image::thumbnail($uploadedFile.'-thickbox.jpg',$param['width'],$param['height']);
                        $image->save($uploadedFile.'-'.$size.'.jpg',['quality'=>100]);
                    }
                }
            }
        }
    }
}
