<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_detail".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $price
 *
 * @property Order $order
 */
class OrderDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_detail';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'quantity', 'price'], 'required'],
            [['order_id', 'product_id', 'quantity', 'price'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'order_id' => Yii::t('frontend', 'Order ID'),
            'product_id' => Yii::t('frontend', 'Product ID'),
            'quantity' => Yii::t('frontend', 'Quantity'),
            'price' => Yii::t('frontend', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    public function getOrderItem()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getOrderItemLang()
    {
        return $this->hasOne(ProductLang::className(), ['product_id' => 'product_id'])->where(['language'=>Yii::$app->language]);
    }
}
