<?php

namespace backend\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Address[] $addresses
 * @property Cart[] $carts
 * @property CartProduct[] $cartProducts
 * @property Loyality[] $loyalities
 * @property Order[] $orders
 * @property Profile[] $profiles
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $password;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'role','password','firstname','lastname','status'], 'required'],
            [['status', 'role', 'created_at', 'updated_at'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key','firstname','lastname'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => Yii::t('backend','username'),
            'firstname' => Yii::t('backend','firstname'),
            'lastname' => Yii::t('backend','lastname'),
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password' =>  Yii::t('backend','password'),
            'password_reset_token' => 'Password Reset Token',
            'email' => Yii::t('backend','email'),
            'status' => Yii::t('backend','status'),
            'role' => Yii::t('backend','role'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(Address::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarts()
    {
        return $this->hasMany(Cart::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartProducts()
    {
        return $this->hasMany(CartProduct::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalities()
    {
        return $this->hasMany(Loyality::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfiles()
    {
        return $this->hasMany(Profile::className(), ['user_id' => 'id']);
    }

    public function saveUser(){
        $userArray = Yii::$app->request->post('User');
        $validateUser = User::find()->where(['email'=>$userArray['email']])->one();
        if(empty($validateUser)){
            $this->firstname = $userArray['firstname'];
            $this->lastname = $userArray['lastname'];
            $this->username = $userArray['firstname'].' '.$userArray['lastname'];
            $this->email = $userArray['email'];
            $this->password_hash = Yii::$app->security->generatePasswordHash($userArray['password']);
            $this->status = $userArray['status'];
            $this->role = $userArray['role'];
            $this->save();
            Yii::$app->controller->redirect('/admin/user/index');
        }else{
            Yii::$app->session->setFlash('emailError','This email is not available');
            Yii::$app->controller->redirect(Yii::$app->request->url);
        }

    }

    public function getFullname(){
        return $this->firstname.' '.$this->lastname;
    }
}
