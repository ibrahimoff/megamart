<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_attr".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $unit
 * @property string $status
 *
 * @property Category $category
 * @property CatAttrLang[] $catAttrLangs
 * @property CatAttrValue[] $catAttrValues
 * @property ProductAttr[] $productAttrs
 */
class CatAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['status'], 'required'],
            [['cat_id', 'unit'], 'integer'],
            [['status'], 'string'],
            //[['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'category_id' => Yii::t('backend', 'Category ID'),
            'unit' => Yii::t('backend', 'Unit'),
            'status' => Yii::t('backend', 'Status'),
        ];
    }
    public function afterSave($insert, $changedAttributes){
//
//        $catAttrValueArray = $_POST['CatAttrValueLang'];
//        foreach($catAttrValueArray as $catValueAttrIndex){
//            $catAttrValueModel = new CatAttrValue();
//            $catAttrValueModel->cat_id = $this->id;
//            $catAttrValueModel->save();
//            foreach($catValueAttrIndex as $key=>$value){
//                $catAttrLangModel = new CatAttrValueLang();
//                $catAttrLangModel->cat_attr_value_id = $catAttrValueModel->id;
//                $catAttrLangModel->language = $key;
//                $catAttrLangModel->name = $value;
//                $catAttrLangModel->save();
//            }
//        }

        parent::afterSave($insert, $changedAttributes);

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrLangs()
    {
        return $this->hasMany(CatAttrLang::className(), ['cat_attr_id' => 'id'])->indexBy('language');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValues()
    {
        return $this->hasMany(CatAttrValue::className(), ['cat_attr_id' => 'id']);
    }
    public function getCatAttrValuesLang()
    {
        return $this->hasMany(CatAttrValueLang::className(), ['cat_attr_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttrs()
    {
        return $this->hasMany(ProductAttr::className(), ['attr_id' => 'id']);
    }
}
