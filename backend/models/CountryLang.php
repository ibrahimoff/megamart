<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "county_lang".
 *
 * @property integer $id
 * @property integer $counry_id
 * @property string $name
 * @property string $language
 *
 * @property Country $counry
 */
class CountryLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['country_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'counry_id' => Yii::t('app', 'Counry ID'),
            'name' => Yii::t('backend', 'name'),
            'language' => Yii::t('app', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'counry_id']);
    }
}
