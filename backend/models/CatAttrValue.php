<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_attr_value".
 *
 * @property integer $id
 * @property integer $cat_attr_id
 *
 * @property CatAttr $catAttr
 * @property CatAttrValueLang[] $catAttrValueLangs
 */
class CatAttrValue extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           // [['cat_attr_id'], 'required'],
            [['cat_attr_id','cat_id'], 'integer'],
           // [['cat_attr_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatAttr::className(), 'targetAttribute' => ['cat_attr_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cat_attr_id' => Yii::t('backend', 'Cat Attr ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttr()
    {
        return $this->hasOne(CatAttr::className(), ['id' => 'cat_attr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValueLangs()
    {
        return $this->hasMany(CatAttrValueLang::className(), ['cat_attr_value_id' => 'id']);
    }
}
