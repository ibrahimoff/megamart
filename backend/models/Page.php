<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $order
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property PageLang[] $pageLangs
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['order'], 'integer'],
            [['status'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'order' => Yii::t('backend', 'Order'),
            'status' => Yii::t('backend', 'status'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageLangs()
    {
        return $this->hasMany(PageLang::className(), ['page_id' => 'id'])->indexBy('language');
    }
    public function getPage(){
        return $this->hasOne(PageLang::className(),['page_id'=>'id'])->where(['language'=>Yii::$app->language]);
    }
}
