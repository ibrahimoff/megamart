<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property integer $name
 * @property string $symbol
 * @property integer $decimal
 * @property string $status
 * @property integer $deleted
 *
 * @property Order[] $orders
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'symbol', 'decimal', 'status', 'deleted'], 'required'],
            [['decimal', 'deleted'], 'integer'],
            [['name', 'status'], 'string'],
            [['symbol'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('backend', 'name'),
            'symbol' => Yii::t('backend', 'symbol'),
            'decimal' => Yii::t('backend', 'decimal'),
            'status' => Yii::t('backend', 'status'),
            'deleted' => Yii::t('backend', 'deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['currency_id' => 'id']);
    }
}
