<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Country;

/**
 * CountrySearch represents the model behind the search form about `backend\models\Country`.
 */
class CountrySearch extends Country
{
    /**
     * @inheritdoc
     */
    public $name;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name','status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Country::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'name'=>[
                    'asc'=>['country_lang.name'=>SORT_ASC],
                    'desc'=>['country_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','name'),
                    'default'=>SORT_ASC
                ],
                'status',
            ]
        ]);

        if (!$this->load($params) && $this->validate()) {
            $query->joinWith('country');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['=', 'status', $this->status]);
        $query->joinWith('country');
        $query->andFilterWhere(['like','country_lang.name',$this->name]);
        return $dataProvider;
    }
}
