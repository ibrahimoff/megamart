<?php

namespace backend\models;

use frontend\models\PaymentLang;
use frontend\models\UserAddress;
use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $email
 * @property integer $number
 * @property string $type
 * @property integer $cart_id
 * @property integer $currency_id
 * @property integer $address_id
 * @property integer $payment_id
 * @property integer $total
 * @property string $invoice
 * @property string $date
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Loyality[] $loyalities
 * @property Cart $cart
 * @property OrderDetail[] $orderDetails
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'email', 'number', 'type', 'cart_id', 'currency_id', 'address_id', 'payment_id', 'total', 'invoice', 'date', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'number', 'cart_id', 'currency_id', 'address_id', 'payment_id', 'total'], 'integer'],
            [['type','status','comment'], 'string'],
            [['date', 'created_at', 'updated_at','total_points'], 'safe'],
            [['email'], 'string', 'max' => 255],
            [['invoice'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('frontend', 'ID'),
            'user_id' => Yii::t('frontend', 'User ID'),
            'email' => Yii::t('frontend', 'Email'),
            'number' => Yii::t('frontend', 'Number'),
            'type' => Yii::t('frontend', 'Type'),
            'cart_id' => Yii::t('frontend', 'Cart ID'),
            'currency_id' => Yii::t('frontend', 'Currency ID'),
            'address_id' => Yii::t('frontend', 'Address ID'),
            'payment_id' => Yii::t('frontend', 'Payment ID'),
            'total' => Yii::t('frontend', 'Total'),
            'invoice' => Yii::t('frontend', 'Invoice'),
            'date' => Yii::t('frontend', 'Date'),
            'created_at' => Yii::t('frontend', 'Created At'),
            'updated_at' => Yii::t('frontend', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalities()
    {
        return $this->hasMany(Loyality::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCart()
    {
        return $this->hasOne(Cart::className(), ['id' => 'cart_id']);
    }
    public function getPayment(){
        return $this->hasOne(PaymentLang::className(), ['payment_id' => 'payment_id'])->where(['language'=>Yii::$app->language]);

    }
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->select(['id','firstname','lastname']);
    }
    public function getUserAddress(){
        return $this->hasOne(UserAddress::className(), ['id' => 'address_id'])->select(['id','user_id','address']);
    }
    /*test*/
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetail::className(), ['order_id' => 'id'])->with('orderItem','orderItemLang');
    }

    public static function sendEmail($rating)
    {
        \Yii::$app->mailer->compose(['html' => 'site/rating'], ['rating' => $rating])
            ->setFrom([\Yii::$app->params['adminEmail'] => 'Megamart'])
            ->setTo($rating->user->email)
            ->setSubject('Please rate your order')
            ->send();
    }
}
