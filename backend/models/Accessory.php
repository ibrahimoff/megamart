<?php

namespace backend\models;

use Yii;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "accessory".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $acc_id
 */
class Accessory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accessory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id', 'acc_id'], 'required'],
            [['product_id', 'acc_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'acc_id' => 'Acc ID',
        ];
    }

    public function getAccessoryName(){
        return $this->hasMany(ProductLang::className(),['product_id'=>'acc_id'])->indexBy('language');
    }
    public function getAcc()
    {
        return $this->hasMany(ProductLang::className(), ['product_id' => 'acc_id']);
    }
}
