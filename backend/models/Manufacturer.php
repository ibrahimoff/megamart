<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "manufacturer".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 * @property integer $deleted
 */
class Manufacturer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status','image'], 'string'],
            [['deleted'], 'integer'],
            [['deleted'],'safe'],
            [['name'], 'string', 'max' => 255],
            [['file'],'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' =>  Yii::t('backend','name'),
            'status' => Yii::t('backend','status'),
            'deleted' =>  Yii::t('backend','deleted'),
            'file' =>  Yii::t('backend','file'),
        ];
    }
}
