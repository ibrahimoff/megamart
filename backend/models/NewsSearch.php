<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\News;

/**
 * NewsSearch represents the model behind the search form about `backend\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public $name;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['date','name', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'name'=>[
                    'asc'=>['news_lang.name'=>SORT_ASC],
                    'desc'=>['news_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','name'),
                    'default'=>SORT_ASC
                ],
                'date',
                'status'
            ]

        ]);


        if (!$this->load($params) && $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith('news');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->joinWith('news');
        $query->andFilterWhere(['=', 'status', $this->status]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'date', $this->date]);

        return $dataProvider;
    }
}
