<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "news_lang".
 *
 * @property integer $id
 * @property integer $news_id
 * @property string $name
 * @property string $description
 * @property string $short_description
 * @property string $slug
 * @property string $language
 *
 * @property News $news
 */
class NewsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'short_description',], 'required'],
            [['news_id'], 'integer'],
            [['description', 'short_description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'news_id' => Yii::t('backend', 'News ID'),
            'name' => Yii::t('backend', 'name'),
            'description' => Yii::t('backend', 'description'),
            'short_description' => Yii::t('backend', 'short_description'),
            'slug' => Yii::t('backend', 'slug'),
            'language' => Yii::t('backend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
