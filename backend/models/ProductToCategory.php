<?php

namespace backend\models;

use frontend\models\CategoryLang;
use frontend\models\ProductLang;
use Yii;

/**
 * This is the model class for table "product_to_category".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $category_id
 */
class ProductToCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_to_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'category_id'], 'required'],
            [['product_id', 'category_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }
    public function getCategoryNames(){
        return $this->hasMany(CategoryLang::className(),['category_id'=>'category_id']);
    }
    public function getProducts(){
        return $this->hasMany(ProductLang::className(),['product_id'=>'product_id']);
    }
}
