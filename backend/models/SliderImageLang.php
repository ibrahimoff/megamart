<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "slider_image_lang".
 *
 * @property integer $id
 * @property integer $image_id
 * @property string $language
 * @property string $value
 *
 * @property ProductImage $image
 */
class SliderImageLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider_image_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value','link'], 'required'],
            [['image_id'], 'integer'],
            [['language'], 'string', 'max' => 2],
            [['link'], 'string'],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Image ID',
            'language' => 'Language',
            'value' => Yii::t('backend','Value'),
            'link' => Yii::t('backend','link'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(ProductImage::className(), ['id' => 'image_id']);
    }
}
