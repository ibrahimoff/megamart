<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "payment_lang".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property string $name
 * @property string $description
 * @property string $language
 *
 * @property Payment $payment
 */
class PaymentLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','description'], 'required'],
            [['payment_id'], 'integer'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'payment_id' => Yii::t('backend', 'Payment ID'),
            'name' => Yii::t('backend', 'name'),
            'description' => Yii::t('backend', 'description'),
            'language' => Yii::t('backend', 'language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }
}
