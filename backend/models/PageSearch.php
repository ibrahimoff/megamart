<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Page;

/**
 * PageSearch represents the model behind the search form about `backend\models\Page`.
 */
class PageSearch extends Page
{
    /**
     * @inheritdoc
     */
    public $name;
    public function rules()
    {
        return [
            [['id', 'order'], 'integer'],
            [['name','status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Page::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        if (!$this->load($params) && $this->validate()) {
            $query->joinWith('page');
            return $dataProvider;
        }
        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'name'=>[
                    'asc'=>['page_lang.name'=>SORT_ASC],
                    'desc'=>['page_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','name'),
                    'default'=>SORT_ASC
                ],
                'status'
            ]

        ]);
        $query->andFilterWhere([
            'id' => $this->id,
            'order' => $this->order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['=', 'status', $this->status]);
        $query->joinWith('page');
        $query->andFilterWhere(['like', 'page_lang.name', $this->name]);

        return $dataProvider;
    }
}
