<?php
namespace backend\models;
use yii\elasticsearch\ActiveRecord;
class ProductElastic extends ActiveRecord
{
    public function rules()
    {
        return [
                [['id','product_id'],'integer'],
                [['name','short_description','description','slug','language'], 'string'],

        ];
    }
    public function attributes()
    {
        return ['id', 'name','product_id','name','short_description','description','slug','language'];
    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductCoverImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'product_id'])->where(['cover'=>'1']);
    }
    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['product_id' => 'product_id']);
    }
    public function getProductAttr(){
        return $this->hasMany(ProductAttr::className(), ['product_id' => 'product_id']);
    }

}