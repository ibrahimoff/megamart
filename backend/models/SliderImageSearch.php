<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\SliderImage;
use yii\helpers\VarDumper;

/**
 * SliderImageSearch represents the model behind the search form about `backend\models\SliderImage`.
 */
class SliderImageSearch extends SliderImage
{
    /**
     * @inheritdoc
     */
    public $value;
    public $slider;

    public function rules()
    {
        return [
            [['id', 'slider_id', 'order'], 'integer'],
            [['image', 'status','value','slider'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SliderImage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'slider'=>[
                    'asc'=>['slider_id'=>SORT_ASC],
                    'desc'=>['slider_id'=>SORT_DESC,],
                    'label'=>Yii::t('backend','slider'),
                    'default'=>SORT_ASC
                ],
                'value'=>[
                    'asc'=>['slider_image_lang.value'=>SORT_ASC],
                    'desc'=>['slider_image_lang.value'=>SORT_DESC,],
                    'label'=>Yii::t('backend','value'),
                    'default'=>SORT_ASC
                ],
                //'link',
                'status',
            ]
        ]);
        if (!$this->load($params) && $this->validate()) {
            $query->joinWith(['slider','sliderLang']);
            return $dataProvider;
        }
//        if($this->id !=''){
//            $query->andWhere(['=','id',$this->id]);
//        }
        $query->joinWith(['slider','sliderLang']);

        $query->where('sliders.name LIKE "%' .
            $this->slider . '%"');
        $query->where('slider_image_lang.value LIKE "%' .
            $this->value . '%"');
        if($this->status!=''){
            $query->andWhere(['=','slider_image.status',$this->status]);
        }
        return $dataProvider;
    }
}
