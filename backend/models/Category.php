<?php

namespace backend\models;

use common\components\MultilingualBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property integer $level
 * @property string $status
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CatAttr[] $catAttrs
 * @property Category $parent
 * @property Category[] $categories
 * @property CategoryLang[] $categoryLangs
 * @property Product[] $products
 */

class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'level', 'order'], 'integer'],
            [['status'], 'required'],
            [['status'], 'string'],
            [['created_at', 'updated_at','top'], 'safe'],
           // [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }
    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                // 'treeAttribute' => 'tree',
                 'leftAttribute' => 'nleft',
                 'rightAttribute' => 'nright',
                 'depthAttribute' => 'level',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'parent_id' => Yii::t('backend', 'Parent ID'),
            'level' => Yii::t('backend', 'Level'),
            'status' => Yii::t('backend', 'status'),
            'order' => Yii::t('backend', 'Order'),
            'created_at' => Yii::t('backend', 'Created At'),
            'updated_at' => Yii::t('backend', 'Updated At'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrs()
    {
        return $this->hasMany(CatAttr::className(), ['cat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }
    public function getP(){
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    public static function getParents(){
        $parents = Category::find()->with('categoryTranslate')->where(['status'=>'1'])->all();
        $parentsArray = ArrayHelper::map($parents,'id',function($query){
            return $query->categoryTranslate->name;
        });
        return $parentsArray;
    }
    public static function addOrdering(){
        $categoryMaxOrdering = Category::find()->where(['status'=>'1'])->max("`order`");
        $categoryMaxOrdering +=1;
        return $categoryMaxOrdering;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryLangs()
    {
        return $this->hasMany(CategoryLang::className(), ['category_id' => 'id'])->indexBy('language');
    }
    public function getCategoryTranslate()
    {
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'id'])->where(['language'=>Yii::$app->language]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}
