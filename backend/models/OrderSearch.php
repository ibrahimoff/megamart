<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;


/**
 * OrderSearch represents the model behind the search form about `backend\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public $user;
    public $address;
    public function rules()
    {
        return [
            [['id', 'user', 'number', 'cart_id', 'currency_id', 'payment_id', 'total'], 'integer'],
            [['email', 'type', 'invoice', 'date', 'created_at', 'updated_at','status','address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find()->orderBy(['id'=>SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'user'=>[
                    'asc'=>['firstname'=>SORT_ASC,'lastname'=>SORT_ASC],
                    'desc'=>['firstname'=>SORT_DESC,'lastname'=>SORT_DESC],
                ],
                'email',
                'number',
                'status',
                'address'=>[
                    'asc'=>['address'=>SORT_ASC],
                    'desc'=>['address'=>SORT_DESC],
                ],
                'created_at'
            ]
        ]);
        if (!$this->load($params) && $this->validate()) {
            $query->joinWith(['user','userAddress']);
            return $dataProvider;
        }
        $query->joinWith(['user','userAddress']);
        $query->andFilterWhere([
            'id' => $this->id,
            'number' => $this->number,
            'cart_id' => $this->cart_id,
            'currency_id' => $this->currency_id,
            'address_id' => $this->address_id,
            'payment_id' => $this->payment_id,
            'total' => $this->total,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

       // VarDumper::dump(,6,1);die();
        $query->andFilterWhere(['like', 'order.email', $this->email])
            ->andFilterWhere(['=','order.status',1])
            ->andFilterWhere(['like', 'firstname', $this->user.'%',false]);
        if(preg_match('/\s/',$this->user)){
            $userField = explode(" ",$this->user);
            $query->andFilterWhere(['like','firstname',$userField[0].'%',false])
                ->orFilterWhere(['like','firstname',$userField[1].'%',false]);
            $query->andFilterWhere(['like','lastname',$userField[0].'%',false])
                ->orFilterWhere(['like','lastname',$userField[1].'%',false]);
        }else{
            $userField = $this->user;
            $query->andFilterWhere(['like','firstname',$userField.'%',false])
                    ->orFilterWhere(['like','lastname',$userField.'%',false]);
        }
        $query->andFilterWhere(['like','address',$this->address]);
        return $dataProvider;
    }
}
