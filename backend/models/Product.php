<?php

namespace backend\models;

use Yii;
use yii\base\View;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $manufacturer_id
 * @property integer $supplier_id
 * @property string $image
 * @property integer $on_sale
 * @property integer $quantity
 * @property integer $price
 * @property integer $wholesale_price
 * @property integer $cost_price
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Discount[] $discounts
 * @property Loyality[] $loyalities
 * @property Category $category
 * @property ProductImage[] $productImages
 * @property ProductLang[] $productLangs
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          //  [['category_id', 'manufacturer_id', 'supplier_id', 'on_sale', 'quantity', 'price', 'wholesale_price', 'cost_price', 'status'], 'required'],
            [['category_id', 'manufacturer_id', 'supplier_id', 'on_sale', 'quantity', 'wholesale_price', 'cost_price'], 'integer'],
            [['status', 'price'], 'string'],
            [['image'], 'string', 'max' => 255],
            [['bonus', 'availability'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category',
            'manufacturer_id' => 'Manufacturer',
            'supplier_id' => 'Supplier',
            'image' => 'Image',
            'on_sale' => 'On Sale',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'wholesale_price' => 'Wholesale Price',
            'cost_price' => 'Cost Price',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['product_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalities()
    {
        return $this->hasMany(Loyality::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(CategoryLang::className(), ['category_id' => 'category_id'])->where(['category_lang.language'=>Yii::$app->language]);
    }

    public function getManufacturer(){
        return $this->hasOne(Manufacturer::className(),['id'=>'manufacturer_id']);
    }
    public function getSupplier(){
        return $this->hasOne(Supplier::className(),['id'=>'supplier_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
    }

    public function getProductCoverImage()
    {
        return $this->hasOne(ProductImage::className(), ['product_id' => 'id'])->where(['cover'=>1]);
    }
    public function getCover($size){
        if(!empty($size)){
            $data = Yii::getAlias('/frontend/web/uploads/products/'.$this->productCoverImage->product_id.'-'.$this->productCoverImage->id.'-'.$size.'.jpg');
        }else{
            $data = 'You must set size of image';
        }
        return $data;
    }
    public function getParent(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);

    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductLangs()
    {
        return $this->hasMany(ProductLang::className(), ['product_id' => 'id'])->indexBy('language');
    }
    public function getProductName(){
        return $this->hasOne(ProductLang::className(),['product_id'=>'id'])->where(['product_lang.language'=>Yii::$app->language]);
    }

    public static function _arrayCategory($id){
        $categoryAttributes = CatAttrLang::find()->where(['cat_id'=>$id])->asArray()->with('catAttValLang')->all();
        $catAttrArray = [];
        foreach($categoryAttributes as $key=>$val){
            $catAttrArray[$val['cat_attr_id']]['key'] = $val['cat_attr_id'];
            $catAttrArray[$val['cat_attr_id']]['languages'][$val['language']] = $val['name'];
            foreach($val['catAttValLang'] as $k=>$v){
                $catAttrArray[$val['cat_attr_id']]['items'][$v['cat_attr_value_id']][$v['language']] = $v['name'];
            }
        }
        return $catAttrArray;
    }
    public static function categoryList($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Product::_arrayCategory($id);
        $html = Yii::$app->controller->renderPartial('_catAttrVal',[
            'catAttrArray'=>$model,
            'newModel'=>true
        ]);
        return $html;
    }
    public static function getChild(){
        if(Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $id = Yii::$app->request->post('id');
            $category = CategoryLang::find()->joinWith('category')->where(['language'=>Yii::$app->language,'category.parent_id'=>$id])->all();
            if(!empty($category)){
                return $category;
            }else {
                return false;
            }
        }
        return false;
    }
    public static function catAttrList($id){
        $catAttr = CatAttrLang::find()->where(['cat_id'=>$id])->indexBy('language')->all();
        $catAttrValueLang = CatAttrValueLang::find()->where(['cat_id'=>$id])->indexBy('language')->all();
        $catAttrArray = [];
            foreach($catAttr as $key=>$value){
                $catAttrArray[$key]['name'] = $value->name;
                $catAttrArray[$key]['cat_attr_id'] = $value->cat_attr_id;
                foreach($catAttrValueLang as $k=>$v){
                    if($key == $v->language){
                        $catAttrArray[$key]['cat_attr_values'][$v->cat_attr_value_id]['name'] = $v->name;
                        $catAttrArray[$key]['cat_attr_values'][$v->cat_attr_value_id]['language'] = $v->language;
                    }
                }

            }
        return $catAttrArray;
    }
    public static function getModelAttributes($id){
        $model = new ProductForm();
        $productModel = Product::find()->where(['id'=>$id])->with('productLangs')->one();
        $categories = ProductToCategory::find()->where(['product_id'=>$id])->asArray()->indexBy('category_id')->all();
        $catArray = [];
        foreach($categories as $k=>$catID){
            $categoriesArray = Category::find()->where(['parent_id'=>$k])->with('categoryTranslate')->asArray()->all();
            if(!empty($categoriesArray)){
                $catArray[$k]=$categoriesArray;
            }
        }
        $productAttrValues = ProductAttr::find()->where(['product_id'=>$id])->asArray()->all();
        $productTranslates = ProductLang::find()
            ->select(['name',
                'short_description',
                'description',
                'slug','language'])
            ->where(['product_id'=>$id])
            ->indexBy('language')
            ->asArray()
            ->all();
        $attributesArray = $productModel->attributes;

        foreach($productTranslates as $key=>$value){
            $attributesArray['productName'][$key] = $value['name'];
            $attributesArray['short_description'][$key] = $value['short_description'];
            $attributesArray['description'][$key] = $value['description'];
            $attributesArray['slug'][$key] = $value['slug'];
            $attributesArray['language'][$key] = $value['language'];
        }
        foreach($productAttrValues as $k=>$v){
            $attributesArray['catAttrValues'][] = $v['attr_value_id'];
        }
        $attributesArray['category'] = $productModel->category_id;
        $attributesArray['categories'] = $catArray;
        $attributesArray['image'] = $productModel->image;
        $attributesArray['manufacturer'] = $productModel->manufacturer_id;
        $attributesArray['supplier'] = $productModel->supplier_id;

        $model->attributes = $attributesArray;
        return $model;
    }
    public static function getAllImages($id){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $array = [];
        $models = ProductImage::find()->where(['product_id'=>$id])->all();
        foreach($models as $key=>$value){
            $array[$key]['id'] = $value->id;
            $array[$key]['cover'] =$value->cover;
            $array[$key]['data'] ='/frontend/web/uploads/products/'.$id.'-'.$value->id.'-large.jpg';
        }
        $data = [
            'models'=>$array
        ];
        return $data;
    }
}
