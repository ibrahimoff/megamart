<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Category;

/**
 * CategorySearch represents the model behind the search form about `backend\models\Category`.
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public $name;

    public function rules()
    {
        return [
            [['id', 'parent_id', 'level', 'order'], 'integer'],
            [['status', 'created_at', 'updated_at','name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Category::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'name'=>[
                    'asc'=>['name'=>SORT_ASC],
                    'desc'=>['name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','name'),
                    'default'=>SORT_ASC
                ],
                'status'
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->joinWith(['categoryTranslate']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'level' => $this->level,
            'order' => $this->order,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);


        $query->joinWith('categoryTranslate');
        $query->andFilterWhere(['like','category_lang.name',$this->name]);
            $query->andFilterWhere(['=', 'status', $this->status]);



        return $dataProvider;
    }
}
