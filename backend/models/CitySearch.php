<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\City;

/**
 * CitySearch represents the model behind the search form about `backend\models\City`.
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public $city;
    public $country;
    public function rules()
    {
        return [
            [['id', 'country_id'], 'integer'],
            [['status','city','country'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->setSort([
            'attributes'=>[
                'id',
                'country'=>[
                    'asc'=>['country_lang.name'=>SORT_ASC],
                    'desc'=>['country_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','country'),
                    'default'=>SORT_ASC
                ],
                'city'=>[
                    'asc'=>['city_lang.name'=>SORT_ASC],
                    'desc'=>['city_lang.name'=>SORT_DESC,],
                    'label'=>Yii::t('backend','city'),
                    'default'=>SORT_ASC
                ],
                'status',
            ]
        ]);
        if (!$this->load($params) && $this->validate()) {
            $query->joinWith(['city','country']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'city.id' => $this->id,
            'country_id' => $this->country_id,
        ]);

        $query->andFilterWhere(['=', 'status', $this->status]);
        $query->joinWith(['city','country']);
        $query->andFilterWhere(['like','city_lang.name',$this->city]);
        $query->andFilterWhere(['like','country_lang.name',$this->country]);
        return $dataProvider;
    }
}
