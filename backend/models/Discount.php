<?php

namespace backend\models;

use common\models\User;
use Yii;

/**
 * This is the model class for table "discount".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $user_id
 * @property integer $group_id
 * @property integer $old_price
 * @property integer $price
 * @property string $from_date
 * @property string $to_date
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Product $product
 * @property UserGroup $group
 */
class Discount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id','price', 'from_date', 'to_date', 'status'], 'required'],
            [['product_id', 'user_id', 'group_id', 'old_price', 'price'], 'integer'],
            [['from_date', 'to_date', 'created_at', 'updated_at'], 'safe'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => Yii::t('backend','Product'),
            'user_id' => Yii::t('backend','User'),
            'group_id' => Yii::t('backend','Group'),
            'old_price' => Yii::t('backend','Old Price'),
            'price' => Yii::t('backend','Price'),
            'from_date' => Yii::t('backend','From Date'),
            'to_date' =>Yii::t('backend','To Date') ,
            'status' => Yii::t('backend','status'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductName(){
        return $this->hasOne(ProductLang::className(),['product_id'=>'product_id'])->where(['language'=>Yii::$app->language]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(UserGroup::className(), ['id' => 'group_id']);
    }
    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }
}
