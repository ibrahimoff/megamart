<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product_attr".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $attr_id
 * @property string $value
 *
 * @property CatAttr $attr
 */
class ProductAttr extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_attr';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['product_id', 'attr_id', 'attr_value_id'], 'required'],
            [['product_id', 'attr_id','attr_value_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'attr_id' => 'Attr ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttr()
    {
        return $this->hasOne(CatAttr::className(), ['id' => 'attr_id']);
    }
}
