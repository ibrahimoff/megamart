<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cat_attr_value_lang".
 *
 * @property integer $id
 * @property integer $cat_attr_value_id
 * @property string $name
 * @property string $language
 *
 * @property CatAttrValue $catAttrValue
 */
class CatAttrValueLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cat_attr_value_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['cat_attr_value_id', 'name', 'language'], 'required'],
            [['cat_attr_value_id','cat_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language'], 'string', 'max' => 10],
           // [['cat_attr_value_id'], 'exist', 'skipOnError' => true, 'targetClass' => CatAttrValue::className(), 'targetAttribute' => ['cat_attr_value_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend', 'ID'),
            'cat_attr_value_id' => Yii::t('backend', 'Cat Attr Value ID'),
            'name' => Yii::t('backend', 'Name'),
            'language' => Yii::t('backend', 'Language'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatAttrValue()
    {
        return $this->hasOne(CatAttrValue::className(), ['id' => 'cat_attr_value_id']);
    }

    public function getCatAttrLang(){
        return $this->hasOne(CatAttrLang::className(),['cat_attr_id'=>'cat_attr_id']);
    }
}
