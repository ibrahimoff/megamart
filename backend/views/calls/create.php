<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Calls */

$this->title = Yii::t('backend', 'Create Calls');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Calls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calls-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
