<?php
use dosamigos\tinymce\TinyMce;
?>
<?=$form->field($model,'name')->textInput(['name'=>'PageLang['.$language.'][name]','class'=>'form-control page_name_'.$language])?>
<?= $form->field($model, 'description')->widget(TinyMce::className(), [
    'options' => ['rows' => 6,'name'=>'PageLang['.$language.'][description]','id'=>'pagelang-description'.$language],
    'language' => 'ru',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]);?>
<?=$form->field($model,'slug')->textInput(['name'=>'PageLang['.$language.'][slug]','hidden'=>true,'class'=>'slug_'.$language])->label(false)?>
<script>
    $('.slug_<?=$language?>').slugify('.page_name_<?=$language?>');
</script>
