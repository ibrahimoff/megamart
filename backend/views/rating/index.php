<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\RatingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Ratings');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rating-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'user_id',
                'value'=>function($data){
                    return $data->user->fullname;
                },
                'filter'=>Html::activeTextInput($searchModel,'user_id',['class'=>'form-control'])
            ],
            'rating',
            'comment:ntext',
            [
                'attribute'=>'status',
                'value'=>function($data){
                    if($data->status=='1')
                        return "Active";
                    return "Not Active";
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',['0'=>'Not Active','1'=>'Active'],['prompt'=>'Select status','class'=>'form-control']),
                'contentOptions'=>['style'=>'width: 150px;']
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
