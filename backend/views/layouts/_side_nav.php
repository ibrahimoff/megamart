<?php
$items = Yii::$app->params['items'];
if(Yii::$app->user->can('user_panel')){
    $items[] = ['label' => 'User control', 'url' => '/admin/user/index'];
}
?>
<div id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <?php foreach($items as $item):?>
            <li><a href="/<?=Yii::$app->language.$item['url']?>"><?=Yii::t('backend',$item['label'])?></a></li>
        <?php endforeach;?>
    </ul>

</div>

