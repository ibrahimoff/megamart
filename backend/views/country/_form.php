<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $items = [];
    foreach(Yii::$app->params['languages'] as $lang=>$info){
        $tab = [
            'label'=>Yii::t('backend',$info['lang']),

            'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
            ]),
            'active'=>$lang=='en',
            'headerOptions' => ['data-lang' => $lang,'class'=>'lang_tabs'],
        ];

        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
