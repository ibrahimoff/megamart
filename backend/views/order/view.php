<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Order */

$this->title = 'Order № '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if(Yii::$app->user->getIsAdmin()):?>
        <p>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif?>

    <?php if($model->status == '1'):?>
    <?= Html::a('Confirm', ['confirm', 'id' => $model->id], ['class'=>'btn btn-success change-status']) ?>
    <?= Html::a('Decline', ['decline', 'id' => $model->id], ['class'=>'btn btn-danger change-status']) ?>
<!--    --><?php //$form = ActiveForm::begin(['action'=>'comment','method'=>'post','id'=>'comment']); ?>
<!--    <div class="form-group">-->
<!---->
<!--    </div>-->
<!--    <div class="form-group col-sm-6">-->
<!--        --><?//=Html::activeTextarea($model,'comment',['class'=>'form-control','name'=>'Order[comment]'])?>
<!--        --><?//=Html::hiddenInput('hidden_id',$model->id)?>
<!--    </div>-->
<!--    --><?php //ActiveForm::end(); ?>
    <?php endif?>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute'=>'user_id',
                'label'=>'User',
                'value'=>$model->user->firstname.' '.$model->user->lastname,
            ],
            'email:email',
            'number',
            //'type',
            'cart_id',
            //'currency_id',
            [
                'attribute'=>'address_id',
                'label'=>'Address',
                'value'=>$model->address,
            ],
            'payment_id',
            [
                'attribute'=>'payment_id',
                'label'=>'Address',
                'value'=>$model->payment->name,
            ],
            'total',
            'invoice',
            'date',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
