<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="order-index">
    <?=Yii::$app->user->isAdmin?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
            if($model->status == '0'){
                $class = ['class'=>'warning'];
            }elseif($model->status == '1'){
                $class = ['class'=>'info'];
            }elseif($model->status == '2'){
                $class = ['class'=>'success'];
            }elseif($model->status == '3'){
                $class = ['class'=>'danger'];
            }
            return $class;
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'attribute'=>'user',
                'value'=>function($data){
                    return $data->user->firstname.' '.$data->user->lastname;
                },
                'filter'=>Html::activeTextInput($searchModel,'user',['class'=>'form-control'])
            ],
            'email:email',
            'number',
            //'type',
            [
                'attribute'=>'status',
                'value'=>function($data){
                    if($data->status == '0'){
                        $status = 'pending';
                    }elseif($data->status == '1'){
                        $status = 'seen';
                    }elseif($data->status == '2'){
                        $status = 'confirmed';
                    }elseif($data->status == '3'){
                        $status = 'declined';
                    }
                    return $status;
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',['0'=>'Pending','1'=>'Confirmed','2'=>'Declined'],['prompt'=>'Select status','class'=>'form-control'])
            ],
            // 'cart_id',
            // 'currency_id',
            [
                'attribute'=>'address',
                'value'=>function($data){
                    return !empty($data->address) ? $data->address : '';
                },
                'filter'=>Html::activeTextInput($searchModel,'address',['class'=>'form-control'])
            ],
            // 'payment_id',
            // 'total',
            // 'invoice',
             //'date',
             'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn','template'=> Yii::$app->user->isAdmin?'{view} {delete}':'{view}'],
        ],
    ]); ?>

</div>


