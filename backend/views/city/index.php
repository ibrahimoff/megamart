<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create City'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>Yii::t('backend','country'),
                'value'=>function($data){
                    return $data->country->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'country',['class'=>'form-control'])
            ],
            [
                'attribute'=>Yii::t('backend','city'),
                'value'=>function($data){
                    return $data->city->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'city',['class'=>'form-control'])
            ],
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
