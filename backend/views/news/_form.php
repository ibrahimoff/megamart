<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">
    <script src="/admin/js/jquery.slugify.js" type="text/javascript"></script>
    <?php $form = ActiveForm::begin(); ?>
    <?php $items = [];
    foreach(Yii::$app->params['languages'] as $lang=>$info){
        $tab = [
            'label'=>Yii::t('backend',$info['lang']),

            'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
            ]),
            'active'=>$lang=='en',
            'headerOptions' => ['data-lang' => $lang,'class'=>'lang_tabs'],
        ];

        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <div class="input_display_inline">
        <?php
        echo '<label class="control-label">'.Yii::t('backend','date').'</label>';
        echo DateTimePicker::widget([
            'name' => 'News[date]',
            'type' => DateTimePicker::TYPE_INLINE,
            'value'=>$model->isNewRecord ? '' :$model->date,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd H:i:s'
            ]
        ]);?>
    </div>
    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
