<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Banner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'image',
                'label'=>Yii::t('backend','image'),
                'format' => 'html',
                'value'=>function($data) {
                    return "<img src='".Yii::getAlias('/frontend/web/uploads/banners/').$data->image."' style='width:90px;height:auto;'/>";
                },
                'filter'=>''
            ],
            [
                'attribute'=>'position',
                'label' => Yii::t('backend', 'position'),
                'value'=>function($data){
                    if($data->position == 1){
                        $headline =  Yii::t('backend','Head');
                    }elseif($data->position == 2){
                        $headline =  Yii::t('backend','Center');
                    }else{
                        $headline =  Yii::t('backend','Footer');
                    }
                    return $headline;
                },
                'filter'=>Html::activeDropDownList($searchModel,'position',[''=>Yii::t('backend','select_position'),'0'=>Yii::t('backend','head'),'1'=>Yii::t('backend','center'),'2'=>Yii::t('backend','footer')],['class'=>'form-control']),
            ],
            [
                'attribute'=>'status',
                'label' => Yii::t('backend', 'status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
