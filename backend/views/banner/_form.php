<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Banner */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banner-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <?php $items = [];

    foreach(Yii::$app->params['languages'] as $lang=>$info){
        $tab = [
            'label'=>Yii::t('backend',$info['lang']),

            'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
            ]),
            'active'=>$lang=='en',
            'headerOptions' => ['data-lang' => $lang,'class'=>'lang_tabs'],
        ];

        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <?php if(!$model->isNewRecord){?>
    <div style="width: 300px;height: auto">
        <img src="<?=Yii::getAlias('/frontend/web/uploads/banners/'.$model->image)?>" alt="" width="100%"/>
    </div>
    <?php }?>
    <?= $form->field($model, 'file')->widget(\kartik\file\FileInput::classname(), [
        'options' => ['accept' => 'image/*'],
        'pluginOptions'=>[
            'showUpload'=>false
        ]
    ]);?>

    <?= $form->field($model, 'position')->dropDownList([ 1 => 'Head', 2 => 'Center',3 => 'Footer', ], ['prompt' => 'Select position']) ?>

    <?= $form->field($model, 'status')->dropDownList(['0'=>Yii::t('backend','inactive'), '1'=>Yii::t('backend','active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
