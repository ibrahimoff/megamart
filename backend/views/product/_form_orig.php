<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="/admin/js/jquery.slugify.js" type="text/javascript"></script>
<script src="/admin/js/jquery.validate.min.js" type="text/javascript"></script>
<div class="product-form">
    <?php $formField = ActiveForm::begin(); ?>
        <?= $formField->field($formModel, 'productName')->textInput() ?>
    <?php ActiveForm::end(); ?>
    <?php $form = ActiveForm::begin(); ?>
    <?php $items = [];
    foreach(Yii::$app->params['languages'] as $lang=>$info){
        $tab = [
            'label'=>$info['lang'],

            'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
            ]),
            'active'=>$lang=='en',
            'headerOptions' => ['data-lang' => $lang,'class'=>'lang_tabs'],
        ];

        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <?= $form->field($model, 'category_id')->dropDownList(\backend\models\Product::getCategoryList(),['prompt'=>'Select category']) ?>

    <?= $form->field($model, 'manufacturer_id')->dropDownList(\backend\models\Product::getManufacturerList(),['prompt'=>'Select manufacturer']) ?>

    <?= $form->field($model, 'supplier_id')->dropDownList(\backend\models\Product::getSupplierList(),['prompt'=>'Select supplier']) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'on_sale')->dropDownList(['prompt'=>'Select on sale','0'=>'True','1'=>'False']) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'wholesale_price')->textInput() ?>

    <?= $form->field($model, 'cost_price')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
