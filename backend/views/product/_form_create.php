<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use dosamigos\tinymce\TinyMce;
/* @var $this yii\web\View */
/* @var $model backend\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<script src="/admin/js/jquery.slugify.js" type="text/javascript"></script>
<script src="/admin/js/components/imageUpload.js" type="text/babel"></script>
<div class="">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div>
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach(Yii::$app->params['languages'] as $key=>$value):?>
                <li role="presentation" class="<?=$value['code'] == Yii::$app->language? "active": ""?>"><a href="#<?=$value['code']?>" aria-controls="home" role="tab" data-toggle="tab" language="<?=$value['code']?>"><?=$value['lang']?></a></li>
            <?php endforeach;?>
        </ul>
        <div class="tab-content">
            <?php foreach(Yii::$app->params['languages'] as $key=>$value):?>
                <div role="tabpanel" class="tab-pane <?=$value['code'] == Yii::$app->language? "active":"" ?>" id="<?=$value['code']?>">
                    <?= $form->field($model, 'productName')->textInput(['name'=>'ProductLang['.$key.'][productName]','id'=>'product_name'.$key,'required'=>true,'placeholder'=>$value['lang']]) ?>
                    <?= $form->field($model, 'slug')->hiddenInput(['name'=>'ProductLang['.$key.'][slug]','id'=>'slug_product_'.$key])->label(false)?>
                    <script>
                        $('#slug_product_<?=$key?>').slugify('#product_name<?=$key?>');
                    </script>

                    <?= $form->field($model, 'short_description')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6,'name'=>'ProductLang['.$key.'][short_description]','id'=>'short_description'.$key,'required'=>true],
                        'language' => 'ru',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ]);?>
                    <?= $form->field($model, 'description')->widget(TinyMce::className(), [
                        'options' => ['rows' => 6,'name'=>'ProductLang['.$key.'][description]','id'=>'description'.$key,'required'=>true],
                        'language' => 'ru',
                        'clientOptions' => [
                            'plugins' => [
                                "advlist autolink lists link charmap print preview anchor",
                                "searchreplace visualblocks code fullscreen",
                                "insertdatetime media table contextmenu paste"
                            ],
                            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                        ]
                    ]);?>
                </div>
            <?php endforeach;?>
        </div>
    </div>

        <div id="imageUpload"></div>
        <?= $form->field($model, 'category',[
            'template'=>"{label}{input}{hint}{error}"
        ])->dropDownList(\backend\models\ProductForm::getCategoryList(),['prompt'=>'Select category','class'=>'categories-select form-control']) ?>
        <div id='categories-block'></div>
        <div id="catAttrValBlock"></div>
            <?= $form->field($model, 'accessory')->widget(Select2::classname(), [
                'name' => 'ProductForm[accessory][]',
                'showToggleAll'=>false,
                'options' => ['placeholder' => 'Select a accessory ...', 'multiple' => true],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'ajax' => [
                        'url' => '/admin/product/get-items',
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
            ],
            ]);?>
        <?= $form->field($model, 'manufacturer')->dropDownList(\backend\models\ProductForm::getManufacturerList(),['prompt'=>'Select manufacturer']) ?>
        <?= $form->field($model, 'supplier')->dropDownList(\backend\models\ProductForm::getSupplierList(),['prompt'=>'Select supplier']) ?>
        <?= $form->field($model, 'availability')->dropDownList([0=>Yii::t('frontend','not_available'),1=>Yii::t('frontend','available')],['prompt'=>'Select supplier']) ?>
        <?= $form->field($model, 'price')->textInput() ?>
        <?= $form->field($model, 'wholesale_price')->textInput() ?>
        <?= $form->field($model, 'cost_price')->textInput() ?>
        <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>
    <div class="form-group">
        <?=Html::button(Yii::t('backend','Create'),['class'=>'btn btn-success','id'=>'submitButton'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
