<?=$form->field($model,'name')->textInput(['name'=>'ProductLang['.$language.'][name]','id'=>'product_name_'.$language])?>
<?=$form->field($model,'short_description')->textarea(['name'=>'ProductLang['.$language.'][short_description]'])?>
<?=$form->field($model,'description')->textarea(['name'=>'ProductLang['.$language.'][description]'])?>

<?=$form->field($model,'slug')->hiddenInput(['name'=>'ProductLang['.$language.'][slug]','id'=>'slug_product_'.$language])->label(false)?>
<script>
    $('#slug_product_<?=$language?>').slugify('#product_name_<?=$language?>');
</script>
<script>
    var validator = $( "#w0" ).validate();
    validator.element('#product_name_<?=$language?>');
</script>