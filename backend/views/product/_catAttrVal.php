<?php
/**
 * Created by PhpStorm.
 * User: fuad
 * Date: 1/4/16
 * Time: 10:17 AM
 */
?>
<script src="/admin/js/selectByLanguage.js" type="text/javascript"></script>
<div>
    <div>
        <?php foreach($catAttrArray as $key=>$array):?>
            <?php foreach($array['languages'] as $langCode=>$lang):?>
                <h3 class="pageItem" data-language="<?=$langCode?>" style="<?=Yii::$app->language == $langCode ? 'display:block':'display:none'?>"><?=$lang?></h3>
            <?php endforeach?>

            <select name="ProductAttributesValues[<?=$key?>]" id="" class="attrSelect form-control" required>
                    <option value="">Select attribute value</option>
                    <?php foreach($array['items'] as $key=>$items):?>

                        <option value="<?=$key?>"

                        <?php if($newModel == false){?>

                            <?=in_array($key,$catAttrVal) ? 'selected':''?>
                        <?php }?>
                        <?php foreach($items as $itemLang=>$itemName):?><?=$itemLang?>="<?=$itemName?>"<?php endforeach;?>>
                        <?=$array['items'][$key][Yii::$app->language]?>
                        </option>

                    <?php endforeach?>
                </select>

            <?//=\yii\helpers\VarDumper::dump($catAttrVal[$array['key']],6,1)?>
        <?php endforeach;?>
    </div>


</div>
