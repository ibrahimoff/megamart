<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a( Yii::t('backend','Create Product'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute'=>'name',
                'label'=>Yii::t('backend','name'),
                'value'=>function($data){
                    return $data->productName->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'name',['class'=>'form-control','style'=>'width:100%']),
            ],
            [
                'attribute'=>'category',
                'label'=>Yii::t('backend','category'),
                'value'=>function($data){
                    return $data->category->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'category',['class'=>'form-control','style'=>'width:100%']),
            ],
//            [
//                'attribute'=>'manufacturer',
//                'label'=>Yii::t('backend','manufacturer'),
//                'value'=>function($data){
//                    return $data->manufacturer->name;
//                },
//                'filter'=>Html::activeTextInput($searchModel,'manufacturer',['class'=>'form-control','style'=>'width:100%']),
//            ],
//            [
//                'attribute'=>'supplier',
//                'label'=>Yii::t('backend','supplier'),
//                'value'=>function($data){
//                    return $data->supplier->name;
//                },
//                'filter'=>Html::activeTextInput($searchModel,'supplier',['class'=>'form-control','style'=>'width:100%']),
//            ],
            [
                'attribute'=>'image',
                'label'=>Yii::t('backend','image'),
                'format' => 'html',
                'value'=>function($data) {
                    $img = '';
                    if(!empty($data->productCoverImage)){
                         $img = "<img src='".Yii::getAlias('/frontend/web/uploads/products/').$data->productCoverImage->product_id.'-'.$data->productCoverImage->id.'-medium.jpg'."' style='width:90px;height:auto;'/>";
                    }
                    return $img;
                },
                'filter'=>''
            ],
            [
                'attribute'=>'on_sale',
                'label'=>Yii::t('backend','on_sale'),
                'format' => 'html',
                'value'=>function($data){
                    return $data->on_sale == 1 ? "Yes":"No";
                },
                'filter'=>Html::activeDropDownList($searchModel,'on_sale',[''=>Yii::t('backend','Select sale status'),0=>Yii::t('backend','No'),1=>Yii::t('backend','Yes')],['class'=>'form-control']),
            ],

            [
                'attribute'=>'quantity',
                'label'=>Yii::t('backend','quantity'),
                'value'=>function($data){
                    return $data->quantity;
                },
                'filter'=>Html::activeTextInput($searchModel,'quantity',['class'=>'form-control','style'=>'width:100%']),
            ],
            [
                'attribute'=>'price',
                'label'=>Yii::t('backend','price'),
                'value'=>function($data){
                    return $data->price;
                },
                'filter'=>Html::activeTextInput($searchModel,'price',['class'=>'form-control','style'=>'width:100%']),
            ],
            [
                'attribute'=>'wholesale_price',
                'label'=>Yii::t('backend','wholesale_price'),
                'value'=>function($data){
                    return $data->wholesale_price;
                },
                'filter'=>Html::activeTextInput($searchModel,'wholesale_price',['class'=>'form-control','style'=>'width:100%']),
            ],
            [
                'attribute'=>'cost_price',
                'label'=>Yii::t('backend','cost_price'),
                'value'=>function($data){
                    return $data->cost_price;
                },
                'filter'=>Html::activeTextInput($searchModel,'cost_price',['class'=>'form-control','style'=>'width:100%']),
            ],
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
