<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend','Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
            'firstname',
            'lastname',
            //'gender',
            // 'group_id',
            // 'date_birtday',
            // 'newsletter',
            // 'fb_uid',
            // 'g_uid',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
             'email:email',
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 10? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'10'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],
            [
                'attribute'=>'role',
                'label'=>Yii::t('backend','role'),
                'value'=>function($data){
                    $roleName = '';
                    switch ($data->role) {
                        case 1:
                            $roleName = 'user';
                            break;
                        case 8:
                            $roleName = 'content manager';
                            break;
                        case 9:
                            $roleName = 'moder';
                            break;
                        case 10:
                            $roleName = 'admin';
                            break;
                    }
                    return $data->role = Yii::t('backend',$roleName);
                },
                'filter'=>Html::activeDropDownList($searchModel,'role',[''=>Yii::t('backend','Select role'),
                    \common\models\User::ROLE_MODER=>Yii::t('backend','moder')],['class'=>'form-control']),
            ],
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php \yii\widgets\Pjax::end()?>
</div>
