<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="text-danger">
    <?=Yii::$app->session->getFlash('emailError')?>
</div>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'firstname')->textInput() ?>

    <?= $form->field($model, 'lastname')->textInput() ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <input type="password" hidden/>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'status')->dropDownList([0=>Yii::t('backend','inactive'),
                                                      10=>Yii::t('backend','active')],['prompt'=>Yii::t('backend','select_role')]) ?>

    <?= $form->field($model, 'role')->dropDownList([8=>Yii::t('backend','content_manager'),9=>Yii::t('backend','moder'),10=>Yii::t('backend','admin')],['prompt'=>Yii::t('backend','select_role')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend','Create') : Yii::t('backend','Update') , ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
