<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textInput() ?>
    <?php
    $items = [];
    foreach(Yii::$app->params['languages'] as $lang=>$info){
        $tab = [
            'label'=>Yii::t('backend',$info['lang']),
            'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
            ]),
            'active'=>$lang==Yii::$app->language,
        ];
        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
