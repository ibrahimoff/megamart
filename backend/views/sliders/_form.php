<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model backend\models\Sliders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sliders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'position')->dropDownList([
                                                        '1'=>Yii::t('backend','head'),
                                                        '2'=>Yii::t('backend','footer'),
                                                        '3'=>Yii::t('backend','left'),
                                                        '4'=>Yii::t('backend','right')

                                                    ], ['prompt' => Yii::t('backend','position')]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','inactive'), '1'=>Yii::t('backend','active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend','Create') : Yii::t('backend','Update') , ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
