<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SlidersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend','Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a( Yii::t('backend','Create Sliders'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute'=>'position',
                'label'=>Yii::t('backend','position'),
                'value'=>function($data){
                    $position = '';
                    switch ($data->position) {
                        case 1:
                            $position = Yii::t('backend','head');
                            break;
                        case 2:
                            $position = Yii::t('backend','footer');
                            break;
                        case 3:
                            $position = Yii::t('backend','left');
                            break;
                        case 4:
                            $position = Yii::t('backend','right');
                            break;
                    }
                    return $data->position = $position;
                },
                'filter'=>Html::activeDropDownList($searchModel,'position',[''=>Yii::t('backend','select_position'),'1'=>Yii::t('backend','head'),'2'=>Yii::t('backend','footer'),'3'=>Yii::t('backend','left'),'4'=>Yii::t('backend','right')],['class'=>'form-control']),
            ],
            'name',
            //'order',
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
