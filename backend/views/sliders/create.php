<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Sliders */

$this->title = Yii::t('backend','Create Sliders');
$this->params['breadcrumbs'][] = ['label' =>Yii::t('backend','Sliders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sliders-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
