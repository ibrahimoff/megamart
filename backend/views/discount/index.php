<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DiscountSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Discounts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Discount'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute'=>'product',
                'label'=>Yii::t('backend','Product'),
                'value'=>function($data){
                    return $data->productName->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'product',['class'=>'form-control'])
            ],
            [
                'attribute'=>Yii::t('backend','user'),
                'label'=>Yii::t('backend','User'),
                'value'=>function($data){
                    return $data->user->username;
                },
                'filter'=>Html::activeTextInput($searchModel,'user',['class'=>'form-control'])
            ],
            [
                'attribute'=>Yii::t('backend','image'),
                'format'=>'html',
                'value'=>function($data){
                    return '<img style="width:90px;height:90px;" src="'.$data->product->getCover('medium').'" />';
                },
            ],
            //'group_id',
            'old_price',
            'price',
            'from_date',
            'to_date',
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
