<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\Discount */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discount-form">

    <?php $form = ActiveForm::begin(); ?>
    <?//=\yii\helpers\VarDumper::dump($model,6,1)?>
    <?= $form->field($model, 'product_id')->widget(Select2::classname(), [
        'name' => 'Discount[product_id]',
        'data' => $model->isNewRecord ? []  : $values,
        'showToggleAll'=>false,
        'options' => ['placeholder' => 'Select product ...', 'multiple' => false],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => '/admin/discount/get-items',
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q:params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function(city) { return city.text; }'),
            'templateSelection' => new JsExpression('function (option) { if(option.id != ""){$.post("/admin/discount/get-old-price",{id:option.id}).success(function(result){$("#oldPriceInput").val(result.oldPrice)})} return option.text; }'),

        ],
        'pluginEvents'=>[
            "select2:select" => "function(data) { console.log(data.target) }",
        ]
    ]);?>

    <?= $form->field($model, 'old_price')->textInput(['id'=>'oldPriceInput','readonly'=>true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <div class="input_display_inline">
        <?php
        echo '<label class="control-label">'.Yii::t('backend','From Date').'</label>';
        echo DateTimePicker::widget([
            'name' => 'Discount[from_date]',
            'type' => DateTimePicker::TYPE_INLINE,
            'value'=>$model->isNewRecord ? '' :$model->from_date,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd H:i:s'
            ]
        ]);?>
    </div>

    <div class="input_display_inline">
        <?php
        echo '<label class="control-label">'.Yii::t('backend','To Date').'</label>';
        echo DateTimePicker::widget([
            'name' => 'Discount[to_date]',
            'type' => DateTimePicker::TYPE_INLINE,
            'value'=>$model->isNewRecord ? '' :$model->to_date,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd H:i:s'
            ]
        ]);?>
    </div>
    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
