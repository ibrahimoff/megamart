<?=$form->field($model,'name')->textInput(['name'=>'CategoryLang['.$language.'][name]','id'=>'category_name'.$language])?>
<?=$form->field($model,'description')->textarea(['name'=>'CategoryLang['.$language.'][description]'])?>
<?= $form->field($model, 'slug')->hiddenInput(['name'=>'CategoryLang['.$language.'][slug]','id'=>'slug_category_'.$language])->label(false)?>
<script>
    $('#slug_category_<?=$language?>').slugify('#category_name<?=$language?>');
</script>
