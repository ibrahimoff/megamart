<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
/* @var $this yii\web\View */
/* @var $model backend\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>
<script src="/admin/js/components/dynamic.js" type="text/babel"></script>
<script src="/admin/js/jquery.slugify.js" type="text/javascript"></script>

<div class="category-form">

    <?php $form = ActiveForm::begin(['id'=>'category-form']); ?>
    <?php $items = [];
        foreach(Yii::$app->params['languages'] as $lang=>$info){
            $tab = [
                'label'=>Yii::t('backend',$info['lang']),

                'content'=>$this->context->renderPartial('_form_multilang',[
                'form'=>$form,
                'model'=>$model->translateModels[$lang],
                'language'=>$lang,
                ]),
                'active'=>$lang=='en',
                'headerOptions' => ['data-lang' => $lang,'class'=>'lang_tabs'],
            ];

        $items[]=$tab;
    }
    echo yii\bootstrap\Tabs::widget(['items'=>$items]);
    ?>
    <div id="example" new-model="<?=$model->isNewRecord? 'true' : 'false'?>" model-id="<?=$model->isNewRecord? 'null' : $model->id?>"></div>
    <?= $form->field($model, 'parent_id')->dropDownList(\backend\models\Category::getParents(),['prompt'=>'Select parent']) ?>

    <?= $form->field($model, 'status')->dropDownList([ '0'=>Yii::t('backend','Inactive'), '1'=>Yii::t('backend','Active'), ], ['prompt' => Yii::t('backend','select_status')]) ?>
    <?php if(!$model->isNewRecord){?>
        <?= $form->field($model, 'order')->textInput() ?>
    <?php }?>
    <?= $form->field($model, 'top')->checkbox() ?>
    <div class="form-group">
        <?= Html::button($model->isNewRecord ? Yii::t('backend', 'Create2') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary','id'=>'submitBtn','type'=>'button','style'=>'display:none;']) ?>
    </div>
    <div id="changeButton">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
