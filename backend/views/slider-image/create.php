<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SliderImage */

$this->title = Yii::t('backend', 'Create Slider Image');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Slider Images'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
