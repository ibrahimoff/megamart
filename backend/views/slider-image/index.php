<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SliderImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Slider Images');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slider-image-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Slider Image'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin()?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>function($model){
            if(!($model->status == '0')){
                return ['class'=>'success'];

            }else{
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [
                'attribute'=>'slider',
                'label'=>Yii::t('backend','Slider'),
                'value'=>function($data){
                    return $data->slider->name;
                },
                'filter'=>Html::activeTextInput($searchModel,'slider',['class'=>'form-control'])
            ],
            [
              'attribute'=>'value',
                'label'=>Yii::t('backend','Value'),
               'value'=>function($data){
                   return $data->sliderLang->value;
               },
               'filter'=>Html::activeTextInput($searchModel,'value',['class'=>'form-control'])
            ],
            [
                'attribute'=>'link',
                'label'=>Yii::t('backend','link'),
                'value'=>function($data){
                    return $data->sliderLang->link;
                }
            ],
            [
                'attribute'=>'image',
                'label'=>Yii::t('backend','image'),
                'format' => 'html',
                'value'=>function($data) {
                    return "<img src='".Yii::getAlias('/frontend/web/uploads/slider/').$data->slider_id.'/'.$data->image."' style='width:90px;height:auto;'/>";
                },
                'filter'=>''
            ],
            [
                'attribute'=>'status',
                'label'=>Yii::t('backend','status'),
                'value'=>function($data){
                    return $data->status == 1? Yii::t('backend','active'):Yii::t('backend','inactive');
                },
                'filter'=>Html::activeDropDownList($searchModel,'status',[''=>Yii::t('backend','select_status'),'0'=>Yii::t('backend','inactive'),'1'=>Yii::t('backend','active')],['class'=>'form-control']),
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end()?>
</div>
